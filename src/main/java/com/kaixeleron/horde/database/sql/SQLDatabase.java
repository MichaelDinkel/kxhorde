package com.kaixeleron.horde.database.sql;

import com.kaixeleron.horde.database.DatabaseException;
import com.kaixeleron.horde.database.DatabaseManager;
import org.bukkit.entity.Player;

import java.sql.*;

public class SQLDatabase implements DatabaseManager {

    private Connection connection;

    private final String hostname, database, username, password;
    private final int port;

    public SQLDatabase(String hostname, int port, String database, String username, String password) throws DatabaseException {

        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;

        PreparedStatement createMuted = null;

        try {

            connect();

            createMuted = connection.prepareStatement("CREATE TABLE IF NOT EXISTS `kxhorde_muted` (`uuid` CHAR(36), UNIQUE(`uuid`));");

            createMuted.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(createMuted);

        }

    }

    private void connect() throws SQLException {

        if (connection == null || connection.isClosed()) {

            connection = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s", hostname, port, database), username, password);

        }

    }

    private void closeSilently(AutoCloseable... closeables) {

        for (AutoCloseable closeable : closeables) {

            if (closeable != null) {

                try {

                    closeable.close();

                } catch (Exception ignored) {}

            }

        }

    }

    @Override
    public void setMuted(Player player, boolean muted) throws DatabaseException {

        PreparedStatement statement = null;

        try {

            connect();

            if (muted) {

                statement = connection.prepareStatement("INSERT INTO `kxhorde_muted` (`uuid`) VALUES (?);");

            } else {

                statement = connection.prepareStatement("DELETE FROM `kxhorde_muted` WHERE `uuid` = ?;");

            }

            statement.setString(1, player.getUniqueId().toString());

            statement.executeUpdate();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(statement);

        }

    }

    @Override
    public boolean isMuted(Player player) throws DatabaseException {

        boolean muted;

        PreparedStatement get = null;
        ResultSet rs = null;

        try {

            connect();

            get = connection.prepareStatement("SELECT * FROM `kxhorde_muted` WHERE `uuid` = ?;");
            get.setString(1, player.getUniqueId().toString());

            rs = get.executeQuery();

            muted = rs.next();

        } catch (SQLException e) {

            throw new DatabaseException(e);

        } finally {

            closeSilently(get, rs);

        }

        return muted;

    }

}
