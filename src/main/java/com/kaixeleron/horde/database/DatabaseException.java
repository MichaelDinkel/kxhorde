package com.kaixeleron.horde.database;

import java.io.IOException;
import java.sql.SQLException;

public class DatabaseException extends Exception {

    private final Exception parent;

    public DatabaseException(SQLException parent) {

        this.parent = parent;

    }

    public DatabaseException(IOException parent) {

        this.parent = parent;

    }

    @Override
    public void printStackTrace() {

        super.printStackTrace();
        parent.printStackTrace();

    }

}
