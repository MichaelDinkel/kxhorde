package com.kaixeleron.horde.database;

import org.bukkit.entity.Player;

public interface DatabaseManager {

    void setMuted(Player player, boolean muted) throws DatabaseException;

    boolean isMuted(Player player) throws DatabaseException;

}
