package com.kaixeleron.horde.database.yaml;

import com.kaixeleron.horde.database.DatabaseException;
import com.kaixeleron.horde.database.DatabaseManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class YAMLDatabase implements DatabaseManager {

    private final File muteFile;
    private final FileConfiguration muteFileConfig;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public YAMLDatabase(File dataFolder) throws DatabaseException {

        File userDataFolder = new File(dataFolder, "userdata");
        userDataFolder.mkdir();

        try {

            muteFile = new File(userDataFolder, "mutes.yml");
            muteFile.createNewFile();

            muteFileConfig = YamlConfiguration.loadConfiguration(muteFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @Override
    public void setMuted(Player player, boolean muted) throws DatabaseException {

        List<String> muteList = muteFileConfig.getStringList("muted");

        if (muted) {

            muteList.add(player.getUniqueId().toString());

        } else {

            muteList.remove(player.getUniqueId().toString());

        }

        muteFileConfig.set("muted", muteList);

        try {

            muteFileConfig.save(muteFile);

        } catch (IOException e) {

            throw new DatabaseException(e);

        }

    }

    @Override
    public boolean isMuted(Player player) {

        return muteFileConfig.getStringList("muted").contains(player.getUniqueId().toString());

    }

}
