package com.kaixeleron.horde.party.command;

import com.kaixeleron.horde.database.DatabaseException;
import com.kaixeleron.horde.database.DatabaseManager;
import com.kaixeleron.horde.party.PartyManager;
import com.kaixeleron.horde.party.data.Party;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;

import static com.kaixeleron.horde.party.PartyManager.PREFIX;

public class CommandParty implements CommandExecutor {

    private final DatabaseManager databaseManager;
    private final PartyManager partyManager;

    public CommandParty(DatabaseManager databaseManager, PartyManager partyManager) {

        this.databaseManager = databaseManager;
        this.partyManager = partyManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player player) {

            switch (args.length) {

                case 0:

                    help(player, label);
                    break;

                case 1:

                    switch (args[0].toLowerCase()) {
                        case "create" -> create(player);
                        case "leave" -> leave(player);
                        case "join" -> join(player, label, null);
                        case "invite" -> invite(player, label, null);
                        case "kick" -> kick(player, label, null);
                        case "setleader" -> setLeader(player, label, null);
                        case "mute" -> mute(player);
                    }

                    break;

                default:

                    switch (args[0].toLowerCase()) {
                        case "create" -> create(player);
                        case "leave" -> leave(player);
                        case "join" -> join(player, label, args[1]);
                        case "invite" -> invite(player, label, args[1]);
                        case "kick" -> kick(player, label, args[1]);
                        case "setleader" -> setLeader(player, label, args[1]);
                        case "mute" -> mute(player);
                    }

                    break;

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

    private void help(Player player, String label) {

        player.sendMessage(PREFIX + ChatColor.AQUA + "kxHorde party management:");

        player.sendMessage(PREFIX + ChatColor.YELLOW + "/" + label + (partyManager.isInParty(player) ? ChatColor.RED : "") + " create " + ChatColor.RESET + "- Create a party.");
        player.sendMessage(PREFIX + ChatColor.YELLOW + "/" + label + (!partyManager.isInParty(player) ? ChatColor.RED : "") + " leave " + ChatColor.RESET + "- Leave a party.");

        player.sendMessage(PREFIX + ChatColor.YELLOW + "/" + label + (partyManager.isInParty(player) ? ChatColor.RED : "") + " join " + ChatColor.YELLOW + "<leader> " + ChatColor.RESET + "- Join a leader's party.");

        player.sendMessage(PREFIX + ChatColor.YELLOW + "/" + label + (!partyManager.isInParty(player) ||
                (!partyManager.getParty(player).getLeader().equals(player) && !player.hasPermission("kxhorde.command.party.invitebypass"))
                ? ChatColor.RED : "") + " invite " + ChatColor.YELLOW + "<player> " + ChatColor.RESET + "- Invite a player to your party.");

        player.sendMessage(PREFIX + ChatColor.YELLOW + "/" + label + (!partyManager.isInParty(player) ||
                (!partyManager.getParty(player).getLeader().equals(player) && !player.hasPermission("kxhorde.command.party.kickbypass"))
                ? ChatColor.RED : "") + " kick " + ChatColor.YELLOW + "<player> " + ChatColor.RESET + "- Kick a player from your party.");

        player.sendMessage(PREFIX + ChatColor.YELLOW + "/" + label + (!partyManager.isInParty(player) ||
                (!partyManager.getParty(player).getLeader().equals(player) && !player.hasPermission("kxhorde.command.party.leaderbypass"))
                ? ChatColor.RED : "") + " setleader " + ChatColor.YELLOW + "<leader> " + ChatColor.RESET + "- Set the party leader.");

        player.sendMessage(PREFIX + ChatColor.YELLOW + "/" + label + " mute " + ChatColor.RESET + "- Mute party invitation notifications.");

    }

    private void create(Player player) {

        if (partyManager.isInParty(player)) {

            player.sendMessage(PREFIX + ChatColor.RED + "You are already in a party and cannot create a new one.");

        } else {

            partyManager.createParty(player);
            player.sendMessage(PREFIX + ChatColor.AQUA + "Party created.");

        }

    }

    private void leave(Player player) {

        if (!partyManager.isInParty(player)) {

            player.sendMessage(PREFIX + ChatColor.RED + "You are not in a party.");

        } else {

            Party party = partyManager.getParty(player);

            party.removeMember(player);

            if (party.getLeader().equals(player) || party.getMemberCount() == 0) {

                partyManager.disbandParty(party);

            }

            player.sendMessage(PREFIX + ChatColor.AQUA + "You have left your party.");

        }

    }

    private void join(Player player, String label, @Nullable String leaderName) {

        if (leaderName == null) {

            player.sendMessage(PREFIX + ChatColor.AQUA + "Join a leader's party.");
            player.sendMessage(PREFIX + ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " join <leader>");

        } else {

            if (partyManager.isInParty(player)) {

                player.sendMessage(PREFIX + ChatColor.RED + "You are already in a party.");

            } else {

                Player leader = Bukkit.getPlayerExact(leaderName);

                if (leader == null) {

                    player.sendMessage(PREFIX + ChatColor.RESET + leaderName + ChatColor.RED + " is not online.");

                } else {

                    Party party = partyManager.getParty(leader);

                    if (party == null) {

                        player.sendMessage(PREFIX + ChatColor.RESET + leaderName + ChatColor.RED + " is not in a party.");

                    } else {

                        if (party.getLeader().equals(leader)) {

                            boolean bypass = player.hasPermission("kxhorde.command.party.joinbypass");

                            if (party.isInvited(player) || bypass) {

                                if (party.getMemberCount() >= partyManager.getGameSize() && !bypass && !leader.hasPermission("kxhorde.command.party.sizebypass")) {

                                    player.sendMessage(PREFIX + ChatColor.RESET + leader.getName() + "'s" + ChatColor.RED + " party is full.");

                                } else {

                                    party.addMember(player);
                                    player.sendMessage(PREFIX + ChatColor.AQUA + "Joined " + ChatColor.RESET + leader.getName() + "'s" + ChatColor.AQUA + " party.");

                                }

                            } else {

                                player.sendMessage(PREFIX + ChatColor.RED + "You have not been invited to " + ChatColor.RESET + leaderName + ChatColor.RESET + "'s" + ChatColor.RED + " party.");

                            }

                        } else {

                            player.sendMessage(PREFIX + ChatColor.RESET + leaderName + ChatColor.RED + " is not the leader of their party.");

                        }

                    }

                }

            }

        }

    }

    private void invite(Player player, String label, @Nullable String inviteeName) {

        if (inviteeName == null) {

            player.sendMessage(PREFIX + ChatColor.AQUA + "Invite a player to your party.");
            player.sendMessage(PREFIX + ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " invite <player>");

        } else {

            Party party = partyManager.getParty(player);

            if (party == null) {

                player.sendMessage(PREFIX + ChatColor.RED + "You are not in a party.");

            } else {

                if (party.getLeader().equals(player) || player.hasPermission("kxhorde.command.party.invitebypass")) {

                    Player invitee = Bukkit.getPlayerExact(inviteeName);

                    if (invitee == null || !player.canSee(invitee)) {

                        player.sendMessage(PREFIX + ChatColor.RESET + inviteeName + ChatColor.RED + " is not online.");

                    } else {

                        if (party.isMember(invitee)) {

                            player.sendMessage(PREFIX + ChatColor.RESET + invitee.getName() + ChatColor.RED + " is already in your party.");

                        } else {

                            if (party.isInvited(invitee)) {

                                party.removeInvitation(invitee);
                                player.sendMessage(PREFIX + ChatColor.AQUA + "Cancelled " + ChatColor.RESET + invitee.getName() + "'s" + ChatColor.AQUA + " invitation.");

                            } else {

                                party.addInvitation(player, invitee);
                                player.sendMessage(PREFIX + ChatColor.AQUA + "Invited " + ChatColor.RESET + invitee.getName() + ChatColor.AQUA + ".");

                            }

                        }

                    }

                } else {

                    player.sendMessage(PREFIX + ChatColor.RED + "You are not the leader of your party.");

                }

            }

        }

    }

    private void kick(Player player, String label, @Nullable String targetName) {

        if (targetName == null) {

            player.sendMessage(PREFIX + ChatColor.AQUA + "Kick a player from your party.");
            player.sendMessage(PREFIX + ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " kick <player>");

        } else {

            Party party = partyManager.getParty(player);

            if (party == null) {

                player.sendMessage(PREFIX + ChatColor.RED + "You are not in a party.");

            } else {

                if (party.getLeader().equals(player) || player.hasPermission("kxhorde.command.party.kickbypass")) {

                    Player target = Bukkit.getPlayerExact(targetName);

                    if (target == null) {

                        player.sendMessage(PREFIX + ChatColor.RESET + targetName + ChatColor.RED + " is not online.");

                    } else {

                        if (party.isMember(target)) {

                            party.removeMember(target);

                            if (party.getLeader().equals(target) || party.getMemberCount() == 0) {

                                partyManager.disbandParty(party);

                            }

                            player.sendMessage(PREFIX + ChatColor.AQUA + "Kicked " + ChatColor.RESET + target.getName() + ChatColor.AQUA + ".");
                            target.sendMessage(PREFIX + ChatColor.AQUA + "You have been kicked from your party.");

                        } else {

                            player.sendMessage(PREFIX + ChatColor.RESET + target.getName() + ChatColor.RED + " is not in your party.");

                        }

                    }

                } else {

                    player.sendMessage(PREFIX + ChatColor.RED + "You are not the leader of your party.");

                }

            }

        }

    }

    private void setLeader(Player player, String label, @Nullable String newLeaderName) {

        if (newLeaderName == null) {

            player.sendMessage(PREFIX + ChatColor.AQUA + "Set the party leader.");
            player.sendMessage(PREFIX + ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " setleader <leader>");

        } else {

            Party party = partyManager.getParty(player);

            if (party == null) {

                player.sendMessage(PREFIX + ChatColor.RED + "You are not in a party.");

            } else {

                if (party.getLeader().equals(player) || player.hasPermission("kxhorde.command.party.leaderbypass")) {

                    Player newLeader = Bukkit.getPlayerExact(newLeaderName);

                    if (newLeader == null) {

                        player.sendMessage(PREFIX + ChatColor.RESET + newLeaderName + ChatColor.RED + " is not online.");

                    } else {

                        if (party.getLeader().equals(newLeader)) {

                            player.sendMessage(PREFIX + ChatColor.RESET + newLeader.getName() + ChatColor.RED + " is already the leader of the party.");

                        } else {

                            party.setLeader(newLeader);

                        }

                    }

                } else {

                    player.sendMessage(PREFIX + ChatColor.RED + "You are not the leader of your party.");

                }

            }

        }

    }

    private void mute(Player player) {

        boolean muted = partyManager.isMuted(player);

        try {

            databaseManager.setMuted(player, !muted);

        } catch (DatabaseException e) {

            System.err.printf("Could not set muted status for %s.%n", player.getName());
            e.printStackTrace();

        }

        partyManager.setMuted(player, !muted);

        player.sendMessage(PREFIX + ChatColor.AQUA + "Party invites " + (muted ? "unmuted." : "muted."));

    }

}
