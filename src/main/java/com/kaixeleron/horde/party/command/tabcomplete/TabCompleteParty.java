package com.kaixeleron.horde.party.command.tabcomplete;

import com.kaixeleron.horde.party.PartyManager;
import com.kaixeleron.horde.party.data.Party;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TabCompleteParty implements TabCompleter {

    private final PartyManager partyManager;

    public TabCompleteParty(PartyManager partyManager) {

        this.partyManager = partyManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

        List<String> suggestions = new ArrayList<>();

        if (sender instanceof Player player) {

            switch (args.length) {
                case 1 -> {

                    Party party = partyManager.getParty(player);

                    if (party == null) {

                        if (args[0].length() == 0 || "create".startsWith(args[0].toLowerCase())) {

                            suggestions.add("create");

                        }

                        if (args[0].length() == 0 || "join".startsWith(args[0].toLowerCase())) {

                            suggestions.add("join");

                        }

                    } else {

                        if (party.getLeader().equals(player)) {

                            if (args[0].length() == 0 || "invite".startsWith(args[0].toLowerCase())) {

                                suggestions.add("invite");

                            }

                            if (args[0].length() == 0 || "kick".startsWith(args[0].toLowerCase())) {

                                suggestions.add("kick");

                            }

                            if (args[0].length() == 0 || "setleader".startsWith(args[0].toLowerCase())) {

                                suggestions.add("setleader");

                            }

                        } else {

                            if (player.hasPermission("kxhorde.command.party.invitebypass")) {

                                if (args[0].length() == 0 || "invite".startsWith(args[0].toLowerCase())) {

                                    suggestions.add("invite");

                                }

                            }

                            if (player.hasPermission("kxhorde.command.party.kickbypass")) {

                                if (args[0].length() == 0 || "kick".startsWith(args[0].toLowerCase())) {

                                    suggestions.add("kick");

                                }

                            }

                            if (player.hasPermission("kxhorde.command.party.leaderbypass")) {

                                if (args[0].length() == 0 || "setleader".startsWith(args[0].toLowerCase())) {

                                    suggestions.add("setleader");

                                }

                            }

                        }

                        if (args[0].length() == 0 || "leave".startsWith(args[0].toLowerCase())) {

                            suggestions.add("leave");

                        }

                    }

                    if (args[0].length() == 0 || "mute".startsWith(args[0].toLowerCase())) {

                        suggestions.add("mute");

                    }

                }
                case 2 -> {

                    switch (args[0].toLowerCase()) {

                        case "join":

                            if (!partyManager.isInParty(player)) {

                                for (Party party : partyManager.getParties()) {

                                    if (player.hasPermission("kxhorde.command.party.joinbypass") || party.isInvited(player)) {

                                        String leader = party.getLeader().getName();

                                        if (args[1].length() == 0 || leader.toLowerCase().startsWith(args[1].toLowerCase())) {

                                            if (player.canSee(party.getLeader())) {

                                                suggestions.add(leader);

                                            }

                                        }

                                    }

                                }

                            }

                            break;

                        case "invite": {

                            Party party = partyManager.getParty(player);

                            if (party != null && (player.hasPermission("kxhorde.command.party.invitebypass") || party.getLeader().equals(player))) {

                                for (Player nonMember : Bukkit.getOnlinePlayers()) {

                                    if (!party.isMember(nonMember) && player.canSee(nonMember)) {

                                        String name = nonMember.getName();

                                        if (args[1].length() == 0 || name.toLowerCase().startsWith(args[1].toLowerCase())) {

                                            suggestions.add(name);

                                        }

                                    }

                                }

                            }

                            break;

                        }

                        case "kick": {

                            Party party = partyManager.getParty(player);

                            if (party != null && (player.hasPermission("kxhorde.command.party.kickbypass") || party.getLeader().equals(player))) {

                                for (Player member : party.getMembers()) {

                                    String name = member.getName();

                                    if (args[1].length() == 0 || name.toLowerCase().startsWith(args[1].toLowerCase())) {

                                        suggestions.add(name);

                                    }

                                }

                            }

                            break;

                        }

                        case "setleader": {

                            Party party = partyManager.getParty(player);

                            if (party != null && (player.hasPermission("kxhorde.command.party.leaderbypass") || party.getLeader().equals(player))) {

                                for (Player member : party.getMembers()) {

                                    if (!party.getLeader().equals(member)) {

                                        String name = member.getName();

                                        if (args[1].length() == 0 || name.toLowerCase().startsWith(args[1].toLowerCase())) {

                                            suggestions.add(name);

                                        }

                                    }

                                }

                            }

                            break;

                        }

                    }

                }
            }

        }

        return suggestions;

    }

}
