package com.kaixeleron.horde.party.command;

import com.kaixeleron.horde.chat.ChatManager;
import com.kaixeleron.horde.party.PartyManager;
import com.kaixeleron.horde.party.data.Party;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.kaixeleron.horde.party.PartyManager.PREFIX;

public class CommandPartyChat implements CommandExecutor {

    private static final String PARTY_PREFIX = ChatColor.BLUE + "[P] ";

    private final ChatManager chatManager;
    private final PartyManager partyManager;

    public CommandPartyChat(ChatManager chatManager, PartyManager partyManager) {

        this.chatManager = chatManager;
        this.partyManager = partyManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player player) {

            if (args.length == 0) {

                sender.sendMessage(ChatColor.RED + "Chat with members of your party.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " <message>");

            } else {

                Party party = partyManager.getParty(player);

                if (party == null) {

                    sender.sendMessage(PREFIX + ChatColor.RED + "You are not in a party.");

                } else {

                    StringBuilder chatMessage = new StringBuilder(PARTY_PREFIX);

                    chatMessage.append(chatManager.getPartyChatColor(player));

                    chatMessage.append(player.getName());
                    chatMessage.append(ChatColor.RESET);
                    chatMessage.append(": ");

                    for (String word : args) {

                        chatMessage.append(word);
                        chatMessage.append(" ");

                    }

                    for (Player member : party.getMembers()) {

                        member.sendMessage(chatMessage.toString());

                    }

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
