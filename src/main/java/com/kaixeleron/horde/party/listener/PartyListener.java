package com.kaixeleron.horde.party.listener;

import com.kaixeleron.horde.database.DatabaseException;
import com.kaixeleron.horde.database.DatabaseManager;
import com.kaixeleron.horde.party.PartyManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PartyListener implements Listener {

    private final DatabaseManager databaseManager;
    private final PartyManager partyManager;

    public PartyListener(DatabaseManager databaseManager, PartyManager partyManager) {

        this.databaseManager = databaseManager;
        this.partyManager = partyManager;

        for (Player player : Bukkit.getOnlinePlayers()) {

            try {

                partyManager.setMuted(player, databaseManager.isMuted(player));

            } catch (DatabaseException e) {

                System.err.printf("Could not load muted status for %s.%n", player.getName());
                e.printStackTrace();

            }

        }

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        boolean muted = false;

        try {

            muted = databaseManager.isMuted(event.getPlayer());

        } catch (DatabaseException e) {

            System.err.printf("Could not load muted status for %s.%n", event.getPlayer().getName());
            e.printStackTrace();

        }

        partyManager.setMuted(event.getPlayer(), muted);

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        partyManager.setMuted(event.getPlayer(), false);

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        partyManager.setMuted(event.getPlayer(), false);

    }

}
