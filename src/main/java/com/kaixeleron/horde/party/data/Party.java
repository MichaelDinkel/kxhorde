package com.kaixeleron.horde.party.data;

import com.kaixeleron.horde.chat.ChatManager;
import com.kaixeleron.horde.party.PartyManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.List;

import static com.kaixeleron.horde.party.PartyManager.LEADER_COLOR;
import static com.kaixeleron.horde.party.PartyManager.PREFIX;

public class Party {

    private Player leader;

    private final ChatManager chatManager;

    private final PartyManager partyManager;

    private final List<Player> members, invitations;

    private final Scoreboard scoreboard;

    private final Objective partyBoardObjective;

    @SuppressWarnings("ConstantConditions")
    public Party(Player leader, ChatManager chatManager, PartyManager partyManager) {

        this.leader = leader;
        this.chatManager = chatManager;
        this.partyManager = partyManager;

        members = new ArrayList<>();

        invitations = new ArrayList<>();

        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        partyBoardObjective = scoreboard.registerNewObjective("party", "dummy", ChatColor.RED + "" + ChatColor.BOLD + "Party");
        partyBoardObjective.setDisplaySlot(DisplaySlot.SIDEBAR);

        addMember(leader);

    }

    public Player getLeader() {

        return leader;

    }

    public void setLeader(Player leader) {

        scoreboard.resetScores(LEADER_COLOR + "Leader " + chatManager.getChatColor(leader) + this.leader.getName());
        scoreboard.resetScores(leader.getName());

        partyBoardObjective.getScore(LEADER_COLOR + "Leader " + chatManager.getChatColor(leader) + leader.getName()).setScore(0);
        partyBoardObjective.getScore(this.leader.getName()).setScore(0);

        if (members.contains(leader)) {

            this.leader = leader;

        }

        for (Player member : members) {

            member.sendMessage(PREFIX + chatManager.getChatColor(leader) + leader.getName() + ChatColor.AQUA + " has been promoted to leader of the party.");

        }

    }

    public void addMember(Player player) {

        invitations.remove(player);
        members.add(player);

        partyBoardObjective.getScore((player.equals(leader) ? LEADER_COLOR + "Leader " : "") + chatManager.getChatColor(player) + player.getName()).setScore(0);

        player.setScoreboard(scoreboard);

        for (Player member : members) {

            if (!member.equals(player)) {

                member.sendMessage(PREFIX + chatManager.getChatColor(player) + player.getName() + ChatColor.AQUA + " has joined the party.");

            }

        }

    }

    @SuppressWarnings("ConstantConditions")
    public void removeMember(Player player) {

        members.remove(player);

        scoreboard.resetScores((player.equals(leader) ? LEADER_COLOR + "Leader " : "") + chatManager.getChatColor(player) + player.getName());

        player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());

        if (!player.equals(leader)) {

            for (Player member : members) {

                member.sendMessage(PREFIX + chatManager.getChatColor(player) + player.getName() + ChatColor.AQUA + " has left the party.");

            }

        }

    }

    public boolean isMember(Player player) {

        return members.contains(player);

    }

    public void clearMembers() {

        List<Player> copy = new ArrayList<>(members);

        for (Player member : copy) {

            removeMember(member);
            member.sendMessage(PREFIX + ChatColor.AQUA + "Your party has been disbanded.");

        }

        leader = null;

    }

    public int getMemberCount() {

        return members.size();

    }

    public void addInvitation(Player inviter, Player player) {

        invitations.add(player);

        if (!partyManager.isMuted(player)) {

            player.sendMessage(PREFIX + chatManager.getChatColor(inviter) + inviter.getName() + ChatColor.AQUA + " has invited you to "
                    + (inviter.equals(leader) ? "their" : chatManager.getChatColor(leader) + leader.getName() + "'s" + ChatColor.AQUA)
                    + " party. Type " + ChatColor.RESET + "/party join " + leader.getName() + ChatColor.AQUA + " to join.");

        }

    }

    public void removeInvitation(Player player) {

        invitations.remove(player);

    }

    public boolean isInvited(Player player) {

        return invitations.contains(player);

    }

    public void clearInvitations() {

        invitations.clear();

    }

    public Player[] getMembers() {

        return members.toArray(new Player[0]);

    }

}
