package com.kaixeleron.horde.party;

import com.kaixeleron.horde.chat.ChatManager;
import com.kaixeleron.horde.party.data.Party;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PartyManager {

    public static final String PREFIX = ChatColor.BLUE + "[Party] ";
    public static final ChatColor LEADER_COLOR = ChatColor.GOLD;

    private final List<Party> parties;
    private final List<Player> muted;

    private final ChatManager chatManager;

    private final int gameSize;

    public PartyManager(ChatManager chatManager, int gameSize) {

        parties = new ArrayList<>();
        this.muted = new ArrayList<>();

        this.chatManager = chatManager;
        this.gameSize = gameSize;

    }

    public void createParty(Player leader) {

        parties.add(new Party(leader, chatManager, this));

    }

    public Party getParty(Player player) {

        Party party = null;

        int i = 0;

        while (party == null && i < parties.size()) {

            if (parties.get(i).isMember(player)) {

                party = parties.get(i);

            }

            i++;

        }

        return party;

    }

    public Party[] getParties() {

        return parties.toArray(new Party[0]);

    }

    public boolean isInParty(Player player) {

        boolean inParty = false;

        int i = 0;

        while (i < parties.size() && !inParty) {

            inParty = parties.get(i).isMember(player);

            i++;

        }

        return inParty;

    }

    public void disbandParty(Party party) {

        party.clearMembers();
        party.clearInvitations();
        parties.remove(party);

    }

    public void disbandAllParties() {

        for (Party party : parties) {

            party.clearMembers();
            party.clearInvitations();

        }

        parties.clear();

    }

    public void setMuted(Player player, boolean muted) {

        if (muted) {

            if (!this.muted.contains(player)) {

                this.muted.add(player);

            }

        } else {

            this.muted.remove(player);

        }

    }

    public boolean isMuted(Player player) {

        return muted.contains(player);

    }

    public void clearMuted() {

        muted.clear();

    }

    public int getGameSize() {

        return gameSize;

    }

}
