package com.kaixeleron.horde.chat;

import com.kaixeleron.horde.mission.data.MissionDifficulty;
import com.kaixeleron.horde.party.PartyManager;
import com.kaixeleron.horde.party.data.Party;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatManager {

    private PartyManager partyManager;

    public void setPartyManager(PartyManager partyManager) {

        this.partyManager = partyManager;

    }

    public ChatColor getChatColor(Player player) {

        ChatColor result = ChatColor.RESET;

        if (player.hasPermission("kxhorde.chat.op")) {

            result = ChatColor.RED;

        } else if (player.hasPermission("kxhorde.chat.staff")) {

            result = ChatColor.YELLOW;

        }

        return result;

    }

    public ChatColor getPartyChatColor(Player player) {

        ChatColor result = ChatColor.GRAY;

        if (player.hasPermission("kxhorde.chat.op")) {

            result = ChatColor.RED;

        } else if (player.hasPermission("kxhorde.chat.staff")) {

            result = ChatColor.YELLOW;

        } else {

            Party party = partyManager.getParty(player);

            if (party != null && party.getLeader().equals(player)) {

                result = PartyManager.LEADER_COLOR;

            }

        }

        return result;

    }

    public ChatColor getDifficultyChatColor(MissionDifficulty difficulty) {

        return switch (difficulty) {
            case UNSET -> ChatColor.GRAY;
            case EASY -> ChatColor.GREEN;
            case MEDIUM -> ChatColor.YELLOW;
            case HARD -> ChatColor.RED;
            case EXPERT -> ChatColor.DARK_RED;
            case NIGHTMARE -> ChatColor.LIGHT_PURPLE;
            case IMPOSSIBLE -> ChatColor.DARK_PURPLE;
        };

    }

}
