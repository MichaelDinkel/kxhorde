package com.kaixeleron.horde.entity.listener;

import com.comphenix.packetwrapper.WrapperPlayClientUseEntity;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.entity.EntityManager;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PacketPlayInUseEntityListener extends PacketAdapter {

    private final HordeMain main;

    private final EntityManager entityManager;

    private final List<Player> debounce;

    public PacketPlayInUseEntityListener(HordeMain main, EntityManager entityManager) {

        super(main, PacketType.Play.Client.USE_ENTITY);

        this.main = main;
        this.entityManager = entityManager;
        this.debounce = Collections.synchronizedList(new ArrayList<>());

    }

    @Override
    public void onPacketReceiving(PacketEvent event) {

        if (event.getPacketType().equals(PacketType.Play.Client.USE_ENTITY)) {

            boolean stop;

            synchronized (debounce) {

                stop = debounce.contains(event.getPlayer());

            }

            if (!stop) {

                synchronized (debounce) {

                    debounce.add(event.getPlayer());

                }

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        WrapperPlayClientUseEntity packet = new WrapperPlayClientUseEntity(event.getPacket());
                        entityManager.fireEntityUse(event.getPlayer(), packet.getTargetID(), packet.getType());

                    }

                }.runTask(main);

                new BukkitRunnable() {

                    @Override
                    public void run() {

                        synchronized (debounce) {

                            debounce.remove(event.getPlayer());

                        }

                    }

                }.runTaskLaterAsynchronously(main, 1L);

            }

        }

    }

}
