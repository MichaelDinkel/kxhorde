package com.kaixeleron.horde.entity;

import com.comphenix.packetwrapper.*;
import com.comphenix.protocol.wrappers.*;
import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.data.HordePlayerClassType;
import com.kaixeleron.horde.entity.data.HordeEntity;
import com.kaixeleron.horde.entity.data.HordeEntityType;
import com.kaixeleron.horde.game.GameManager;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.ItemManager;
import com.kaixeleron.horde.items.data.HordeItemType;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class EntityManager {

    private final HordeMain main;

    private GameManager gameManager;
    private final ItemManager itemManager;

    private final Map<Integer, HordeEntity> entities;

    public EntityManager(HordeMain main, ItemManager itemManager) {

        this.main = main;
        this.itemManager = itemManager;

        entities = new HashMap<>();

    }

    public Location[] displayClassSelectEntities(Player player) {

        Location location = player.getLocation().clone();

        Location center = getForward(location, 4.0D);

        // Instruction armor stand
        WrapperPlayServerSpawnEntityLiving armorStandPacket = new WrapperPlayServerSpawnEntityLiving();
        armorStandPacket.setEntityID(Integer.MAX_VALUE);
        armorStandPacket.setUniqueId(UUID.randomUUID());
        armorStandPacket.setType(HordeEntityType.ARMOR_STAND.getNetworkId());
        armorStandPacket.setX(center.getX());
        armorStandPacket.setY(center.getY() + 0.5D);
        armorStandPacket.setZ(center.getZ());
        armorStandPacket.sendPacket(player);

        WrapperPlayServerEntityMetadata armorStandMeta = new WrapperPlayServerEntityMetadata();
        armorStandMeta.setEntityID(Integer.MAX_VALUE);
        WrappedDataWatcher armorStandWatcher = new WrappedDataWatcher();
        WrappedDataWatcher.Serializer byteSerializer = WrappedDataWatcher.Registry.get(Byte.class);
        WrappedDataWatcher.Serializer chatSerializer = WrappedDataWatcher.Registry.getChatComponentSerializer(true);
        WrappedDataWatcher.Serializer booleanSerializer = WrappedDataWatcher.Registry.get(Boolean.class);
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, byteSerializer), (byte) (0x20)); //Invisible
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(2, chatSerializer), Optional.of(WrappedChatComponent.fromJson("{\"text\":\"Right click to select a class\",\"color\":\"red\",\"bold\":\"true\"}").getHandle())); //TODO localization
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(3, booleanSerializer), true); //Is name visible
        armorStandMeta.setMetadata(armorStandWatcher.getWatchableObjects());
        armorStandMeta.sendPacket(player);

        byte yaw = (byte) Math.round(location.getYaw());
        yaw += (byte) 127;

        HordePlayerClassType[] classTypes = HordePlayerClassType.values();
        int numClasses = classTypes.length;

        Location[] npcLocs = new Location[numClasses];

        for (int i = 0; i < numClasses; i++) {

            Location loc = getLeft(center, ((float) numClasses / 2.0D) - i - 0.5D);

            npcLocs[i] = loc.clone();

            int entityID = Integer.MAX_VALUE - 1 - i;

            // Class NPC
            String name = classTypes[i].name().charAt(0) + classTypes[i].name().toLowerCase().substring(1);
            WrappedChatComponent nameComponent = WrappedChatComponent.fromText(name);
            UUID npcUUID = UUID.randomUUID();

            WrapperPlayServerPlayerInfo playerInfo = new WrapperPlayServerPlayerInfo();
            playerInfo.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
            List<PlayerInfoData> data = new ArrayList<>();
            data.add(new PlayerInfoData(new WrappedGameProfile(npcUUID, name), 0, EnumWrappers.NativeGameMode.CREATIVE, nameComponent));
            playerInfo.setData(data);
            playerInfo.sendPacket(player);

            WrapperPlayServerNamedEntitySpawn namedSpawn = new WrapperPlayServerNamedEntitySpawn();
            namedSpawn.setEntityID(entityID);
            namedSpawn.setPlayerUUID(npcUUID);
            namedSpawn.setX(loc.getX());
            namedSpawn.setY(loc.getY());
            namedSpawn.setZ(loc.getZ());
            namedSpawn.setYaw(yaw);
            namedSpawn.setPitch(0.0F);
            namedSpawn.sendPacket(player);

            WrapperPlayServerPlayerInfo playerInfoRemove = new WrapperPlayServerPlayerInfo();
            playerInfoRemove.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
            List<PlayerInfoData> removeData = new ArrayList<>();
            removeData.add(new PlayerInfoData(new WrappedGameProfile(npcUUID, name), 0, EnumWrappers.NativeGameMode.CREATIVE, nameComponent));
            playerInfoRemove.setData(removeData);
            playerInfoRemove.sendPacket(player);

            WrapperPlayServerEntityHeadRotation headRotation = new WrapperPlayServerEntityHeadRotation();
            headRotation.setEntityID(entityID);
            headRotation.setHeadYaw(yaw);
            headRotation.sendPacket(player);

            WrapperPlayServerEntityMetadata playerMeta = new WrapperPlayServerEntityMetadata();
            playerMeta.setEntityID(entityID);
            WrappedDataWatcher playerDataWatcher = new WrappedDataWatcher();
            playerDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(16, byteSerializer), (byte) (0x01 | 0x02 | 0x04 | 0x08 | 0x10 | 0x20 | 0x40)); // Enable skin layers
            playerMeta.setMetadata(playerDataWatcher.getWatchableObjects());
            playerMeta.sendPacket(player);

            // Armor
            ItemStack helmet = null, chestplate = null, leggings = null, boots = null, weapon = null;

            //TODO player loadouts

            for (HordeItemType type : itemManager.getStockItems()) {

                for (HordePlayerClassType classType : type.getPlayerClassTypes()) {

                    if (classType.equals(classTypes[i])) {

                        switch (type.getSlot()) {

                            case HELMET -> helmet = itemManager.generateItem(player, null, type).getMinecraftItem();
                            case CHESTPLATE -> chestplate = itemManager.generateItem(player, null, type).getMinecraftItem();
                            case LEGGINGS -> leggings = itemManager.generateItem(player, null, type).getMinecraftItem();
                            case BOOTS -> boots = itemManager.generateItem(player, null, type).getMinecraftItem();
                            case PRIMARY -> weapon = itemManager.generateItem(player, null, type).getMinecraftItem();

                        }

                    }

                }

            }

            WrapperPlayServerEntityEquipment equipmentPacket = new WrapperPlayServerEntityEquipment();
            equipmentPacket.setEntityID(entityID);

            if (helmet != null) {

                equipmentPacket.setSlotStackPair(EnumWrappers.ItemSlot.HEAD, helmet);

            }

            if (chestplate != null) {

                equipmentPacket.setSlotStackPair(EnumWrappers.ItemSlot.CHEST, chestplate);

            }

            if (leggings != null) {

                equipmentPacket.setSlotStackPair(EnumWrappers.ItemSlot.LEGS, leggings);

            }

            if (boots != null) {

                equipmentPacket.setSlotStackPair(EnumWrappers.ItemSlot.FEET, boots);

            }

            if (weapon != null) {

                equipmentPacket.setSlotStackPair(EnumWrappers.ItemSlot.MAINHAND, weapon);

            }

            equipmentPacket.sendPacket(player);

        }

        return npcLocs;

    }

    public void clearClassSelectEntities(Player player) {

        int numClasses = HordePlayerClassType.values().length;
        int[] ids = new int[numClasses + 1];

        ids[0] = Integer.MAX_VALUE;

        for (int i = 0; i < numClasses; i++) {

            int entityID = Integer.MAX_VALUE - 1 - i;
            ids[i + 1] = entityID;

        }

        WrapperPlayServerEntityDestroy packet = new WrapperPlayServerEntityDestroy();
        packet.setEntityIds(ids);

        packet.sendPacket(player);

    }

    private int generateEntityId() {

        /*int id = Integer.MAX_VALUE;

        while (entities.containsKey(id) && id > 0) {

            id--;

        }*/

        return Collections.min(entities.keySet());

    }

    public HordeEntity generateEntity(HordeEntityType type, String name, double health, double speed, double armorFactor, double weaponFactor, Location spawn) {

        if (health < 0.0D) {

            health = type.getStandardHealth();

        }

        if (speed < 0.0D) {

            speed = type.getStandardSpeed();

        }

        return new HordeEntity(main, this, generateEntityId(), type, name, health, speed, armorFactor, weaponFactor, spawn);

    }

    public void showEntity(HordeEntity entity, Player player) {

        entity.addSeer(player);

        Location location = entity.getLocation();

        if (entity.getType().equals(HordeEntityType.PLAYER)) {

            UUID npcUUID = UUID.randomUUID();

            WrapperPlayServerPlayerInfo playerInfo = new WrapperPlayServerPlayerInfo();
            playerInfo.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
            List<PlayerInfoData> data = new ArrayList<>();
            data.add(new PlayerInfoData(new WrappedGameProfile(npcUUID, "bot"), 0, EnumWrappers.NativeGameMode.CREATIVE, WrappedChatComponent.fromText("bot")));
            playerInfo.setData(data);
            playerInfo.sendPacket(player);

            WrapperPlayServerNamedEntitySpawn packet = new WrapperPlayServerNamedEntitySpawn();
            packet.setEntityID(entity.getId());
            packet.setPlayerUUID(npcUUID);
            packet.setX(location.getX());
            packet.setY(location.getY());
            packet.setZ(location.getZ());
            packet.setYaw(location.getYaw());
            packet.setPitch(location.getPitch());
            packet.sendPacket(player);

            WrapperPlayServerPlayerInfo playerInfoRemove = new WrapperPlayServerPlayerInfo();
            playerInfoRemove.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
            List<PlayerInfoData> removeData = new ArrayList<>();
            removeData.add(new PlayerInfoData(new WrappedGameProfile(npcUUID, "bot"), 0, EnumWrappers.NativeGameMode.CREATIVE, WrappedChatComponent.fromText("bot")));
            playerInfoRemove.setData(removeData);
            playerInfoRemove.sendPacket(player);

        } else {

            WrapperPlayServerSpawnEntityLiving packet = new WrapperPlayServerSpawnEntityLiving();
            packet.setEntityID(entity.getId());
            packet.setUniqueId(UUID.randomUUID());
            packet.setType(entity.getType().getNetworkId());
            packet.setX(location.getX());
            packet.setY(location.getY());
            packet.setZ(location.getZ());
            packet.setYaw(location.getYaw());
            packet.setPitch(location.getPitch());

            packet.sendPacket(player);

        }

    }

    public void hideEntity(HordeEntity entity, Player player) {

        entity.removeSeer(player);

        WrapperPlayServerEntityDestroy packet = new WrapperPlayServerEntityDestroy();
        packet.setEntityIds(new int[]{ entity.getId() });

        packet.sendPacket(player);

    }

    public void removeEntity(int id) {

        if (entities.containsKey(id)) {

            HordeEntity entity = entities.get(id);

            WrapperPlayServerEntityDestroy packet = new WrapperPlayServerEntityDestroy();
            packet.setEntityIds(new int[]{ id });

            for (Player player : entity.listSeers()) {

                packet.sendPacket(player);

            }

            entity.clearSeers();
            entities.remove(id);

        }

    }

    public void updatePosition(HordeEntity entity, boolean rotate) {

        Location location = entity.getLocation();

        WrapperPlayServerEntityTeleport packet = new WrapperPlayServerEntityTeleport();
        packet.setEntityID(entity.getId());
        packet.setX(location.getX());
        packet.setY(location.getY());
        packet.setZ(location.getZ());
        packet.setYaw(location.getYaw());
        packet.setPitch(location.getPitch());
        packet.setOnGround(true);

        for (Player player : entity.listSeers()) {

            packet.sendPacket(player);

        }

        if (rotate) {

            WrapperPlayServerEntityHeadRotation rotation = new WrapperPlayServerEntityHeadRotation();
            rotation.setEntityID(entity.getId());
            rotation.setHeadYaw((byte) Math.round(location.getYaw()));

            for (Player player : entity.listSeers()) {

                rotation.sendPacket(player);

            }

        }

    }

    public void showDamage(HordeEntity entity) {

        WrapperPlayServerAnimation damagePacket = new WrapperPlayServerAnimation();
        damagePacket.setEntityID(entity.getId());
        damagePacket.setAnimation(1); //Damage animation

        for (Player player : entity.listSeers()) {

            damagePacket.sendPacket(player);
            player.playSound(entity.getLocation(), entity.getType().getSound().getHurt(), 1.0F, 1.0F);

        }

    }

    public void die(HordeEntity entity) {

        Player[] seers = entity.listSeers();

        final Game game = gameManager.getGame(seers[0]);

        WrapperPlayServerEntityStatus packet = new WrapperPlayServerEntityStatus();
        packet.setEntityID(entity.getId());
        packet.setEntityStatus((byte) 3); //Death animation

        for (Player player : seers) {

            packet.sendPacket(player);
            player.playSound(entity.getLocation(), entity.getType().getSound().getDeath(), 1.0F, 1.0F);

        }

        final Location loc = entity.getLocation();

        new BukkitRunnable() {

            @Override
            public void run() {

                WrapperPlayServerEntityDestroy destroy = new WrapperPlayServerEntityDestroy();
                destroy.setEntityIds(new int[]{ entity.getId() });

                WrapperPlayServerWorldParticles particles = new WrapperPlayServerWorldParticles();
                particles.setNumberOfParticles(15);
                particles.setParticleType(WrappedParticle.create(Particle.CLOUD, null));
                particles.setX(loc.getX());
                particles.setY(loc.getY() + 1);
                particles.setZ(loc.getZ());
                particles.setOffsetX(0.35F);
                particles.setOffsetY(0.35F);
                particles.setOffsetZ(0.35F);
                particles.setParticleData(0.05F);

                for (Player player : seers) {

                    destroy.sendPacket(player);
                    particles.sendPacket(player);

                }

                removeEntity(entity.getId());

                if (game != null) {

                    game.removeEntity(entity);

                }

            }

        }.runTaskLater(main, 20L);

    }

    public void highlightClassSelectEntity(Player player, int id) {

        for (int i = 0; i < HordePlayerClassType.values().length; i++) {

            WrappedDataWatcher dataWatcher = new WrappedDataWatcher();
            WrappedDataWatcher.Serializer serializer = WrappedDataWatcher.Registry.get(Byte.class);
            dataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, serializer), (byte) (i == id ? 0x40 : 0));

            WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata();
            packet.setEntityID(Integer.MAX_VALUE - i - 1);
            packet.setMetadata(dataWatcher.getWatchableObjects());

            packet.sendPacket(player);

        }

    }

    public void fireEntityUse(Player player, int entityID, EnumWrappers.EntityUseAction action) {

        switch (action) {

            case INTERACT_AT -> {

                Game game = gameManager.getGame(player);

                if (game != null && game.isSelectingClass(player)) {

                    int id = Integer.MAX_VALUE - entityID - 1;

                    HordePlayerClassType[] classes = HordePlayerClassType.values();

                    if (id >= 0 && id < classes.length) {

                        gameManager.getGame(player).setClass(player, classes[id]);

                    }

                }

            }

            case ATTACK -> {

                Game game = gameManager.getGame(player);

                if (game != null && !game.isSelectingClass(player)) {

                    game.damageEntity(player, entityID);

                }
            }
        }
    }

    private Location getForward(Location location, @SuppressWarnings("SameParameterValue") double amount) {

        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        float yaw = location.getYaw();
        float pitch = location.getPitch();

        if (yaw > 45.0F && yaw <= 135.0F) {

            x -= amount;

        } else if (yaw > 135.0F && yaw <= 225.0F) {

            z -= amount;

        } else if (yaw > 225.0F && yaw <= 315.0F) {

            x += amount;

        } else {

            z += amount;

        }

        return new Location(location.getWorld(), x, y, z, yaw, pitch);

    }

    private Location getLeft(Location location, double amount) {

        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        float yaw = location.getYaw();
        float pitch = location.getPitch();

        if (yaw > 45.0F && yaw <= 135.0F) {

            z += amount;

        } else if (yaw > 135.0F && yaw <= 225.0F) {

            x -= amount;

        } else if (yaw > 225.0F && yaw <= 315.0F) {

            z -= amount;

        } else {

            x += amount;

        }

        return new Location(location.getWorld(), x, y, z, yaw, pitch);

    }

    public void setGameManager(GameManager gameManager) {

        this.gameManager = gameManager;

    }

}
