package com.kaixeleron.horde.entity.data;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.entity.EntityManager;
import com.kaixeleron.horde.navigation.data.BlockLocation;
import com.kaixeleron.horde.navigation.data.Path;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class HordeEntity {

    private final HordeMain main;

    private final EntityManager entityManager;

    private final List<Player> seers;

    private final int id;

    private final HordeEntityType type;

    protected final String name;

    private double health;
    private final double speed, armorFactor, weaponFactor;

    private Location location;

    private BukkitRunnable movementRunnable = null;

    public HordeEntity(HordeMain main, EntityManager entityManager, int id, HordeEntityType type, String name, double health, double speed, double armorFactor, double weaponFactor, Location spawn) {

        this.main = main;
        this.entityManager = entityManager;
        this.seers = new ArrayList<>();
        this.id = id;
        this.type = type;
        this.name = name;
        this.health = health;
        this.speed = speed;
        this.armorFactor = armorFactor;
        this.weaponFactor = weaponFactor;
        this.location = spawn;

    }

    public void addSeer(Player seer) {

        seers.add(seer);

    }

    public void removeSeer(Player seer) {

        seers.remove(seer);

    }

    public void clearSeers() {

        seers.clear();

    }

    public Player[] listSeers() {

        return seers.toArray(new Player[0]);

    }

    public int getId() {

        return id;

    }

    public HordeEntityType getType() {

        return type;

    }

    public String getName() {

        return name;

    }

    public double getHealth() {

        return health;

    }

    public Location getLocation() {

        return location;

    }

    public void followPath(final Path path) {

        movementRunnable = new BukkitRunnable() {

            private boolean half = false;

            private final BlockLocation[] points = path.getPoints();

            private int current = 0;

            @Override
            public void run() {

                if (half) {

                    if (current + 1 < points.length) {

                        double oldX = location.getX();
                        double oldZ = location.getZ();

                        location = new Location(points[current].getBukkitLocation().getWorld(), (points[current].getX() + points[current + 1].getX()) / 2.0D, (points[current].getY() + points[current + 1].getY()) / 2.0D, (points[current].getZ() + points[current + 1].getZ()) / 2.0D);
                        location.add(0.5D, 0.0D, 0.5D);

                        double newX = location.getX();
                        double newZ = location.getZ();

                        float angle = (float) Math.toDegrees(Math.atan2(newX - oldX, newZ - oldZ));
                        if (angle < 0.0F) angle += 360.0F;

                        location.setYaw(angle);

                        entityManager.updatePosition(HordeEntity.this, oldX != newX || oldZ != newZ);

                    }

                } else {

                    current++;

                    if (current >= points.length) {

                        cancel();
                        // Reached the goal

                    } else {

                        double oldX = location.getX();
                        double oldZ = location.getZ();

                        location = points[current].getBukkitLocation();
                        location.add(0.5D, 0.0D, 0.5D);

                        double newX = location.getX();
                        double newZ = location.getZ();

                        float angle = (float) Math.toDegrees(Math.atan2(newX - oldX, newZ - oldZ));
                        if (angle < 0.0F) angle += 360.0F;

                        location.setYaw(angle);

                        entityManager.updatePosition(HordeEntity.this, oldX != newX || oldZ != newZ);

                        Sound sound = type.getSound().getWalk();
                        if (sound == null) sound = Sound.BLOCK_GRASS_STEP; //TODO block types

                        for (Player player : seers) {

                            player.playSound(location, sound, 0.15F, 1.0F);

                        }

                    }

                }

                half = !half;

            }

        };

        movementRunnable.runTaskTimer(main, 0L, 3L);

    }

    public void damage(double damage) {

        health -= damage;

        if (health <= 0.0D) {

            if (movementRunnable != null) {

                movementRunnable.cancel();

            }

            entityManager.die(this);

        } else {

            entityManager.showDamage(this);

        }

    }

}
