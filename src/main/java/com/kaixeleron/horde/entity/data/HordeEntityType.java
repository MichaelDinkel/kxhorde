package com.kaixeleron.horde.entity.data;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;

import java.io.Serializable;

public enum HordeEntityType implements Serializable {

    ARMOR_STAND(EntityType.ARMOR_STAND, 1, Material.ARMOR_STAND, "Armor Stand", 10.0D, 1.0D, HordeEntitySound.ARMOR_STAND),
    AXOLOTL(EntityType.AXOLOTL, 3, Material.AXOLOTL_SPAWN_EGG, "Axolotl", 14.0D, 1.0D, HordeEntitySound.AXOLOTL),
    BAT(EntityType.BAT, 4, Material.BAT_SPAWN_EGG, "Bat", 6.0D, 1.0D, HordeEntitySound.BAT),
    BEE(EntityType.BEE, 5, Material.BEE_SPAWN_EGG, "Bee", 10.0D, 1.0D, HordeEntitySound.BEE),
    BLAZE(EntityType.BLAZE, 6, Material.BLAZE_SPAWN_EGG, "Blaze", 20.0D, 1.0D, HordeEntitySound.BLAZE),
    CAT(EntityType.CAT, 8, Material.CAT_SPAWN_EGG, "Cat", 10.0D, 1.0D, HordeEntitySound.CAT),
    CAVE_SPIDER(EntityType.CAVE_SPIDER, 9, Material.CAVE_SPIDER_SPAWN_EGG, "Cave Spider", 12.0D, 1.0D, HordeEntitySound.CAVE_SPIDER),
    CHICKEN(EntityType.CHICKEN, 10, Material.CHICKEN_SPAWN_EGG, "Chicken", 4.0D, 1.0D, HordeEntitySound.CHICKEN),
    COD(EntityType.COD, 11, Material.COD_SPAWN_EGG, "Cod", 3.0D, 1.0D, HordeEntitySound.COD),
    COW(EntityType.COW, 12, Material.COW_SPAWN_EGG, "Cow", 10.0D, 1.0D, HordeEntitySound.COW),
    CREEPER(EntityType.CREEPER, 13, Material.CREEPER_SPAWN_EGG, "Creeper", 20.0D, 1.0D, HordeEntitySound.CREEPER),
    DOLPHIN(EntityType.DOLPHIN, 14, Material.DOLPHIN_SPAWN_EGG, "Dolphin", 10.0D, 1.0D, HordeEntitySound.DOLPHIN),
    DONKEY(EntityType.DONKEY, 15, Material.DONKEY_SPAWN_EGG, "Donkey", 15.0D, 1.0D, HordeEntitySound.DONKEY),
    DROWNED(EntityType.DROWNED, 17, Material.DROWNED_SPAWN_EGG, "Drowned", 20.0D, 1.0D, HordeEntitySound.DROWNED),
    ELDER_GUARDIAN(EntityType.ELDER_GUARDIAN, 18, Material.ELDER_GUARDIAN_SPAWN_EGG, "Elder Guardian", 80.0D, 1.0D, HordeEntitySound.ELDER_GUARDIAN),
    ENDER_DRAGON(EntityType.ENDER_DRAGON, 20, Material.DRAGON_HEAD, "Ender Dragon", 200.0D, 1.0D, HordeEntitySound.ENDER_DRAGON),
    ENDERMAN(EntityType.ENDERMAN, 21, Material.ENDERMAN_SPAWN_EGG, "Enderman", 40.0D, 1.0D, HordeEntitySound.ENDERMAN),
    ENDERMITE(EntityType.ENDERMITE, 22, Material.ENDERMITE_SPAWN_EGG, "Endermite", 8.0D, 1.0D, HordeEntitySound.ENDERMITE),
    EVOKER(EntityType.EVOKER, 23, Material.EVOKER_SPAWN_EGG, "Evoker", 24.0D, 1.0D, HordeEntitySound.EVOKER),
    FOX(EntityType.FOX, 29, Material.FOX_SPAWN_EGG, "Fox", 10.0D, 1.0D, HordeEntitySound.FOX),
    GHAST(EntityType.GHAST, 30, Material.GHAST_SPAWN_EGG, "Ghast", 10.0D, 1.0D, HordeEntitySound.GHAST),
    GIANT(EntityType.GIANT, 31, Material.ZOMBIE_HEAD, "Giant", 100.0D, 1.0D, HordeEntitySound.GIANT),
    GLOW_SQUID(EntityType.GLOW_SQUID, 33, Material.GLOW_SQUID_SPAWN_EGG, "Glow Squid", 10.0D, 1.0D, HordeEntitySound.GLOW_SQUID),
    GOAT(EntityType.GOAT, 34, Material.GOAT_SPAWN_EGG, "Goat", 10.0D, 1.0D, HordeEntitySound.GOAT),
    GUARDIAN(EntityType.GUARDIAN, 35, Material.GUARDIAN_SPAWN_EGG, "Guardian", 30.0D, 1.0D, HordeEntitySound.GUARDIAN),
    HOGLIN(EntityType.HOGLIN, 36, Material.HOGLIN_SPAWN_EGG, "Hoglin", 40, 1.0, HordeEntitySound.HOGLIN),
    HORSE(EntityType.HORSE, 37, Material.HORSE_SPAWN_EGG, "Horse", 20.0D, 1.0D, HordeEntitySound.HORSE),
    HUSK(EntityType.HUSK, 38, Material.HUSK_SPAWN_EGG, "Husk", 20.0D, 1.0D, HordeEntitySound.HUSK),
    ILLUSIONER(EntityType.ILLUSIONER, 39, Material.LAPIS_ORE, "Illusioner", 32.0D, 1.0D, HordeEntitySound.ILLUSIONER),
    IRON_GOLEM(EntityType.IRON_GOLEM, 40, Material.IRON_BLOCK, "Iron Golem", 100.0D, 0.5D, HordeEntitySound.IRON_GOLEM),
    LLAMA(EntityType.LLAMA, 46, Material.LLAMA_SPAWN_EGG, "Llama", 20.0D, 1.0D, HordeEntitySound.LLAMA),
    MAGMA_CUBE(EntityType.MAGMA_CUBE, 48, Material.MAGMA_CUBE_SPAWN_EGG, "Magma Cube", 16.0D, 1.0D, HordeEntitySound.MAGMA_CUBE),
    MULE(EntityType.MULE, 57, Material.MULE_SPAWN_EGG, "Mule", 20.0D, 1.0D, HordeEntitySound.MULE),
    MOOSHROOM(EntityType.MUSHROOM_COW, 58, Material.MOOSHROOM_SPAWN_EGG, "Mooshroom", 10.0D, 1.0D, HordeEntitySound.MOOSHROOM),
    OCELOT(EntityType.OCELOT, 59, Material.OCELOT_SPAWN_EGG, "Ocelot", 10.0D, 1.0D, HordeEntitySound.OCELOT),
    PANDA(EntityType.PANDA, 61, Material.PANDA_SPAWN_EGG, "Panda", 20.0D, 1.0D, HordeEntitySound.PANDA),
    PARROT(EntityType.PARROT, 62, Material.PARROT_SPAWN_EGG, "Parrot", 6.0D, 1.0D, HordeEntitySound.PARROT),
    PHANTOM(EntityType.PHANTOM, 63, Material.PHANTOM_SPAWN_EGG, "Phantom", 20.0D, 1.0D, HordeEntitySound.PHANTOM),
    PIG(EntityType.PIG, 64, Material.PIG_SPAWN_EGG, "Pig", 10.0D, 1.0D, HordeEntitySound.PIG),
    PIGLIN(EntityType.PIGLIN, 65, Material.PIGLIN_SPAWN_EGG, "Piglin", 16.0D, 1.0D, HordeEntitySound.PIGLIN),
    PIGLIN_BRUTE(EntityType.PIGLIN_BRUTE, 66, Material.PIGLIN_BRUTE_SPAWN_EGG, "Piglin Brute", 50.0D, 1.0D, HordeEntitySound.PIGLIN_BRUTE),
    PILLAGER(EntityType.PILLAGER, 67, Material.PILLAGER_SPAWN_EGG, "Pillager", 24.0D, 1.0D, HordeEntitySound.PILLAGER),
    PLAYER(EntityType.PLAYER, -1, Material.PLAYER_HEAD, "Player", 20.0D, 1.0D, HordeEntitySound.PLAYER),
    POLAR_BEAR(EntityType.POLAR_BEAR, 68, Material.POLAR_BEAR_SPAWN_EGG, "Polar Bear", 30.0D, 1.0D, HordeEntitySound.POLAR_BEAR),
    PUFFERFISH(EntityType.PUFFERFISH, 70, Material.PUFFERFISH_SPAWN_EGG, "Pufferfish", 3.0D, 1.0D, HordeEntitySound.PUFFERFISH),
    RABBIT(EntityType.RABBIT, 71, Material.RABBIT_SPAWN_EGG, "Rabbit", 3.0D, 1.0D, HordeEntitySound.RABBIT),
    RAVAGER(EntityType.RAVAGER, 72, Material.RAVAGER_SPAWN_EGG, "Ravager", 100.0D, 1.0D, HordeEntitySound.RAVAGER),
    SALMON(EntityType.SALMON, 73, Material.SALMON_SPAWN_EGG, "Salmon", 3.0D, 1.0D, HordeEntitySound.SALMON),
    SHEEP(EntityType.SHEEP, 74, Material.SHEEP_SPAWN_EGG, "Sheep", 8.0D, 1.0D, HordeEntitySound.SHEEP),
    SHULKER(EntityType.SHULKER, 75, Material.SHULKER_SPAWN_EGG, "Shulker", 30.0D, 1.0D, HordeEntitySound.SHULKER),
    SILVERFISH(EntityType.SILVERFISH, 77, Material.SILVERFISH_SPAWN_EGG, "Silverfish", 8.0D, 1.0D, HordeEntitySound.SILVERFISH),
    SKELETON(EntityType.SKELETON, 78, Material.SKELETON_SPAWN_EGG, "Skeleton", 20.0D, 1.0D, HordeEntitySound.SKELETON),
    SKELETON_HORSE(EntityType.SKELETON_HORSE, 79, Material.SKELETON_HORSE_SPAWN_EGG, "Skeleton Horse", 15.0D, 1.0D, HordeEntitySound.SKELETON_HORSE),
    SLIME(EntityType.SLIME, 80, Material.SLIME_SPAWN_EGG, "Slime", 16.0D, 1.0D, HordeEntitySound.SLIME),
    SNOW_GOLEM(EntityType.SNOWMAN, 82, Material.SNOW_BLOCK, "Snow Golem", 4.0D, 1.0D, HordeEntitySound.SNOW_GOLEM),
    SPIDER(EntityType.SPIDER, 85, Material.SPIDER_SPAWN_EGG, "Spider", 16.0D, 1.0D, HordeEntitySound.SPIDER),
    SQUID(EntityType.SQUID, 86, Material.SQUID_SPAWN_EGG, "Squid", 10.0D, 1.0D, HordeEntitySound.SQUID),
    STRAY(EntityType.STRAY, 87, Material.STRAY_SPAWN_EGG, "Stray", 20.0D, 1.0D, HordeEntitySound.STRAY),
    STRIDER(EntityType.STRIDER, 88, Material.STRIDER_SPAWN_EGG, "Strider", 20.0D, 1.0D, HordeEntitySound.STRIDER),
    TRADER_LLAMA(EntityType.TRADER_LLAMA, 94, Material.TRADER_LLAMA_SPAWN_EGG, "Trader Llama", 20.0D, 1.0D, HordeEntitySound.TRADER_LLAMA),
    TROPICAL_FISH(EntityType.TROPICAL_FISH, 95, Material.TROPICAL_FISH_SPAWN_EGG, "Tropical Fish", 3.0D, 1.0D, HordeEntitySound.TROPICAL_FISH),
    TURTLE(EntityType.TURTLE, 96, Material.TURTLE_SPAWN_EGG, "Turtle", 30.0D, 1.0D, HordeEntitySound.TURTLE),
    VEX(EntityType.VEX, 97, Material.VEX_SPAWN_EGG, "Vex", 14.0D, 1.0D, HordeEntitySound.VEX),
    VILLAGER(EntityType.VILLAGER, 98, Material.VILLAGER_SPAWN_EGG, "Villager", 20.0D, 1.0D, HordeEntitySound.VILLAGER),
    VINDICATOR(EntityType.VINDICATOR, 99, Material.VINDICATOR_SPAWN_EGG, "Vindicator", 24.0D, 1.0D, HordeEntitySound.VINDICATOR),
    WANDERING_TRADER(EntityType.WANDERING_TRADER, 100, Material.WANDERING_TRADER_SPAWN_EGG, "Wandering Trader", 20.0D, 1.0D, HordeEntitySound.WANDERING_TRADER),
    WITCH(EntityType.WITCH, 101, Material.WITCH_SPAWN_EGG, "Witch", 26.0D, 1.0D, HordeEntitySound.WITCH),
    WITHER(EntityType.WITHER, 102, Material.WITHER_SKELETON_SKULL, "Wither", 300.0D, 1.0D, HordeEntitySound.WITHER),
    WITHER_SKELETON(EntityType.WITHER_SKELETON, 103, Material.WITHER_SKELETON_SPAWN_EGG, "Wither Skeleton", 20.0D, 1.0D, HordeEntitySound.WITHER_SKELETON),
    WOLF(EntityType.WOLF, 105, Material.WOLF_SPAWN_EGG, "Wolf", 8.0D, 1.0D, HordeEntitySound.WOLF),
    ZOGLIN(EntityType.ZOGLIN, 106, Material.ZOGLIN_SPAWN_EGG, "Zoglin", 20.0D, 1.0D, HordeEntitySound.ZOGLIN),
    ZOMBIE(EntityType.ZOMBIE, 107, Material.ZOMBIE_SPAWN_EGG, "Zombie", 20.0D, 1.0D, HordeEntitySound.ZOMBIE),
    ZOMBIE_HORSE(EntityType.ZOMBIE_HORSE, 108, Material.ZOMBIE_HORSE_SPAWN_EGG, "Zombie Horse", 15.0D, 1.0D, HordeEntitySound.ZOMBIE_HORSE),
    ZOMBIE_VILLAGER(EntityType.ZOMBIE_VILLAGER, 109, Material.ZOMBIE_VILLAGER_SPAWN_EGG, "Zombie Villager", 20.0D, 1.0D, HordeEntitySound.ZOMBIE_VILLAGER),
    ZOMBIFIED_PIGLIN(EntityType.ZOMBIFIED_PIGLIN, 110, Material.ZOMBIFIED_PIGLIN_SPAWN_EGG, "Zombie Pigman", 20.0D, 1.0D, HordeEntitySound.ZOMBIFIED_PIGLIN);

    private final EntityType bukkitType;

    private final int networkId;

    private final Material menuMaterial;

    private final String friendlyName;

    private final double standardHealth, standardSpeed;
    
    private final HordeEntitySound sound;

    HordeEntityType(EntityType bukkitType, int networkId, Material menuMaterial, String friendlyName, double standardHealth, double standardSpeed, HordeEntitySound sound) {

        this.bukkitType = bukkitType;
        this.networkId = networkId;
        this.menuMaterial = menuMaterial;
        this.friendlyName = friendlyName;
        this.standardHealth = standardHealth;
        this.standardSpeed = standardSpeed;
        this.sound = sound;

    }

    public EntityType getBukkitType() {

        return bukkitType;

    }

    public int getNetworkId() {

        return networkId;

    }

    public Material getMenuMaterial() {

        return menuMaterial;

    }

    public String getFriendlyName() {

        return friendlyName;

    }

    public static HordeEntityType getByMenuMaterial(Material menuMaterial) {

        HordeEntityType result = null;

        HordeEntityType[] types = values();

        int i = 0;

        while (result == null && i < types.length) {

            if (types[i].menuMaterial.equals(menuMaterial)) {

                result = types[i];

            }

            i++;

        }

        return result;

    }

    public double getStandardHealth() {

        return standardHealth;

    }

    public double getStandardSpeed() {

        return standardSpeed;

    }

    public HordeEntitySound getSound() {
        
        return sound;
        
    }
}
