package com.kaixeleron.horde;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.kaixeleron.horde.buildings.BuildingManager;
import com.kaixeleron.horde.chat.ChatManager;
import com.kaixeleron.horde.database.DatabaseException;
import com.kaixeleron.horde.database.DatabaseManager;
import com.kaixeleron.horde.database.sql.SQLDatabase;
import com.kaixeleron.horde.database.yaml.YAMLDatabase;
import com.kaixeleron.horde.entity.EntityManager;
import com.kaixeleron.horde.entity.listener.PacketPlayInUseEntityListener;
import com.kaixeleron.horde.game.GameManager;
import com.kaixeleron.horde.game.command.CommandHorde;
import com.kaixeleron.horde.game.command.CommandReady;
import com.kaixeleron.horde.game.command.tabcomplete.TabCompleteReady;
import com.kaixeleron.horde.game.listener.GameListener;
import com.kaixeleron.horde.game.listener.ProtectionListener;
import com.kaixeleron.horde.game.listener.SignListener;
import com.kaixeleron.horde.items.ItemManager;
import com.kaixeleron.horde.items.listener.ItemDisconnectListener;
import com.kaixeleron.horde.items.listener.ItemListener;
import com.kaixeleron.horde.localization.LocalizationManager;
import com.kaixeleron.horde.map.MapManager;
import com.kaixeleron.horde.map.command.CommandMap;
import com.kaixeleron.horde.map.command.tabcomplete.TabCompleteMap;
import com.kaixeleron.horde.mission.MissionManager;
import com.kaixeleron.horde.mission.command.CommandMission;
import com.kaixeleron.horde.mission.command.tabcomplete.TabCompleteMission;
import com.kaixeleron.horde.mission.listener.ConfiguratorListener;
import com.kaixeleron.horde.mission.util.MissionConfigurator;
import com.kaixeleron.horde.navigation.GraphManager;
import com.kaixeleron.horde.navigation.command.CommandNavMesh;
import com.kaixeleron.horde.navigation.command.tabcomplete.TabCompleteNavMesh;
import com.kaixeleron.horde.navigation.listener.WandListener;
import com.kaixeleron.horde.party.PartyManager;
import com.kaixeleron.horde.party.command.CommandParty;
import com.kaixeleron.horde.party.command.CommandPartyChat;
import com.kaixeleron.horde.party.command.tabcomplete.TabCompleteParty;
import com.kaixeleron.horde.party.command.tabcomplete.TabCompletePartyChat;
import com.kaixeleron.horde.party.listener.PartyListener;
import com.kaixeleron.horde.permission.PermissionManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public class HordeMain extends JavaPlugin {

    private DatabaseManager databaseManager;

    private BuildingManager buildingManager;
    private ChatManager chatManager;
    private ItemManager itemManager;
    private EntityManager entityManager;
    private GameManager gameManager;
    private GraphManager graphManager;
    private LocalizationManager localizationManager;
    private MapManager mapManager;
    private MissionManager missionManager;
    private PartyManager partyManager;
    private PermissionManager permissionManager;

    private MissionConfigurator missionConfigurator;

    @Override
    public void onLoad() {

        saveDefaultConfig();

        try {

            if (getConfig().getBoolean("sql.enabled")) {

                databaseManager = new SQLDatabase(getConfig().getString("sql.host"), getConfig().getInt("sql.port"), getConfig().getString("sql.database"), getConfig().getString("sql.username"), getConfig().getString("sql.password"));

            } else {

                databaseManager = new YAMLDatabase(getDataFolder());

            }

        } catch (DatabaseException e) {

            System.err.println("Could not set up database connection.");
            e.printStackTrace();

        }

        buildingManager = new BuildingManager(this);
        chatManager = new ChatManager();
        itemManager = new ItemManager(this, buildingManager);
        entityManager = new EntityManager(this, itemManager);
        gameManager = new GameManager(this, chatManager, entityManager, itemManager, getConfig().getInt("gameSize"));
        entityManager.setGameManager(gameManager);
        graphManager = new GraphManager(this, getDataFolder());
        localizationManager = new LocalizationManager(getDataFolder());
        missionManager = new MissionManager(getDataFolder());
        mapManager = new MapManager(getDataFolder(), graphManager);
        partyManager = new PartyManager(chatManager, getConfig().getInt("gameSize"));
        chatManager.setPartyManager(partyManager);

        missionConfigurator = new MissionConfigurator(this, missionManager);

    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onEnable() {

        try {

            graphManager.loadGraphs();
            graphManager.loadPaths();
            missionManager.loadMissions();
            mapManager.loadMaps();

        } catch (IOException e) {

            System.err.println("Could not load graphs from files.");
            e.printStackTrace();

        }

        buildingManager.startTask();
        itemManager.startRefreshRunnable();

        ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();

        permissionManager = new PermissionManager(this);
        permissionManager.setConsolePermissions();

        getCommand("navmesh").setExecutor(new CommandNavMesh(this, graphManager));
        getCommand("navmesh").setTabCompleter(new TabCompleteNavMesh(graphManager));
        getServer().getPluginManager().registerEvents(new WandListener(this, graphManager), this);

        getCommand("map").setExecutor(new CommandMap(graphManager, mapManager, missionManager));
        getCommand("map").setTabCompleter(new TabCompleteMap(graphManager, mapManager, missionManager));

        getCommand("mission").setExecutor(new CommandMission(chatManager, missionManager, missionConfigurator));
        getCommand("mission").setTabCompleter(new TabCompleteMission(missionManager));
        getServer().getPluginManager().registerEvents(new ConfiguratorListener(missionConfigurator), this);

        getCommand("horde").setExecutor(new CommandHorde(gameManager, localizationManager, mapManager, partyManager));
        getCommand("ready").setExecutor(new CommandReady(gameManager));
        getCommand("ready").setTabCompleter(new TabCompleteReady());

        GameListener gameListener = new GameListener(this, gameManager);
        getServer().getPluginManager().registerEvents(gameListener, this);
        getServer().getPluginManager().registerEvents(new ProtectionListener(getConfig().getStringList("allowedCommands").toArray(new String[0]), gameManager), this);
        getServer().getPluginManager().registerEvents(new SignListener(this), this);

        getCommand("party").setExecutor(new CommandParty(databaseManager, partyManager));
        getCommand("party").setTabCompleter(new TabCompleteParty(partyManager));
        getCommand("p").setExecutor(new CommandPartyChat(chatManager, partyManager));
        getCommand("p").setTabCompleter(new TabCompletePartyChat());
        getServer().getPluginManager().registerEvents(new PartyListener(databaseManager, partyManager), this);

        getServer().getPluginManager().registerEvents(new ItemListener(itemManager), this);
        getServer().getPluginManager().registerEvents(new ItemDisconnectListener(itemManager), this);

        protocolManager.addPacketListener(gameListener);
        protocolManager.addPacketListener(new PacketPlayInUseEntityListener(this, entityManager));

    }

    @Override
    public void onDisable() {

        buildingManager.clearBuildings();
        buildingManager.stopTask();
        gameManager.clearGames();
        graphManager.clearPositions();
        graphManager.stopAllVisualizing();
        graphManager.clearGraphs();
        itemManager.clearPlayers();
        itemManager.stopRefreshRunnable();
        mapManager.clearMaps();
        missionManager.clearMissions();
        partyManager.disbandAllParties();
        partyManager.clearMuted();
        permissionManager.detach();

        missionConfigurator.clearConfiguring();

    }

}
