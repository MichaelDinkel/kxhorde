package com.kaixeleron.horde.navigation;

import com.kaixeleron.horde.navigation.data.BlockLocation;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Node;
import com.kaixeleron.horde.navigation.data.Path;
import org.bukkit.Location;

import java.util.*;

public class Pathfinder {

    private final Location start, end;

    private final Graph graph;

    public Pathfinder(Location start, Location end, Graph graph) {

        this.start = start;
        this.end = end;
        this.graph = graph;

    }

    public Path findPath(String name) {

        List<BlockLocation> locations = new ArrayList<>();

        Map<BlockLocation, Node> nodes = graph.getNodes();

        Node startNode = nodes.get(new BlockLocation(start));

        if (startNode != null) {

            Map<BlockLocation, BlockLocation> parents = new HashMap<>();

            if (startNode.getLocation().getBukkitLocation().equals(end)) {

                parents.put(new BlockLocation(start), new BlockLocation(end));

            } else {

                List<BlockLocation> visited = new ArrayList<>();

                Map<BlockLocation, Integer> queue1 = new HashMap<>(startNode.getAdjacent()), queue2 = new HashMap<>();

                boolean found = false, invalid = false;

                while (!found && !invalid) {

                    queue2.clear();

                    for (BlockLocation location : queue1.keySet()) {

                        Node node = graph.getNodes().get(location);

                        if (node != null) {

                            Map<BlockLocation, Integer> adjacent = graph.getNodes().get(location).getAdjacent();

                            for (BlockLocation adjLoc : adjacent.keySet()) {

                                if (!visited.contains(adjLoc)) {

                                    queue2.put(adjLoc, adjacent.get(adjLoc));
                                    parents.put(adjLoc, node.getLocation());
                                    visited.add(adjLoc);

                                }

                            }

                            if (node.getLocation().getBukkitLocation().equals(end)) {

                                found = true;

                            }

                        }

                    }

                    queue1.clear();

                    invalid = queue2.isEmpty();

                    if (!found && !invalid) {

                        for (BlockLocation location : queue2.keySet()) {

                            Node node = graph.getNodes().get(location);

                            if (node != null) {

                                Map<BlockLocation, Integer> adjacent = graph.getNodes().get(location).getAdjacent();

                                for (BlockLocation adjLoc : adjacent.keySet()) {

                                    if (!visited.contains(adjLoc)) {

                                        queue1.put(adjLoc, adjacent.get(adjLoc));
                                        parents.put(adjLoc, node.getLocation());
                                        visited.add(adjLoc);

                                    }

                                }

                                if (node.getLocation().getBukkitLocation().equals(end)) {

                                    found = true;

                                }

                            }

                        }

                    }

                }

                BlockLocation location = parents.get(new BlockLocation(end));

                while (location != null) {

                    locations.add(location);

                    BlockLocation pop = location;

                    location = parents.get(location);

                    parents.remove(pop);

                }

                Collections.reverse(locations);

            }

        }

        List<BlockLocation> smoothed = new ArrayList<>();

        for (int i = 0; i < locations.size(); i++) {

            BlockLocation current = locations.get(i);
            BlockLocation next = locations.get(i + 1 >= locations.size() ? i : i + 1);

            smoothed.add(locations.get(i));

            if (next.getY() != current.getY()) {

                if (next.getY() > current.getY()) {

                    smoothed.add(new BlockLocation(current.getWorld(), current.getX(), next.getY(), current.getZ()));

                } else {

                    smoothed.add(new BlockLocation(current.getWorld(), next.getX(), current.getY(), next.getZ()));

                }

            }

        }

        return new Path(name, smoothed.toArray(new BlockLocation[0]));

    }

}
