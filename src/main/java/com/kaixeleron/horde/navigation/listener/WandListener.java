package com.kaixeleron.horde.navigation.listener;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.navigation.GraphManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class WandListener implements Listener {

    private final HordeMain main;

    private final GraphManager manager;

    private final List<Player> debounce;

    public WandListener(HordeMain main, GraphManager manager) {

        this.main = main;
        this.manager = manager;

        debounce = new ArrayList<>();

    }

    @SuppressWarnings("ConstantConditions")
    @EventHandler
    public void onClick(PlayerInteractEvent event) {

        if (event.getPlayer().hasPermission("kxhorde.command.navmesh") && event.getPlayer().getInventory().getItemInMainHand().equals(manager.getWand())) {

            if (!debounce.contains(event.getPlayer())) {

                if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {

                    Location position = event.getClickedBlock().getLocation();

                    manager.setFirstPosition(event.getPlayer(), position);
                    event.getPlayer().sendMessage("First position set to " + ChatColor.GRAY + position.getBlockX() + ChatColor.RESET + ", " + ChatColor.GRAY + position.getBlockY() + ChatColor.RESET + ", " + ChatColor.GRAY + position.getBlockZ() + ChatColor.RESET + ".");

                    event.setCancelled(true);
                    debounce(event.getPlayer());

                } else if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

                    Location position = event.getClickedBlock().getLocation();

                    manager.setSecondPosition(event.getPlayer(), position);
                    event.getPlayer().sendMessage("Second position set to " + ChatColor.GRAY + position.getBlockX() + ChatColor.RESET + ", " + ChatColor.GRAY + position.getBlockY() + ChatColor.RESET + ", " + ChatColor.GRAY + position.getBlockZ() + ChatColor.RESET + ".");

                    event.setCancelled(true);
                    debounce(event.getPlayer());

                }

            }

        }

    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {

        if (event.getItemDrop().getItemStack().equals(manager.getWand())) {

            event.getItemDrop().remove();

        }

    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.getCurrentItem() != null) {

            switch (event.getInventory().getType()) {

                case CHEST:
                case DISPENSER:
                case DROPPER:
                case FURNACE:
                case ENCHANTING:
                case MERCHANT:
                case ENDER_CHEST:
                case ANVIL:
                case HOPPER:
                case SHULKER_BOX:
                case BARREL:
                case BLAST_FURNACE:
                case SMOKER:
                case GRINDSTONE:

                    if (event.getCurrentItem().equals(manager.getWand())) {

                        event.setCancelled(true);

                    }

                    break;

            }

        }

    }

    @SuppressWarnings("unused")
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        manager.clearPositions(event.getPlayer());

    }

    @SuppressWarnings("unused")
    @EventHandler
    public void onQuit(PlayerKickEvent event) {

        manager.clearPositions(event.getPlayer());

    }

    private void debounce(final Player player) {

        debounce.add(player);

        new BukkitRunnable() {

            @Override
            public void run() {

                debounce.remove(player);

            }

        }.runTaskLater(main, 1L);

    }

}
