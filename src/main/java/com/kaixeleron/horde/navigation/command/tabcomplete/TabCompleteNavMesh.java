package com.kaixeleron.horde.navigation.command.tabcomplete;

import com.kaixeleron.horde.navigation.GraphManager;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Path;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TabCompleteNavMesh implements TabCompleter {

    private final String[] arg1Suggestions;

    private final GraphManager graphManager;

    public TabCompleteNavMesh(GraphManager graphManager) {

        arg1Suggestions = new String[]{"wand", "generate", "visualize", "delete", "list", "findpath", "visualizepath", "deletepath", "listpaths"};

        this.graphManager = graphManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

        List<String> suggestions = new ArrayList<>();

        switch (args.length) {

            case 1:

                if (args[0].length() == 0) {

                    suggestions.addAll(Arrays.asList(arg1Suggestions));

                } else {

                    for (String suggestion : arg1Suggestions) {

                        if (suggestion.startsWith(args[0].toLowerCase())) {

                            suggestions.add(suggestion);

                        }

                    }

                }

                break;

            case 2:

                switch (args[0].toLowerCase()) {

                    case "generate":

                        if (args[1].length() > 0) {

                            suggestions.add(args[1]);

                        } else {

                            suggestions.add("name");

                        }

                        break;

                    case "visualize":
                    case "delete":
                    case "findpath":
                    case "visualizepath":
                    case "deletepath":
                    case "listpaths":

                        if (args[1].length() == 0) {

                            for (Graph graph : graphManager.listGraphs()) {

                                suggestions.add(graph.getName());

                            }

                        } else {

                            for (Graph graph : graphManager.listGraphs()) {

                                if (graph.getName().startsWith(args[1])) {

                                    suggestions.add(graph.getName());

                                }

                            }

                        }

                        break;

                }

                break;

            case 3:

                switch (args[0].toLowerCase()) {

                    case "findpath":

                        if (args[2].length() > 0) {

                            suggestions.add(args[2]);

                        } else {

                            suggestions.add("name");

                        }

                        break;

                    case "visualizepath":
                    case "deletepath":

                        Graph graph = graphManager.getGraph(args[1]);

                        if (args[2].length() == 0) {

                            if (graph.getPaths() != null) {

                                for (Path path : graph.getPaths()) {

                                    suggestions.add(path.getName());

                                }

                            }

                        } else {

                            if (graph.getPaths() != null) {

                                for (Path path : graph.getPaths()) {

                                    if (path.getName().startsWith(args[2])) {

                                        suggestions.add(path.getName());

                                    }

                                }

                            }

                        }

                        break;

                }

                break;

        }

        return suggestions;

    }

}
