package com.kaixeleron.horde.navigation.command;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.navigation.GraphManager;
import com.kaixeleron.horde.navigation.Pathfinder;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Path;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import javax.annotation.Nullable;
import java.util.List;

public class CommandNavMesh implements CommandExecutor {

    private final HordeMain main;

    private final GraphManager graphManager;

    public CommandNavMesh(HordeMain main, GraphManager graphManager) {

        this.main = main;
        this.graphManager = graphManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        switch (args.length) {

            case 0:

                help(sender, label);
                break;

            case 1:

                switch (args[0].toLowerCase()) {
                    case "wand" -> wand(sender);
                    case "generate" -> generate(sender, label, null);
                    case "visualize" -> visualize(sender, label, null);
                    case "delete" -> delete(sender, label, null);
                    case "list" -> list(sender);
                    case "findpath" -> findPath(sender, label, null, null);
                    case "visualizepath" -> visualizePath(sender, label, null, null);
                    case "deletepath" -> deletePath(sender, label, null, null);
                    case "listpaths" -> listPaths(sender, null);
                    default -> help(sender, label);
                }

                break;

            case 2:

                switch (args[0].toLowerCase()) {
                    case "wand" -> wand(sender);
                    case "generate" -> generate(sender, label, args[1]);
                    case "visualize" -> visualize(sender, label, args[1]);
                    case "delete" -> delete(sender, label, args[1]);
                    case "list" -> list(sender);
                    case "findpath" -> findPath(sender, label, args[1], null);
                    case "visualizepath" -> visualizePath(sender, label, args[1], null);
                    case "deletepath" -> deletePath(sender, label, args[1], null);
                    case "listpaths" -> listPaths(sender, args[1]);
                    default -> help(sender, label);
                }

                break;

            default:

                switch (args[0].toLowerCase()) {
                    case "wand" -> wand(sender);
                    case "generate" -> generate(sender, label, args[1]);
                    case "visualize" -> visualize(sender, label, args[1]);
                    case "delete" -> delete(sender, label, args[1]);
                    case "list" -> list(sender);
                    case "findpath" -> findPath(sender, label, args[1], args[2]);
                    case "visualizepath" -> visualizePath(sender, label, args[1], args[2]);
                    case "deletepath" -> deletePath(sender, label, args[1], args[2]);
                    case "listpaths" -> listPaths(sender, args[1]);
                    default -> help(sender, label);
                }

                break;

        }

        return true;

    }

    private void help(CommandSender sender, String label) {

        sender.sendMessage("kxHorde navmesh management:");

        if (sender instanceof Player) {

            sender.sendMessage(ChatColor.GRAY + "/" + label + " wand " + ChatColor.RESET + "- Spawn a navmesh wand.");
            sender.sendMessage(ChatColor.GRAY + "/" + label + " generate <name> " + ChatColor.RESET + "- Generate a navmesh.");
            sender.sendMessage(ChatColor.GRAY + "/" + label + " visualize <name> " + ChatColor.RESET + "- Visualize a navmesh.");

        }

        sender.sendMessage(ChatColor.GRAY + "/" + label + " delete <name> " + ChatColor.RESET + "- Delete a navmesh.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " list " + ChatColor.RESET + "- List navmeshes.");

        if (sender instanceof Player) {

            sender.sendMessage(ChatColor.GRAY + "/" + label + " findpath <mesh> <name> " + ChatColor.RESET + "- Find a path on a navmesh.");
            sender.sendMessage(ChatColor.GRAY + "/" + label + " visualizepath <mesh> <name> " + ChatColor.RESET + "- Visualize a path.");

        }

        sender.sendMessage(ChatColor.GRAY + "/" + label + " deletepath <mesh> <name> " + ChatColor.RESET + "- Delete a path.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " listpaths [mesh] " + ChatColor.RESET + "- List paths.");

    }

    private void wand(CommandSender sender) {

        if (sender instanceof Player player) {

            player.getInventory().addItem(graphManager.getWand());
            sender.sendMessage("Left click for position 1. Right click for position 2.");

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

    @SuppressWarnings("ConstantConditions")
    private void generate(CommandSender sender, String label, @Nullable String name) {

        if (sender instanceof Player player) {

            if (name == null) {

                sender.sendMessage("Generate a navmesh based on a region.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " generate <name>");

            } else {

                Location[] positions = graphManager.getPositions(player);

                if (positions[0] == null || positions[1] == null || !positions[0].getWorld().equals(positions[1].getWorld())) {

                    sender.sendMessage(ChatColor.RED + "You must define a region first.");

                } else {

                    if (graphManager.graphExists(name)) {

                        sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + name + ChatColor.RED + " already exists.");

                    } else {

                        sender.sendMessage("Generating a navmesh. This may take a long time.");

                        Graph graph = graphManager.getGraphGenerator().generateGraph(name, positions[0], positions[1]);

                        sender.sendMessage("Navmesh generated. Saving...");

                        graphManager.saveGraph(player, graph);

                        sender.sendMessage("Navmesh saved.");

                        graphManager.addGraph(graph);

                    }

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

    private void visualize(CommandSender sender, String label, @Nullable String name) {

        if (sender instanceof Player player) {

            if (name == null) {

                sender.sendMessage("Visualize a navmesh.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " visualize <name>");

            } else {

                Graph graph = graphManager.getGraph(name);

                if (graph == null) {

                    sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + name + ChatColor.RED + " does not exist.");

                } else {

                    if (graphManager.getVisualizedGraph(player) != null && graphManager.getVisualizedGraph(player).equals(graph)) {

                        graphManager.stopVisualizingGraph(player);

                        sender.sendMessage("Stopped visualizing navmesh.");

                    } else {

                        graphManager.stopVisualizingPath(player);
                        graphManager.visualizeGraph(player, graph);

                        sender.sendMessage("Visualizing navmesh " + ChatColor.GRAY + graph.getName() + ChatColor.RESET + ". Type " + ChatColor.GRAY + "/" + label + " visualize " + name + ChatColor.RESET + " to stop.");

                    }

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

    private void delete(CommandSender sender, String label, @Nullable String name) {

        if (name == null) {

            sender.sendMessage("Delete a navmesh.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " delete <name>");

        } else {

            Graph graph = graphManager.getGraph(name);

            if (graph == null) {

                sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + name + ChatColor.RED + " does not exist.");

            } else {

                if (graphManager.deleteGraph(graph)) {

                    graphManager.removeGraph(graph);
                    sender.sendMessage("Navmesh deleted.");

                } else {

                    sender.sendMessage(ChatColor.RED + "The navmesh " + ChatColor.RESET + name + ChatColor.RED + " could not be deleted.");

                }

            }

        }

    }

    private void list(CommandSender sender) {

        List<Graph> graphs = graphManager.listGraphs();

        sender.sendMessage("There " + (graphs.size() == 1 ? "is 1 navmesh" : "are " + graphs.size() + " navmeshes") + " loaded" + (graphs.size() == 0 ? "." : ":"));

        for (Graph graph : graphs) {

            sender.sendMessage(ChatColor.GRAY + " - " + ChatColor.RESET + graph.getName());

        }

    }

    @SuppressWarnings("ConstantConditions")
    private void findPath(CommandSender sender, String label, @Nullable String mesh, @Nullable String name) {

        if (sender instanceof Player player) {

            if (mesh == null || name == null) {

                sender.sendMessage("Find a path on a navmesh.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " findpath <mesh> <name>");

            } else {

                Graph graph = graphManager.getGraph(mesh);

                if (graph == null) {

                    sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + mesh + ChatColor.RED + " does not exist.");

                } else {

                    boolean exists = false;

                    int i = 0;

                    while (i < graph.getPaths().size() && !exists) {

                        if (graph.getPaths().get(i).getName().equals(name)) {

                            exists = true;

                        }

                        i++;

                    }

                    if (exists) {

                        sender.sendMessage(ChatColor.RED + "The navmesh " + ChatColor.RESET + mesh + ChatColor.RED + " already has a path named " + ChatColor.RESET + name + ChatColor.RED + ".");

                    } else {

                        Location[] positions = graphManager.getPositions(player);

                        if (positions[0] == null || positions[1] == null || !positions[0].getWorld().equals(positions[1].getWorld())) {

                            sender.sendMessage(ChatColor.RED + "You must define a start and end point first.");

                        } else {

                            sender.sendMessage("Attempting to find a path between the specified points...");

                            new Thread(() -> {

                                Path path = new Pathfinder(positions[0], positions[1], graph).findPath(name);

                                new BukkitRunnable() {

                                    @Override
                                    public void run() {

                                        if (path.getPoints().length == 0) {

                                            sender.sendMessage(ChatColor.RED + "A path could not be found between the specified points.");

                                        } else {

                                            sender.sendMessage("Path found. Saving...");

                                            graphManager.savePath((Player) sender, path, graph);
                                            graph.addPath(path);

                                            sender.sendMessage("Path saved.");

                                        }

                                    }

                                }.runTask(main);

                            }).start();

                        }

                    }

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

    private void visualizePath(CommandSender sender, String label, @Nullable String mesh, @Nullable String name) {

        if (sender instanceof Player player) {

            if (mesh == null || name == null) {

                sender.sendMessage("Visualize a path on a navmesh.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " visualizepath <mesh> <name>");

            } else {

                Graph graph = graphManager.getGraph(mesh);

                if (graph == null) {

                    sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + mesh + ChatColor.RED + " does not exist.");

                } else {

                    List<Path> paths = graph.getPaths();

                    Path path = null;
                    int i = 0;

                    while (path == null && i < paths.size()) {

                        if (paths.get(i).getName().equals(name)) {

                            path = paths.get(i);

                        }

                        i++;

                    }

                    if (path == null) {

                        sender.sendMessage(ChatColor.RED + "The navmesh " + ChatColor.RESET + mesh + ChatColor.RED + " has no path named " + ChatColor.RESET + name + ChatColor.RED + ".");

                    } else {

                        if (graphManager.getVisualizedPath(player) != null && graphManager.getVisualizedPath(player).equals(path)) {

                            graphManager.stopVisualizingPath(player);

                            sender.sendMessage("Stopped visualizing path.");

                        } else {

                            graphManager.stopVisualizingGraph(player);
                            graphManager.visualizePath(player, path);

                            sender.sendMessage("Visualizing path " + ChatColor.GRAY + name + ChatColor.RESET + " on navmesh " + ChatColor.GRAY + mesh + ChatColor.RESET + ". Type " + ChatColor.GRAY + "/" + label + " visualizepath " + mesh + " " + name + ChatColor.RESET + " to stop.");

                        }

                    }

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

    private void deletePath(CommandSender sender, String label, @Nullable String mesh, @Nullable String name) {

        if (mesh == null || name == null) {

            sender.sendMessage("Delete a path.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " deletepath <mesh> <name>");

        } else {

            Graph graph = graphManager.getGraph(mesh);

            if (graph == null) {

                sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + mesh + ChatColor.RED + " does not exist.");

            } else {

                List<Path> paths = graph.getPaths();

                Path path = null;
                int i = 0;

                while (path == null && i < paths.size()) {

                    if (paths.get(i).getName().equals(name)) {

                        path = paths.get(i);

                    }

                    i++;

                }

                if (path == null) {

                    sender.sendMessage(ChatColor.RED + "The navmesh " + ChatColor.RESET + mesh + ChatColor.RED + " has no path named " + ChatColor.RESET + name + ChatColor.RED + ".");

                } else {

                    if (graphManager.deletePath(graph, path)) {

                        graph.removePath(path);
                        sender.sendMessage("Path deleted.");

                    } else {

                        sender.sendMessage(ChatColor.RED + "The path " + ChatColor.RESET + name + ChatColor.RED + " could not be deleted.");

                    }

                }

            }

        }

    }

    private void listPaths(CommandSender sender, @Nullable String mesh) {

        if (mesh == null) {

            List<Graph> graphs = graphManager.listGraphs();

            sender.sendMessage("There " + (graphs.size() == 1 ? "is 1 navmesh" : "are " + graphs.size() + " navmeshes") + " loaded" + (graphs.size() == 0 ? "." : ":"));

            for (Graph graph : graphs) {

                List<Path> paths = graph.getPaths();

                sender.sendMessage("  " + ChatColor.GRAY + graph.getName() + ChatColor.RESET + " has " + paths.size() + " path" + (paths.size() == 1 ? "" : "s") + " loaded" + (paths.size() > 0 ? ":" : "."));

                for (Path path : paths) {

                    sender.sendMessage(ChatColor.GRAY + "   - " + ChatColor.RESET + path.getName());

                }

            }

        } else {

            Graph graph = graphManager.getGraph(mesh);

            if (graph == null) {

                sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + mesh + ChatColor.RED + " does not exist.");

            } else {

                List<Path> paths = graph.getPaths();

                sender.sendMessage("The navmesh " + ChatColor.GRAY + mesh + ChatColor.RESET + " has " + paths.size() + " path" + (paths.size() == 1 ? "" : "s") + " loaded" + (paths.size() > 0 ? ":" : "."));

                for (Path path : paths) {

                    sender.sendMessage(ChatColor.GRAY + " - " + ChatColor.RESET + path.getName());

                }

            }

        }

    }

}
