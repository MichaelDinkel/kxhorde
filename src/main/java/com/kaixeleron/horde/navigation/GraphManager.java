package com.kaixeleron.horde.navigation;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Path;
import com.kaixeleron.horde.navigation.runnable.PathVisualizingRunnable;
import com.kaixeleron.horde.navigation.runnable.VisualizingRunnable;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.*;
import java.util.*;

public class GraphManager {

    private final HordeMain main;

    private final File graphFolder, pathFolder;

    private final ItemStack wand;

    private final Map<Player, Location> firstPositions, secondPositions;

    private final List<Graph> graphs;

    private final GraphGenerator graphGenerator;

    private VisualizingRunnable visualizingRunnable = null;
    private PathVisualizingRunnable pathVisualizingRunnable = null;

    @SuppressWarnings({"ResultOfMethodCallIgnored", "ConstantConditions"})
    public GraphManager(HordeMain main, File dataFolder) {

        this.main = main;

        graphFolder = new File(dataFolder, "graphs");
        graphFolder.mkdir();

        pathFolder = new File(dataFolder, "paths");
        pathFolder.mkdir();

        wand = new ItemStack(Material.WOODEN_HOE);
        ItemMeta meta = wand.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "NavMesh Wand");
        meta.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Left click to select position 1.", ChatColor.DARK_GRAY + "Right click to select position 2."));
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        wand.setItemMeta(meta);

        firstPositions = new HashMap<>();
        secondPositions = new HashMap<>();

        graphs = new ArrayList<>();

        graphGenerator = new GraphGenerator();

    }

    public ItemStack getWand() {

        return wand;

    }

    public void setFirstPosition(Player player, Location position) {

        firstPositions.put(player, position);

    }

    public void setSecondPosition(Player player, Location position) {

        secondPositions.put(player, position);

    }

    public Location[] getPositions(Player player) {

        return new Location[]{firstPositions.get(player), secondPositions.get(player)};

    }

    public void clearPositions(Player player) {

        firstPositions.remove(player);
        secondPositions.remove(player);

    }

    public void clearPositions() {

        firstPositions.clear();
        secondPositions.clear();

    }

    public GraphGenerator getGraphGenerator() {

        return graphGenerator;

    }

    public void addGraph(Graph graph) {

        graphs.add(graph);

    }

    public void removeGraph(Graph graph) {

        graphs.remove(graph);

    }

    public void clearGraphs() {

        for (Graph graph : graphs) {

            graph.clearPaths();

        }

        graphs.clear();

    }

    public void visualizeGraph(Player player, Graph graph) {

        if (visualizingRunnable == null) {

            visualizingRunnable = new VisualizingRunnable(player, graph);
            visualizingRunnable.runTaskTimer(main, 5L, 5L);

        } else {

            visualizingRunnable.visualize(player, graph);

        }

    }

    public void stopVisualizingGraph(Player player) {

        if (visualizingRunnable != null) {

            visualizingRunnable.stopVisualizing(player);

            if (visualizingRunnable.areNoPlayersVisualizing()) {

                visualizingRunnable.cancel();
                visualizingRunnable = null;

            }

        }

    }

    public Graph getVisualizedGraph(Player player) {

        Graph graph = null;

        if (visualizingRunnable != null) {

            graph = visualizingRunnable.getVisualizing(player);

        }

        return graph;

    }

    public Graph getGraph(String name) {

        Graph result = null;

        int i = 0;

        while (result == null && i < graphs.size()) {

            Graph graph = graphs.get(i);

            if (graph.getName().equals(name)) {

                result = graph;

            }

            i++;

        }

        return result;

    }

    public void saveGraph(Player player, Graph graph) {

        File graphFile = new File(graphFolder, String.format("%s.graph", graph.getName()));

        if (!graphFile.exists()) {

            ObjectOutputStream outputStream = null;

            try {

                //noinspection ResultOfMethodCallIgnored
                graphFile.createNewFile();

                outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(graphFile)));
                outputStream.writeObject(graph);

                outputStream.flush();

            } catch (IOException e) {

                player.sendMessage(ChatColor.RED + "Could not save the graph file.");
                System.err.println("Could not save the graph file.");
                e.printStackTrace();

            } finally {

                if (outputStream != null) {

                    try {

                        outputStream.close();

                    } catch (IOException ignored) {}

                }

            }

        }

    }

    public void loadGraphs() throws IOException {

        File[] graphs = graphFolder.listFiles();

        if (graphs != null) {

            for (File graph : graphs) {

                if (graph.getName().endsWith(".graph")) {

                    ObjectInputStream inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(graph)));

                    try {

                        this.graphs.add((Graph) inputStream.readObject());

                    } catch (ClassNotFoundException ignored) {}

                    inputStream.close();

                }

            }

        }

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void savePath(final Player player, final Path path, final Graph graph) {

        File folder = new File(pathFolder, graph.getName());
        folder.mkdir();

        File pathFile = new File(folder, String.format("%s.path", path.getName()));

        if (!pathFile.exists()) {

            ObjectOutputStream outputStream = null;

            try {

                pathFile.createNewFile();

                outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(pathFile)));
                outputStream.writeObject(path);

                outputStream.flush();

            } catch (IOException e) {

                player.sendMessage(ChatColor.RED + "Could not save the path file.");
                System.err.println("Could not save the path file.");
                e.printStackTrace();

            } finally {

                if (outputStream != null) {

                    try {

                        outputStream.close();

                    } catch (IOException ignored) {}

                }

            }

        }

    }

    public void loadPaths() throws IOException {

        File[] pathFolders = pathFolder.listFiles();

        if (pathFolders != null) {

            for (File pathFolder : pathFolders) {

                if (pathFolder.isDirectory()) {

                    File[] paths = pathFolder.listFiles();

                    if (paths != null) {

                        for (File path : paths) {

                            if (path.getName().endsWith(".path")) {

                                ObjectInputStream inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(path)));

                                for (Graph graph : graphs) {

                                    if (graph.getName().equals(pathFolder.getName())) {

                                        try {

                                            graph.addPath((Path) inputStream.readObject());

                                        } catch (ClassNotFoundException ignored) {}

                                    }

                                }

                                inputStream.close();

                            }

                        }

                    }

                }

            }

        }

    }

    public boolean deleteGraph(Graph graph) {

        File file = new File(graphFolder, String.format("%s.graph", graph.getName()));

        boolean deleted = file.delete();

        if (deleted) {

            deleteDirectory(new File(pathFolder, graph.getName()));

        }

        return deleted;

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void deleteDirectory(File directory) {

        if (directory.isDirectory()) {

            File[] files = directory.listFiles();

            if (files != null) {

                for (File file : files) {

                    if (file.isDirectory()) {

                        deleteDirectory(file);

                    } else {

                        file.delete();

                    }

                }

            }

            directory.delete();

        }

    }

    public boolean deletePath(Graph graph, Path path) {

        File folder = new File(pathFolder, graph.getName());

        boolean deleted = false;

        if (folder.exists()) {

            deleted = new File(folder, String.format("%s.path", path.getName())).delete();

        }

        return deleted;

    }

    public void visualizePath(Player player, Path path) {

        if (pathVisualizingRunnable == null) {

            pathVisualizingRunnable = new PathVisualizingRunnable(player, path);
            pathVisualizingRunnable.runTaskTimer(main, 5L, 5L);

        } else {

            pathVisualizingRunnable.visualize(player, path);

        }

    }

    public void stopVisualizingPath(Player player) {

        if (pathVisualizingRunnable != null) {

            pathVisualizingRunnable.stopVisualizing(player);

            if (pathVisualizingRunnable.areNoPlayersVisualizing()) {

                pathVisualizingRunnable.cancel();
                pathVisualizingRunnable = null;

            }

        }

    }

    public Path getVisualizedPath(Player player) {

        Path path = null;

        if (pathVisualizingRunnable != null) {

            path = pathVisualizingRunnable.getVisualizing(player);

        }

        return path;

    }

    public void stopAllVisualizing() {

        if (visualizingRunnable != null) {

            visualizingRunnable.stopAllVisualizing();
            visualizingRunnable = null;

        }

        if (pathVisualizingRunnable != null) {

            pathVisualizingRunnable.stopAllVisualizing();
            pathVisualizingRunnable = null;

        }

    }

    public List<Graph> listGraphs() {

        return graphs;

    }

    public boolean graphExists(String name) {

        boolean exists = false;

        int i = 0;

        while (i < graphs.size() && !exists) {

            if (graphs.get(i).getName().equals(name)) {

                exists = true;

            }

            i++;

        }

        return exists;

    }

}
