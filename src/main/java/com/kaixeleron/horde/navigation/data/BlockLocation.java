package com.kaixeleron.horde.navigation.data;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.Serializable;
import java.util.Objects;

public class BlockLocation implements Serializable {

    private transient Location bukkitLoc = null;

    private transient World world;

    private final String worldName;

    private final int x, y, z;

    public BlockLocation(World world, int x, int y, int z) {

        this.world = Objects.requireNonNull(world);
        this.worldName = world.getName();
        this.x = x;
        this.y = y;
        this.z = z;

    }

    public BlockLocation(Location parent) {

        this(Objects.requireNonNull(parent.getWorld()), parent.getBlockX(), parent.getBlockY(), parent.getBlockZ());

        bukkitLoc = parent;

    }

    public Location getBukkitLocation() {

        if (world == null) {

            world = Bukkit.getWorld(worldName);

        }

        if (bukkitLoc == null) {

            bukkitLoc = new Location(world, x, y, z);

        }

        return bukkitLoc;

    }

    public World getWorld() {

        if (world == null) {

            world = Bukkit.getWorld(worldName);

        }

        return world;

    }

    public int getX() {

        return x;

    }

    public int getY() {

        return y;

    }

    public int getZ() {

        return z;

    }

    public double distance(BlockLocation other) {

        int diffX = x - other.x;
        int diffY = y - other.y;
        int diffZ = z - other.z;

        diffX *= diffX;
        diffY *= diffY;
        diffZ *= diffZ;

        return Math.sqrt(diffX + diffY + diffZ);

    }

    @Override
    public int hashCode() {

        int hash = 7;

        hash = 43 * hash + worldName.hashCode();
        hash = 43 * hash + x ^ (x >> 16);
        hash = 43 * hash + y ^ (y >> 16);
        hash = 43 * hash + z ^ (z >> 16);

        return hash;

    }

    @Override
    public boolean equals(Object obj) {

        boolean equals = false;

        if (obj instanceof BlockLocation other) {

            equals = other.worldName.equals(worldName) && other.x == x && other.y == y && other.z == z;

        }

        return equals;

    }

}
