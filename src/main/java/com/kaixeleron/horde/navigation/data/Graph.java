package com.kaixeleron.horde.navigation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Graph implements Serializable {

    private final String name;

    private final Map<BlockLocation, Node> nodes;

    private final List<Path> paths;

    public Graph(String name, Map<BlockLocation, Node> nodes) {

        this.name = name;
        this.nodes = nodes;
        this.paths = new ArrayList<>();

    }

    public String getName() {

        return name;

    }

    public synchronized Map<BlockLocation, Node> getNodes() {

        return nodes;

    }

    public void addPath(Path path) {

        paths.add(path);

    }

    public void removePath(Path path) {

        paths.remove(path);

    }

    public void clearPaths() {

        paths.clear();

    }

    public List<Path> getPaths() {

        return paths;

    }

    public Path getPath(String name) {

        Path result = null;

        int i = 0;

        while (result == null && i < paths.size()) {

            if (paths.get(i).getName().equals(name)) {

                result = paths.get(i);

            }

            i++;

        }

        return result;

    }

}
