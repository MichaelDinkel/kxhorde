package com.kaixeleron.horde.navigation.data;

import java.io.Serializable;

public class Path implements Serializable {

    private final String name;

    private final BlockLocation[] points;

    public Path(String name, BlockLocation[] points) {

        this.name = name;
        this.points = points;

    }

    public String getName() {

        return name;

    }

    public BlockLocation[] getPoints() {

        return points;

    }

}
