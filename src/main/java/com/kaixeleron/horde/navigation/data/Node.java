package com.kaixeleron.horde.navigation.data;

import org.bukkit.block.Block;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Node implements Serializable {

    private final BlockLocation location;

    // Location, weight
    private final Map<BlockLocation, Integer> adjacent;

    private final boolean valid;

    public Node(BlockLocation location) {

        this.location = location;
        adjacent = Collections.synchronizedMap(new HashMap<>());

        Block block = location.getBukkitLocation().getBlock();

        if (block.isPassable() && !block.getRelative(0, -1, 0).isPassable()) {

            int blockY = block.getY();

            for (int x = -1; x <= 1; x++) {

                for (int y = -1; y <= 1; y++) {

                    for (int z = -1; z <= 1; z++) {

                        synchronized (adjacent) {

                            adjacent.put(new BlockLocation(block.getRelative(x, y, z).getLocation()), y > blockY ? 2 : 1);

                        }

                    }

                }

            }

            synchronized (adjacent) {

                Iterator<Map.Entry<BlockLocation, Integer>> adjacentIterator = adjacent.entrySet().iterator();

                while (adjacentIterator.hasNext()) {

                    Map.Entry<BlockLocation, Integer> adjacent = adjacentIterator.next();

                    Block adjacentBlock = adjacent.getKey().getBukkitLocation().getBlock();

                    if (!adjacentBlock.isPassable() || adjacentBlock.getRelative(0, -1, 0).isPassable()) {

                        adjacentIterator.remove();

                    }

                }

                adjacent.remove(location);

                valid = !adjacent.isEmpty();

            }

        } else {

            valid = false;

        }

    }

    public Node(BlockLocation location, Map<BlockLocation, Integer> adjacent) {

        this.location = location;
        this.adjacent = adjacent;

        valid = true;

    }

    public BlockLocation getLocation() {

        return location;

    }

    public Map<BlockLocation, Integer> getAdjacent() {

        return adjacent;

    }

    public boolean isValid() {

        return valid;

    }

}
