package com.kaixeleron.horde.navigation.runnable;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.kaixeleron.horde.navigation.data.BlockLocation;
import com.kaixeleron.horde.navigation.data.Graph;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VisualizingRunnable extends BukkitRunnable {

    private final Map<Player, Graph> visualizing;

    private final Table<Player, BlockLocation, BlockData> changed;

    private final BlockData blockData;

    public VisualizingRunnable(Player initialPlayer, Graph initialGraph) {

        visualizing = new HashMap<>();

        visualizing.put(initialPlayer, initialGraph);

        changed = HashBasedTable.create();

        blockData = Bukkit.createBlockData(Material.YELLOW_CARPET);

    }

    public void visualize(Player player, Graph graph) {

        if (visualizing.containsKey(player)) {

            stopVisualizing(player);

        }

        visualizing.put(player, graph);

        visualizeArea(player);

    }

    public void stopVisualizing(Player player) {

        visualizing.remove(player);

        Map<BlockLocation, BlockData> pendingUndo = changed.row(player);
        List<BlockLocation> toRemove = new ArrayList<>();

        for (BlockLocation node : pendingUndo.keySet()) {

            player.sendBlockChange(node.getBukkitLocation(), pendingUndo.get(node));

            toRemove.add(node);

        }

        for (BlockLocation node : toRemove) {

            changed.remove(player, node);

        }

    }

    public void stopAllVisualizing() {

        for (Player player : new ArrayList<>(visualizing.keySet())) {

            stopVisualizing(player);

        }

    }

    public boolean areNoPlayersVisualizing() {

        return visualizing.isEmpty();

    }

    public Graph getVisualizing(Player player) {

        return visualizing.get(player);

    }

    @Override
    public void run() {

        for (Player player : visualizing.keySet()) {

            visualizeArea(player);

        }

    }

    private void visualizeArea(Player player) {

        BlockLocation location = new BlockLocation(player.getLocation());

        for (BlockLocation node : visualizing.get(player).getNodes().keySet()) {

            if (node.distance(location) < 32) {

                changed.put(player, node, node.getBukkitLocation().getBlock().getBlockData());
                player.sendBlockChange(node.getBukkitLocation(), blockData);

            } else {

                if (changed.contains(player, node)) {

                    player.sendBlockChange(node.getBukkitLocation(), changed.get(player, node));
                    changed.remove(player, node);

                }

            }

        }

    }

}
