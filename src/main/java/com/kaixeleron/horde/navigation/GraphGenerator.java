package com.kaixeleron.horde.navigation;

import com.kaixeleron.horde.navigation.data.BlockLocation;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Node;
import org.bukkit.Location;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GraphGenerator {

    public Graph generateGraph(String name, Location position1, Location position2) {

        int startX = Math.min(position1.getBlockX(), position2.getBlockX());
        int startY = Math.min(position1.getBlockY(), position2.getBlockY());
        int startZ = Math.min(position1.getBlockZ(), position2.getBlockZ());
        int endX = Math.max(position1.getBlockX(), position2.getBlockX());
        int endY = Math.max(position1.getBlockY(), position2.getBlockY());
        int endZ = Math.max(position1.getBlockZ(), position2.getBlockZ());

        Map<BlockLocation, Node> nodes = Collections.synchronizedMap(new HashMap<>());

        for (int x = startX; x <= endX; x++) {

            for (int y = startY; y <= endY; y++) {

                for (int z = startZ; z <= endZ; z++) {

                    BlockLocation nodeLoc = new BlockLocation(position1.getWorld(), x, y, z);

                    Node node = new Node(nodeLoc);

                    if (node.isValid()) {

                        nodes.put(nodeLoc, node);

                    }

                }

            }

        }

        return new Graph(name, nodes);

    }

}
