package com.kaixeleron.horde.items.data.weapons;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.buildings.BuildingManager;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.entity.Player;

public abstract class HordePDA extends HordeItem {

    protected final HordeMain main;
    protected final Player player;
    protected final Game game;
    protected final BuildingManager buildingManager;

    protected HordePDA(HordeMain main, Player player, Game game, BuildingManager buildingManager) {

        this.main = main;
        this.player = player;
        this.game = game;
        this.buildingManager = buildingManager;

    }

    public abstract void refreshStatus();

    public abstract void fireLeftClick();

    public abstract void fireRightClick();

}
