package com.kaixeleron.horde.items.data.armor.leggings;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LeggingsNetherite extends HordeItem {

    public LeggingsNetherite() {

        super.minecraftItem = new ItemStack(Material.NETHERITE_LEGGINGS);

    }

}
