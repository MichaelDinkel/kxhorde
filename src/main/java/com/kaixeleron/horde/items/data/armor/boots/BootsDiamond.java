package com.kaixeleron.horde.items.data.armor.boots;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class BootsDiamond extends HordeItem {

    public BootsDiamond() {

        super.minecraftItem = new ItemStack(Material.DIAMOND_BOOTS);

    }

}
