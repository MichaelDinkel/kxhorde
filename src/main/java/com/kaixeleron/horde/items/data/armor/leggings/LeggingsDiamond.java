package com.kaixeleron.horde.items.data.armor.leggings;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LeggingsDiamond extends HordeItem {

    public LeggingsDiamond() {

        super.minecraftItem = new ItemStack(Material.DIAMOND_LEGGINGS);

    }

}
