package com.kaixeleron.horde.items.data.weapons.tertiary;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.HordeWeapon;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;

public abstract class HordeMelee extends HordeWeapon {

    private boolean swinging = false;

    protected HordeMelee(HordeMain main, Player player, Game game) {

        super(main, player, game, 0, 0);

    }

    //TODO not broadcast
    @Override
    public void fireLeftClick() {

        if (!swinging) {

            double speed = Objects.requireNonNull(player.getAttribute(Attribute.GENERIC_ATTACK_SPEED)).getValue();

            long cooldown = Math.round(20.0D / speed);

            player.setCooldown(getItemMaterial(), (int) cooldown);

            swinging = true;

            player.getWorld().playSound(player.getLocation(), Sound.ENTITY_PHANTOM_FLAP, 2.0F, 2.0F);

            new BukkitRunnable() {

                @Override
                public void run() {

                    swinging = false;

                }

            }.runTaskLater(main, cooldown);

        }

    }

    @Override
    public void refreshStatus() {

        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));

    }

}
