package com.kaixeleron.horde.items.data.weapons.primary;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.HordeWeapon;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

public class PrimaryShotgun extends HordeWeapon {

    @SuppressWarnings("ConstantConditions")
    public PrimaryShotgun(HordeMain main, Player player, Game game) {

        super(main, player, game, 6, 32);

        ItemStack item = new ItemStack(Material.COAL);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Shotgun");
        meta.setCustomModelData(1);
        item.setItemMeta(meta);

        super.minecraftItem = item;

    }

    @Override
    public void fireLeftClick() {

        reloadWeapon(8, 1);

    }

    //TODO seers instead of broadcasting
    @Override
    public void fireRightClick() {

        if (!reloading && !firing) {

            if (ammoLoaded <= 0) {

                fireLeftClick();

            } else {

                ammoLoaded--;

                player.setCooldown(getItemMaterial(), 15);

                firing = true;

                Bukkit.getScheduler().runTaskLater(main, () -> firing = false, 15L);

                refreshStatus();

                Location location = player.getEyeLocation();
                player.getWorld().playSound(location, Sound.ENTITY_GENERIC_EXPLODE, 1.0F, 2.0F);

                Vector origin = location.clone().toVector();
                Vector[] directions = new Vector[9];

                for (int i = 0; i < 9; i++) {

                    directions[i] = location.getDirection().clone().add(new Vector(ThreadLocalRandom.current().nextDouble(-0.1D, 0.1D), ThreadLocalRandom.current().nextDouble(-0.1D, 0.1D), ThreadLocalRandom.current().nextDouble(-0.1D, 0.1D)));

                }

                for (Vector direction : directions) {

                    int i = 0;
                    boolean hit = false;

                    while (i < 20 && !hit) {

                        Location particleLoc = direction.clone().normalize().multiply(i).add(origin).toLocation(player.getWorld());

                        if (particleLoc.getBlock().isPassable()) {

                            Objects.requireNonNull(particleLoc.getWorld()).spawnParticle(Particle.CRIT, particleLoc, 0, 0.0D, 0.0D, 0.0D, 0.0D);

                        } else {

                            Objects.requireNonNull(particleLoc.getWorld()).playSound(particleLoc, Sound.BLOCK_ANVIL_LAND, 0.5F, 2.0F);
                            hit = true;

                        }

                        i++;

                    }

                }

                if (ammoLoaded <= 0) {

                    Bukkit.getScheduler().runTaskLater(main, this::fireLeftClick, 15L);

                }

            }

        }

    }

}
