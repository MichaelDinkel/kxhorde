package com.kaixeleron.horde.items.data.weapons.quaternary;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.buildings.BuildingManager;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.HordePDA;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class QuaternaryConstructionPDA extends HordePDA {

    @SuppressWarnings("ConstantConditions")
    public QuaternaryConstructionPDA(HordeMain main, Player player, Game game, BuildingManager buildingManager) {

        super(main, player, game, buildingManager);

        ItemStack item = new ItemStack(Material.BOOK);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Construction PDA");
        meta.setCustomModelData(1);
        item.setItemMeta(meta);

        super.minecraftItem = item;

    }

    @Override
    public void refreshStatus() {

    }

    @Override
    public void fireLeftClick() {

    }

    @Override
    public void fireRightClick() {

    }


    //TODO implement buildings

}
