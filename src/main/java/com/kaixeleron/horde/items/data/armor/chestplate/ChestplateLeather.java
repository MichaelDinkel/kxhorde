package com.kaixeleron.horde.items.data.armor.chestplate;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ChestplateLeather extends HordeItem {

    public ChestplateLeather() {

        super.minecraftItem = new ItemStack(Material.LEATHER_CHESTPLATE);

    }

}
