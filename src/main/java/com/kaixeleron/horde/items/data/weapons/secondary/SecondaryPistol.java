package com.kaixeleron.horde.items.data.weapons.secondary;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.HordeWeapon;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.Objects;

public class SecondaryPistol extends HordeWeapon {

    @SuppressWarnings("ConstantConditions")
    public SecondaryPistol(HordeMain main, Player player, Game game) {

        super(main, player, game, 12, 36);

        ItemStack item = new ItemStack(Material.FEATHER);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Pistol");
        meta.setCustomModelData(1);
        item.setItemMeta(meta);

        super.minecraftItem = item;

    }

    @Override
    public void fireLeftClick() {

        reloadWeapon(20, maxAmmoLoaded);

    }

    //TODO seers instead of broadcasting
    @Override
    public void fireRightClick() {

        if (!reloading && !firing) {

            if (ammoLoaded > 0) {

                ammoLoaded--;

                player.setCooldown(getItemMaterial(), 4);

                firing = true;

                Bukkit.getScheduler().runTaskLater(main, () -> firing = false, 4L);

                refreshStatus();

                Location location = player.getEyeLocation();
                player.getWorld().playSound(location, Sound.BLOCK_NETHERITE_BLOCK_BREAK, 1.0F, 2.0F);

                Vector origin = location.clone().toVector();
                Vector direction = location.getDirection().clone();

                int i = 0;
                boolean hit = false;

                while (i < 50 && !hit) {

                    Location particleLoc = direction.clone().normalize().multiply(i).add(origin).toLocation(player.getWorld());

                    if (particleLoc.getBlock().isPassable()) {

                        Objects.requireNonNull(particleLoc.getWorld()).spawnParticle(Particle.CRIT, particleLoc, 0, 0.0D, 0.0D, 0.0D, 0.0D);

                    } else {

                        Objects.requireNonNull(particleLoc.getWorld()).playSound(particleLoc, Sound.BLOCK_ANVIL_LAND, 0.5F, 2.0F);
                        hit = true;

                    }

                    i++;

                }

                if (ammoLoaded <= 0) {

                    Bukkit.getScheduler().runTaskLater(main, this::fireLeftClick, 4L);

                }

            }

        }

    }

}
