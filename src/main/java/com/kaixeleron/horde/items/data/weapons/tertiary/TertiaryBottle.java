package com.kaixeleron.horde.items.data.weapons.tertiary;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class TertiaryBottle extends HordeMelee {

    @SuppressWarnings("ConstantConditions")
    public TertiaryBottle(HordeMain main, Player player, Game game) {

        super(main, player, game);

        ItemStack item = new ItemStack(Material.NETHERITE_AXE);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Bottle");
        meta.setCustomModelData(2);
        item.setItemMeta(meta);

        super.minecraftItem = item;

    }

}
