package com.kaixeleron.horde.items.data.armor.helmet;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class HelmetIron extends HordeItem {

    public HelmetIron() {

        super.minecraftItem = new ItemStack(Material.IRON_HELMET);

    }

}
