package com.kaixeleron.horde.items.data.armor.helmet;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class HelmetGold extends HordeItem {

    public HelmetGold() {

        super.minecraftItem = new ItemStack(Material.GOLDEN_HELMET);

    }

}
