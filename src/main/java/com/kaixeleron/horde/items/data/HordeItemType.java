package com.kaixeleron.horde.items.data;

import com.kaixeleron.horde.data.HordePlayerClassType;
import com.kaixeleron.horde.items.data.armor.boots.*;
import com.kaixeleron.horde.items.data.armor.chestplate.*;
import com.kaixeleron.horde.items.data.armor.helmet.*;
import com.kaixeleron.horde.items.data.armor.leggings.*;
import com.kaixeleron.horde.items.data.weapons.primary.*;
import com.kaixeleron.horde.items.data.weapons.quaternary.QuaternaryConstructionPDA;
import com.kaixeleron.horde.items.data.weapons.quinary.QuinaryDestructionPDA;
import com.kaixeleron.horde.items.data.weapons.secondary.SecondaryMediGun;
import com.kaixeleron.horde.items.data.weapons.secondary.SecondaryPistol;
import com.kaixeleron.horde.items.data.weapons.secondary.SecondaryShotgun;
import com.kaixeleron.horde.items.data.weapons.secondary.SecondaryStickyLauncher;
import com.kaixeleron.horde.items.data.weapons.tertiary.*;

import static com.kaixeleron.horde.data.HordePlayerClassType.*;
import static com.kaixeleron.horde.items.data.ItemSlot.*;

public enum HordeItemType {

    PRIMARY_SHOTGUN(PRIMARY, PrimaryShotgun.class, false, RUNNER, BUILDER),
    PRIMARY_FLAMETHROWER(PRIMARY, PrimaryFlamethrower.class, false, FIRE),
    PRIMARY_MINIGUN(PRIMARY, PrimaryMinigun.class, false, TANK),
    PRIMARY_GRENADE_LAUNCHER(PRIMARY, PrimaryGrenadeLauncher.class, false, EXPLOSIVES),
    PRIMARY_MEDI_DART(PRIMARY, PrimaryMediDart.class, false, HEALER),
    SECONDARY_SHOTGUN(SECONDARY, SecondaryShotgun.class, false, FIRE, TANK),
    SECONDARY_PISTOL(SECONDARY, SecondaryPistol.class, false, RUNNER, BUILDER),
    SECONDARY_STICKY_LAUNCHER(SECONDARY, SecondaryStickyLauncher.class, false, EXPLOSIVES),
    SECONDARY_MEDI_GUN(SECONDARY, SecondaryMediGun.class, false, HEALER),
    TERTIARY_BAT(TERTIARY, TertiaryBat.class, false, RUNNER),
    TERTIARY_AXE(TERTIARY, TertiaryAxe.class, false, FIRE),
    TERTIARY_BOTTLE(TERTIARY, TertiaryBottle.class, false, EXPLOSIVES),
    TERTIARY_CLUB(TERTIARY, TertiaryClub.class, false, TANK),
    TERTIARY_WRENCH(TERTIARY, TertiaryWrench.class, false, BUILDER),
    TERTIARY_SAW(TERTIARY, TertiarySaw.class, false, HEALER),
    QUATERNARY_CONSTRUCTION_PDA(QUATERNARY, QuaternaryConstructionPDA.class, true, BUILDER),
    QUINARY_DESTRUCTION_PDA(QUINARY, QuinaryDestructionPDA.class, true, BUILDER),
    HELMET_LEATHER(HELMET, HelmetLeather.class, false, RUNNER, HEALER),
    HELMET_GOLD(HELMET, HelmetGold.class, false, BUILDER),
    HELMET_IRON(HELMET, HelmetIron.class, false),
    HELMET_DIAMOND(HELMET, HelmetDiamond.class, false, FIRE, EXPLOSIVES),
    HELMET_NETHERITE(HELMET, HelmetNetherite.class, false, TANK),
    CHESTPLATE_LEATHER(CHESTPLATE, ChestplateLeather.class, false),
    CHESTPLATE_GOLD(CHESTPLATE, ChestplateGold.class, false, RUNNER, FIRE, BUILDER),
    CHESTPLATE_IRON(CHESTPLATE, ChestplateIron.class, false, EXPLOSIVES, HEALER),
    CHESTPLATE_DIAMOND(CHESTPLATE, ChestplateDiamond.class, false),
    CHESTPLATE_NETHERITE(CHESTPLATE, ChestplateNetherite.class, false, TANK),
    LEGGINGS_LEATHER(LEGGINGS, LeggingsLeather.class, false, RUNNER),
    LEGGINGS_GOLD(LEGGINGS, LeggingsGold.class, false, FIRE),
    LEGGINGS_IRON(LEGGINGS, LeggingsIron.class, false, EXPLOSIVES, BUILDER, HEALER),
    LEGGINGS_DIAMOND(LEGGINGS, LeggingsDiamond.class, false),
    LEGGINGS_NETHERITE(LEGGINGS, LeggingsNetherite.class, false, TANK),
    BOOTS_LEATHER(BOOTS, BootsLeather.class, false, RUNNER),
    BOOTS_GOLD(BOOTS, BootsGold.class, false, EXPLOSIVES, HEALER),
    BOOTS_IRON(BOOTS, BootsIron.class, false, FIRE, BUILDER),
    BOOTS_DIAMOND(BOOTS, BootsDiamond.class, false),
    BOOTS_NETHERITE(BOOTS, BootsNetherite.class, false, TANK);

    private final ItemSlot slot;
    private final Class<? extends HordeItem> clazz;
    private final boolean isPDA;
    private final HordePlayerClassType[] playerClassTypes;

    HordeItemType(ItemSlot slot, Class<? extends HordeItem> clazz, boolean isPDA, HordePlayerClassType... playerClassTypes) {

        this.slot = slot;
        this.clazz = clazz;
        this.isPDA = isPDA;
        this.playerClassTypes = playerClassTypes;

    }

    public ItemSlot getSlot() {

        return slot;

    }

    public Class<? extends HordeItem> getClassType() {

        return clazz;

    }

    public boolean isPDA() {

        return isPDA;

    }

    public HordePlayerClassType[] getPlayerClassTypes() {

        return playerClassTypes;

    }

}
