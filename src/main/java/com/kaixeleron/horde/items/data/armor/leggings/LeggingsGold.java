package com.kaixeleron.horde.items.data.armor.leggings;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LeggingsGold extends HordeItem {

    public LeggingsGold() {

        super.minecraftItem = new ItemStack(Material.GOLDEN_LEGGINGS);

    }

}
