package com.kaixeleron.horde.items.data.weapons.secondary;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.HordeWeapon;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SecondaryStickyLauncher extends HordeWeapon {

    private final Player player;

    @SuppressWarnings("ConstantConditions")
    public SecondaryStickyLauncher(HordeMain main, Player player, Game game) {

        super(main, player, game, 0, 0);

        this.player = player;

        ItemStack item = new ItemStack(Material.BRICK);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Stickybomb Launcher");
        meta.setCustomModelData(1);
        item.setItemMeta(meta);

        super.minecraftItem = item;

    }

    @Override
    public void refreshStatus() {

    }

    @Override
    public void fireLeftClick() {

    }

    @Override
    public void fireRightClick() {

    }
}
