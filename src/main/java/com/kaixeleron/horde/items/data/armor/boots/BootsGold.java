package com.kaixeleron.horde.items.data.armor.boots;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class BootsGold extends HordeItem {

    public BootsGold() {

        super.minecraftItem = new ItemStack(Material.GOLDEN_BOOTS);

    }

}
