package com.kaixeleron.horde.items.data.armor.boots;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class BootsIron extends HordeItem {

    public BootsIron() {

        super.minecraftItem = new ItemStack(Material.IRON_BOOTS);

    }

}
