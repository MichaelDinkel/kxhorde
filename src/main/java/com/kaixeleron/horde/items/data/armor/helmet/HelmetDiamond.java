package com.kaixeleron.horde.items.data.armor.helmet;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class HelmetDiamond extends HordeItem {

    public HelmetDiamond() {

        super.minecraftItem = new ItemStack(Material.DIAMOND_HELMET);

    }

}
