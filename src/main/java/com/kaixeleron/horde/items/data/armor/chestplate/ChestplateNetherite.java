package com.kaixeleron.horde.items.data.armor.chestplate;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ChestplateNetherite extends HordeItem {

    public ChestplateNetherite() {

        super.minecraftItem = new ItemStack(Material.NETHERITE_CHESTPLATE);

    }

}
