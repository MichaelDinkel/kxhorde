package com.kaixeleron.horde.items.data.weapons.primary;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntity;
import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.HordeWeapon;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class PrimaryMediDart extends HordeWeapon {

    @SuppressWarnings("ConstantConditions")
    public PrimaryMediDart(HordeMain main, Player player, Game game) {

        super(main, player, game, 1, 24);

        ItemStack item = new ItemStack(Material.GOLD_INGOT);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Medi-dart");
        meta.setCustomModelData(1);
        item.setItemMeta(meta);

        super.minecraftItem = item;

    }

    @Override
    public void fireLeftClick() {

        reloadWeapon(20, maxAmmoLoaded);

    }

    //TODO seers instead of broadcasting
    @Override
    public void fireRightClick() {

        if (!reloading && !firing) {

            if (ammoLoaded > 0) {

                ammoLoaded--;

                player.setCooldown(getItemMaterial(), 4);

                firing = true;

                Bukkit.getScheduler().runTaskLater(main, () -> firing = false, 4L);

                refreshStatus();

                Location location = player.getEyeLocation();
                player.getWorld().playSound(location, Sound.BLOCK_NETHERITE_BLOCK_BREAK, 1.0F, 2.0F);

                Vector direction = location.getDirection().clone();

                final int id = ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE - 1000000, Integer.MAX_VALUE);

                WrapperPlayServerSpawnEntity packet = new WrapperPlayServerSpawnEntity();
                packet.setEntityID(id);
                packet.setType(EntityType.SHULKER_BULLET);
                packet.setUniqueId(UUID.randomUUID());
                packet.setX(location.getX());
                packet.setY(location.getY());
                packet.setZ(location.getZ());
                packet.setOptionalSpeedX(direction.getX());
                packet.setOptionalSpeedY(direction.getY());
                packet.setOptionalSpeedZ(direction.getZ());

                packet.broadcastPacket();

                Bukkit.getScheduler().runTaskLater(main, () -> {

                    WrapperPlayServerEntityDestroy destroyPacket = new WrapperPlayServerEntityDestroy();
                    destroyPacket.setEntityId(id);
                    destroyPacket.broadcastPacket();

                }, 40L);

                if (ammoLoaded <= 0) {

                    Bukkit.getScheduler().runTaskLater(main, this::fireLeftClick, 4L);

                }

            }

        }

    }

}
