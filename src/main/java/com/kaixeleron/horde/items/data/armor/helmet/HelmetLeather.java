package com.kaixeleron.horde.items.data.armor.helmet;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class HelmetLeather extends HordeItem {

    public HelmetLeather() {

        super.minecraftItem = new ItemStack(Material.LEATHER_HELMET);

    }

}
