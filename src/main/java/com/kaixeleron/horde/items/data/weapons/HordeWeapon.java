package com.kaixeleron.horde.items.data.weapons;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.HordeItem;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Objects;

public abstract class HordeWeapon extends HordeItem {

    protected final HordeMain main;
    protected final Player player;
    protected final Game game;
    protected final int maxAmmoLoaded, maxAmmoRemaining;
    protected int ammoLoaded, ammoRemaining;
    protected boolean reloading = false, firing = false;

    protected HordeWeapon(HordeMain main, Player player, Game game, int maxAmmoLoaded, int maxAmmoRemaining) {

        this.main = main;
        this.player = player;
        this.game = game;
        this.maxAmmoLoaded = maxAmmoLoaded;
        this.maxAmmoRemaining = maxAmmoRemaining;
        ammoLoaded = maxAmmoLoaded;
        ammoRemaining = maxAmmoRemaining;

    }

    protected void reloadWeapon(int interval, int ammoPerInterval) {

        if (ammoLoaded < maxAmmoLoaded && ammoRemaining > 0 && !reloading) {

            reloading = true;

            player.setCooldown(getItemMaterial(), interval);

            Location location = player.getLocation();
            Objects.requireNonNull(location.getWorld()).playSound(location, Sound.BLOCK_IRON_DOOR_OPEN, 0.5F, 2.0F);

            new BukkitRunnable() {

                @Override
                public void run() {

                    int change = maxAmmoLoaded - ammoLoaded;
                    if (change > ammoPerInterval) change = ammoPerInterval;
                    if (change > ammoRemaining) change = ammoRemaining;

                    ammoRemaining -= change;
                    ammoLoaded += change;

                    refreshStatus();

                    if (ammoLoaded == maxAmmoLoaded || ammoRemaining == 0) {

                        cancel();
                        reloading = false;

                    } else {

                        player.setCooldown(getItemMaterial(), interval);

                        Location location = player.getLocation();
                        Objects.requireNonNull(location.getWorld()).playSound(location, Sound.BLOCK_IRON_DOOR_OPEN, 0.5F, 2.0F);

                    }

                }

            }.runTaskTimer(main, interval, interval);

        }

    }

    public void refreshStatus() {

        TextComponent ammo = new TextComponent(Integer.toString(ammoLoaded));
        ammo.setColor(net.md_5.bungee.api.ChatColor.AQUA);
        ammo.setBold(true);

        TextComponent slash = new TextComponent(" / ");
        slash.setColor(net.md_5.bungee.api.ChatColor.RED);
        slash.setBold(true);
        ammo.addExtra(slash);

        TextComponent remaining = new TextComponent(Integer.toString(ammoRemaining));
        remaining.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
        remaining.setBold(true);
        ammo.addExtra(remaining);

        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, ammo);

    }

    public void fireLeftClick() {}

    public void fireRightClick() {}

}
