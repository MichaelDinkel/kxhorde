package com.kaixeleron.horde.items.data.armor.helmet;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class HelmetNetherite extends HordeItem {

    public HelmetNetherite() {

        super.minecraftItem = new ItemStack(Material.NETHERITE_HELMET);

    }

}
