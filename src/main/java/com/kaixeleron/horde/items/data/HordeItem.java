package com.kaixeleron.horde.items.data;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public abstract class HordeItem {

    protected ItemStack minecraftItem;

    protected HordeItem() {}

    public final ItemStack getMinecraftItem() {

        return minecraftItem;

    }

    public final Material getItemMaterial() {

        return minecraftItem.getType();

    }

}
