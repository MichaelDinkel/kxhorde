package com.kaixeleron.horde.items.data.armor.leggings;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LeggingsLeather extends HordeItem {

    public LeggingsLeather() {

        super.minecraftItem = new ItemStack(Material.LEATHER_LEGGINGS);

    }

}
