package com.kaixeleron.horde.items.data.armor.boots;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class BootsLeather extends HordeItem {

    public BootsLeather() {

        super.minecraftItem = new ItemStack(Material.LEATHER_BOOTS);

    }

}
