package com.kaixeleron.horde.items.data.armor.chestplate;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ChestplateDiamond extends HordeItem {

    public ChestplateDiamond() {

        super.minecraftItem = new ItemStack(Material.DIAMOND_CHESTPLATE);

    }

}
