package com.kaixeleron.horde.items.data.armor.leggings;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LeggingsIron extends HordeItem {

    public LeggingsIron() {

        super.minecraftItem = new ItemStack(Material.IRON_LEGGINGS);

    }

}
