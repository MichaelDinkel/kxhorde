package com.kaixeleron.horde.items.data.weapons.primary;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntity;
import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.HordeWeapon;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class PrimaryFlamethrower extends HordeWeapon {

    @SuppressWarnings("ConstantConditions")
    public PrimaryFlamethrower(HordeMain main, Player player, Game game) {

        super(main, player, game, 0, 200);

        ItemStack item = new ItemStack(Material.COPPER_INGOT);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Flamethrower");
        meta.setCustomModelData(1);
        item.setItemMeta(meta);

        super.minecraftItem = item;

    }

    @Override
    public void refreshStatus() {

        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder(Integer.toString(ammoRemaining)).color(net.md_5.bungee.api.ChatColor.YELLOW).bold(true).create());

    }

    @Override
    public void fireLeftClick() {

    }

    //TODO seers instead of broadcasting
    @Override
    public void fireRightClick() {

        if (ammoRemaining > 0) {

            firing = true;

            Bukkit.getScheduler().runTaskLater(main, () -> firing = false, 3L);

            new BukkitRunnable() {

                int i = 0;

                @Override
                public void run() {

                    ammoRemaining--;
                    refreshStatus();

                    Location location = player.getEyeLocation().clone().add(0.0D, -1.0D, 0.0D);
                    player.getWorld().playSound(location, Sound.ENTITY_BLAZE_SHOOT, 0.5F, 2.0F);

                    Vector[] directions = new Vector[9];

                    for (int i = 0; i < 9; i++) {

                        directions[i] = location.getDirection().clone().add(new Vector(ThreadLocalRandom.current().nextDouble(-0.05D, 0.05D), ThreadLocalRandom.current().nextDouble(-0.05D, 0.05D), ThreadLocalRandom.current().nextDouble(-0.05D, 0.05D)));

                    }

                    for (Vector direction : directions) {

                        final int id = ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE - 1000000, Integer.MAX_VALUE);

                        WrapperPlayServerSpawnEntity packet = new WrapperPlayServerSpawnEntity();
                        packet.setEntityID(id);
                        packet.setType(EntityType.SMALL_FIREBALL);
                        packet.setUniqueId(UUID.randomUUID());
                        packet.setX(location.getX());
                        packet.setY(location.getY());
                        packet.setZ(location.getZ());
                        packet.setOptionalSpeedX(direction.getX());
                        packet.setOptionalSpeedY(direction.getY());
                        packet.setOptionalSpeedZ(direction.getZ());

                        packet.broadcastPacket();

                        Bukkit.getScheduler().runTaskLater(main, () -> {

                            WrapperPlayServerEntityDestroy destroyPacket = new WrapperPlayServerEntityDestroy();
                            destroyPacket.setEntityId(id);
                            destroyPacket.broadcastPacket();

                        }, 12L);

                    }

                    i++;

                    if (i == 3 || ammoRemaining == 0) cancel();

                }

            }.runTaskTimer(main, 0L, 1L);

        }

    }

}
