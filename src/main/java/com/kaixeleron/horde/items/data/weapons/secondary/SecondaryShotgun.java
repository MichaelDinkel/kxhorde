package com.kaixeleron.horde.items.data.weapons.secondary;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.weapons.primary.PrimaryShotgun;
import org.bukkit.entity.Player;

public class SecondaryShotgun extends PrimaryShotgun {

    public SecondaryShotgun(HordeMain main, Player player, Game game) {

        super(main, player, game);

    }

}
