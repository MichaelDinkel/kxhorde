package com.kaixeleron.horde.items.data.armor.chestplate;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ChestplateIron extends HordeItem {

    public ChestplateIron() {

        super.minecraftItem = new ItemStack(Material.IRON_CHESTPLATE);

    }

}
