package com.kaixeleron.horde.items.data.armor.boots;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class BootsNetherite extends HordeItem {

    public BootsNetherite() {

        super.minecraftItem = new ItemStack(Material.NETHERITE_BOOTS);

    }

}
