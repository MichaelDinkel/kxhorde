package com.kaixeleron.horde.items.data.armor.chestplate;

import com.kaixeleron.horde.items.data.HordeItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ChestplateGold extends HordeItem {

    public ChestplateGold() {

        super.minecraftItem = new ItemStack(Material.GOLDEN_CHESTPLATE);

    }

}
