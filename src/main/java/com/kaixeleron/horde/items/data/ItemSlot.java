package com.kaixeleron.horde.items.data;

public enum ItemSlot {

    HELMET(39),
    CHESTPLATE(38),
    LEGGINGS(37),
    BOOTS(36),
    OFFHAND(40),
    PRIMARY(0),
    SECONDARY(1),
    TERTIARY(2),
    QUATERNARY(3),
    QUINARY(4),
    CASE(-1);

    private final int inventorySlot;

    ItemSlot(int inventorySlot) {

        this.inventorySlot = inventorySlot;

    }

    public int getInventorySlot() {

        return inventorySlot;

    }

    public static ItemSlot getByInventorySlot(int inventorySlot) {

        ItemSlot result = null;

        for (ItemSlot slot : values()) {

            if (slot.inventorySlot == inventorySlot) {

                result = slot;

            }

        }

        return result;

    }

}
