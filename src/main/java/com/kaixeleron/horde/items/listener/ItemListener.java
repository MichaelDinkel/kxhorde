package com.kaixeleron.horde.items.listener;

import com.kaixeleron.horde.items.ItemManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Objects;

public class ItemListener implements Listener {

    private final ItemManager itemManager;

    public ItemListener(ItemManager itemManager) {

        this.itemManager = itemManager;

    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {

        if (Objects.equals(event.getHand(), EquipmentSlot.HAND)) {

            boolean cancel = false;

            switch (event.getAction()) {
                case LEFT_CLICK_AIR, LEFT_CLICK_BLOCK -> cancel = itemManager.fireLeftClick(event.getPlayer(), event.getPlayer().getInventory().getHeldItemSlot());
                case RIGHT_CLICK_AIR, RIGHT_CLICK_BLOCK -> cancel = itemManager.fireRightClick(event.getPlayer(), event.getPlayer().getInventory().getHeldItemSlot());
            }

            if (cancel) {

                event.setCancelled(true);

            }

        }

    }

    @EventHandler(ignoreCancelled = true)
    public void onItemSlotChange(PlayerItemHeldEvent event) {

        itemManager.refreshPlayerStatus(event.getPlayer(), event.getNewSlot());

    }

}
