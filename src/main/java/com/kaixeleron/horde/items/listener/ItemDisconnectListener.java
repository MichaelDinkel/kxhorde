package com.kaixeleron.horde.items.listener;

import com.kaixeleron.horde.items.ItemManager;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ItemDisconnectListener implements Listener {

    private final ItemManager itemManager;

    public ItemDisconnectListener(ItemManager itemManager) {

        this.itemManager = itemManager;

    }

    public void onQuit(PlayerQuitEvent event) {

        itemManager.removePlayer(event.getPlayer());

    }

    public void onKick(PlayerKickEvent event) {

        itemManager.removePlayer(event.getPlayer());

    }

}
