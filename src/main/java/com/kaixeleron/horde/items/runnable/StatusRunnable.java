package com.kaixeleron.horde.items.runnable;

import com.kaixeleron.horde.items.ItemManager;
import org.bukkit.scheduler.BukkitRunnable;

public class StatusRunnable extends BukkitRunnable {

    private final ItemManager itemManager;

    public StatusRunnable(ItemManager itemManager) {

        this.itemManager = itemManager;

    }

    @Override
    public void run() {

        itemManager.refreshStatuses();

    }

}
