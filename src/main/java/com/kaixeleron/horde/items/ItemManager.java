package com.kaixeleron.horde.items;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.buildings.BuildingManager;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.data.HordeItem;
import com.kaixeleron.horde.items.data.HordeItemType;
import com.kaixeleron.horde.items.data.ItemSlot;
import com.kaixeleron.horde.items.data.weapons.HordePDA;
import com.kaixeleron.horde.items.data.weapons.HordeWeapon;
import com.kaixeleron.horde.items.runnable.StatusRunnable;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static com.kaixeleron.horde.items.data.HordeItemType.*;

public class ItemManager {

    private final HordeMain main;
    private final BuildingManager buildingManager;

    private final HordeItemType[] stockItems;

    private final Table<Player, ItemSlot, HordeItem> activeItems;

    private final StatusRunnable runnable;

    public ItemManager(HordeMain main, BuildingManager buildingManager) {

        this.main = main;
        this.buildingManager = buildingManager;

        stockItems = new HordeItemType[]{ PRIMARY_SHOTGUN, PRIMARY_FLAMETHROWER, PRIMARY_GRENADE_LAUNCHER, PRIMARY_MINIGUN, PRIMARY_MEDI_DART,
                SECONDARY_SHOTGUN, SECONDARY_PISTOL, SECONDARY_STICKY_LAUNCHER, SECONDARY_MEDI_GUN, TERTIARY_BAT, TERTIARY_AXE, TERTIARY_BOTTLE,
                TERTIARY_CLUB, TERTIARY_WRENCH, TERTIARY_SAW, QUATERNARY_CONSTRUCTION_PDA, QUINARY_DESTRUCTION_PDA, HELMET_LEATHER, HELMET_GOLD,
                HELMET_IRON, HELMET_DIAMOND, HELMET_NETHERITE, CHESTPLATE_LEATHER, CHESTPLATE_GOLD, CHESTPLATE_IRON, CHESTPLATE_DIAMOND,
                CHESTPLATE_NETHERITE, LEGGINGS_LEATHER, LEGGINGS_GOLD, LEGGINGS_IRON, LEGGINGS_DIAMOND, LEGGINGS_NETHERITE, BOOTS_LEATHER,
                BOOTS_GOLD, BOOTS_IRON, BOOTS_DIAMOND, BOOTS_NETHERITE };

        activeItems = HashBasedTable.create();

        runnable = new StatusRunnable(this);

    }

    public HordeItem generateItem(Player player, @Nullable Game game, HordeItemType type) {

        HordeItem item = null;

        try {

            switch (type.getSlot()) {

                case PRIMARY, SECONDARY, TERTIARY, QUATERNARY, QUINARY -> {

                    if (type.isPDA()) {

                        Constructor<? extends HordeItem> constructor = type.getClassType().getConstructor(HordeMain.class, Player.class, Game.class, BuildingManager.class);
                        item = constructor.newInstance(main, player, game, buildingManager);

                    } else {

                        Constructor<? extends HordeItem> constructor = type.getClassType().getConstructor(HordeMain.class, Player.class, Game.class);
                        item = constructor.newInstance(main, player, game);

                    }

                }

                default -> {

                    Constructor<? extends HordeItem> constructor = type.getClassType().getConstructor();
                    item = constructor.newInstance();

                }

            }

        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ignored) {}

        return item;

    }

    public void startRefreshRunnable() {

        runnable.runTaskTimer(main, 0L, 20L);

    }

    public HordeItemType[] getStockItems() {

        return stockItems;

    }

    public void assignItem(Player player, ItemSlot slot, HordeItem item) {

        activeItems.put(player, slot, item);

    }

    public void removePlayer(Player player) {

        activeItems.rowKeySet().removeIf(key -> key.equals(player));
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));

    }

    public void clearPlayers() {

        for (Player player : activeItems.rowKeySet()) {

            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));

        }

        activeItems.clear();

    }

    public void stopRefreshRunnable() {

        runnable.cancel();

    }

    public boolean fireLeftClick(Player player, int slot) {

        boolean fired = false;

        HordeItem item = activeItems.get(player, ItemSlot.getByInventorySlot(slot));

        if (item instanceof HordeWeapon weapon) {

            weapon.fireLeftClick();
            fired = true;

        } else if (item instanceof HordePDA pda) {

            pda.fireLeftClick();
            fired = true;

        }

        return fired;

    }

    public boolean fireRightClick(Player player, int slot) {

        boolean fired = false;

        HordeItem item = activeItems.get(player, ItemSlot.getByInventorySlot(slot));

        if (item instanceof HordeWeapon weapon) {

            weapon.fireRightClick();
            fired = true;

        } else if (item instanceof HordePDA pda) {

            pda.fireRightClick();
            fired = true;

        }

        return fired;

    }

    public void refreshPlayerStatus(Player player, int slot) {

        HordeItem item = activeItems.get(player, ItemSlot.getByInventorySlot(slot));

        if (item instanceof HordeWeapon weapon) {

            weapon.refreshStatus();

        } else if (item instanceof HordePDA pda) {

            pda.refreshStatus();

        } else {

            player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));

        }

    }

    public void refreshStatuses() {

        for (Player player : activeItems.rowKeySet()) {

            HordeItem item = activeItems.get(player, ItemSlot.getByInventorySlot(player.getInventory().getHeldItemSlot()));

            if (item instanceof HordeWeapon weapon) {

                weapon.refreshStatus();

            } else if (item instanceof HordePDA pda) {

                pda.refreshStatus();

            }

        }

    }

}
