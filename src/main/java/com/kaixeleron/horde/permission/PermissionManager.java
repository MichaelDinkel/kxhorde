package com.kaixeleron.horde.permission;

import com.kaixeleron.horde.HordeMain;
import org.bukkit.Bukkit;
import org.bukkit.permissions.PermissionAttachment;

public class PermissionManager {

    private final PermissionAttachment attachment;

    public PermissionManager(HordeMain main) {

        attachment = Bukkit.getConsoleSender().addAttachment(main);

    }

    public void setConsolePermissions() {

        attachment.setPermission("kxhorde.group.admin", true);

    }

    public void detach() {

        attachment.remove();

    }

}
