package com.kaixeleron.horde.buildings.data;

import com.comphenix.packetwrapper.WrapperPlayServerEntityEquipment;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.Vector3F;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;
import java.util.stream.Stream;

public class BuildingDispenser2 extends HordeBuilding {

    public BuildingDispenser2(Player player, Game game, int entityId) {

        super(player, game, entityId, EntityType.ARMOR_STAND);

    }

    @Override
    public void build(Location location) {

        super.build(location);

        ItemStack body = new ItemStack(Material.IRON_HELMET);
        ItemMeta bodyMeta = Objects.requireNonNull(body.getItemMeta());
        bodyMeta.setCustomModelData(14); //TODO might conflict with custom items
        body.setItemMeta(bodyMeta);

        WrapperPlayServerEntityEquipment equipment = new WrapperPlayServerEntityEquipment();
        equipment.setEntityID(entityId);
        equipment.setSlotStackPair(EnumWrappers.ItemSlot.CHEST, body);

        Stream.concat(game.getPlayers().stream(), game.getSpectators().stream()).forEach(equipment::sendPacket);

        WrapperPlayServerEntityMetadata metadata = new WrapperPlayServerEntityMetadata();
        metadata.setEntityID(entityId);
        WrappedDataWatcher armorStandWatcher = new WrappedDataWatcher();
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0x20); // Invisible
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(15, WrappedDataWatcher.Registry.get(Byte.class)), (byte) (0x01 | 0x08)); // Small, no baseplate
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(17, WrappedDataWatcher.Registry.getVectorSerializer()), new Vector3F(0, (float) Math.toRadians(location.getYaw()), 0));
        metadata.setMetadata(armorStandWatcher.getWatchableObjects());

        Stream.concat(game.getPlayers().stream(), game.getSpectators().stream()).forEach(metadata::sendPacket);

    }

    @Override
    public void tick() {

    }

}
