package com.kaixeleron.horde.buildings.data;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntity;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.stream.Stream;

public abstract class HordeBuilding {

    protected final int entityId;
    private final EntityType entityType;

    protected final Player owner;
    protected final Game game;
    protected Location location;
    protected boolean active = false;

    protected HordeBuilding(Player owner, Game game, int entityId, EntityType entityType) {

        this.owner = owner;
        this.game = game;
        this.entityId = entityId;
        this.entityType = entityType;

    }

    public void preview() {

        active = false;

        Location location = owner.getLocation();

        WrapperPlayServerSpawnEntity spawnPacket = new WrapperPlayServerSpawnEntity();
        spawnPacket.setEntityID(entityId);
        spawnPacket.setType(entityType);
        spawnPacket.setUniqueId(UUID.randomUUID());
        spawnPacket.setX(location.getX());
        spawnPacket.setY(location.getY());
        spawnPacket.setZ(location.getZ());
        spawnPacket.setYaw(location.getYaw());

        spawnPacket.sendPacket(owner);

    }

    public void build(Location location) {

        active = true;

        this.location = location;

        WrapperPlayServerEntityDestroy destroyPacket = new WrapperPlayServerEntityDestroy();
        destroyPacket.setEntityId(entityId);
        destroyPacket.sendPacket(owner);

        WrapperPlayServerSpawnEntity spawnPacket = new WrapperPlayServerSpawnEntity();
        spawnPacket.setEntityID(entityId);
        spawnPacket.setType(entityType);
        spawnPacket.setUniqueId(UUID.randomUUID());
        spawnPacket.setX(location.getX());
        spawnPacket.setY(location.getY());
        spawnPacket.setZ(location.getZ());
        spawnPacket.setYaw(location.getYaw());

        Stream.concat(game.getPlayers().stream(), game.getSpectators().stream()).forEach(spawnPacket::sendPacket);

    }

    public void pickUp() {

        destroy();
        preview();

    }

    public void destroy() {

        location = null;

        WrapperPlayServerEntityDestroy destroyPacket = new WrapperPlayServerEntityDestroy();
        destroyPacket.setEntityId(entityId);

        Stream.concat(game.getPlayers().stream(), game.getSpectators().stream()).forEach(destroyPacket::sendPacket);

    }

    public abstract void tick();

    public final int getEntityId() {

        return entityId;

    }

}
