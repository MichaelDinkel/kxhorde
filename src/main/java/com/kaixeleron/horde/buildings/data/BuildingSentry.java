package com.kaixeleron.horde.buildings.data;

import com.comphenix.packetwrapper.WrapperPlayServerEntityEquipment;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.Vector3F;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;
import java.util.stream.Stream;

public class BuildingSentry extends HordeBuilding {

    private float initialYaw = 0.0F;
    private float currentYaw = 0.0F;
    boolean yawUp = true;

    public BuildingSentry(Player player, Game game, int entityId) {

        super(player, game, entityId, EntityType.ARMOR_STAND);

    }

    @Override
    public void build(Location location) {

        super.build(location);

        initialYaw = location.getYaw();
        currentYaw = initialYaw;

        ItemStack body = new ItemStack(Material.IRON_HELMET);
        ItemMeta bodyMeta = Objects.requireNonNull(body.getItemMeta());
        bodyMeta.setCustomModelData(10); //TODO might conflict with custom items
        body.setItemMeta(bodyMeta);

        ItemStack stand = new ItemStack(Material.IRON_CHESTPLATE);
        ItemMeta standMeta = Objects.requireNonNull(body.getItemMeta());
        standMeta.setCustomModelData(10); //TODO might conflict with custom items
        stand.setItemMeta(standMeta);

        WrapperPlayServerEntityEquipment equipment = new WrapperPlayServerEntityEquipment();
        equipment.setEntityID(entityId);
        equipment.setSlotStackPair(EnumWrappers.ItemSlot.HEAD, body);
        equipment.setSlotStackPair(EnumWrappers.ItemSlot.CHEST, stand);

        Stream.concat(game.getPlayers().stream(), game.getSpectators().stream()).forEach(equipment::sendPacket);

        WrapperPlayServerEntityMetadata metadata = new WrapperPlayServerEntityMetadata();
        metadata.setEntityID(entityId);
        WrappedDataWatcher armorStandWatcher = new WrappedDataWatcher();
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0x20); // Invisible
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(15, WrappedDataWatcher.Registry.get(Byte.class)), (byte) (0x01 | 0x08)); // Small, no baseplate
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(17, WrappedDataWatcher.Registry.getVectorSerializer()), new Vector3F(0, (float) Math.toRadians(currentYaw), 0));
        metadata.setMetadata(armorStandWatcher.getWatchableObjects());

        Stream.concat(game.getPlayers().stream(), game.getSpectators().stream()).forEach(metadata::sendPacket);

    }

    @Override
    public void tick() {

        if (active) {

            if (yawUp) {

                currentYaw += 2.0F;

                if (currentYaw - initialYaw >= 45.0F) {

                    yawUp = false;

                }

            } else {

                currentYaw -= 2.0F;

                if (currentYaw - initialYaw <= -45.0F) {

                    yawUp = true;

                }

            }

        }

        float byteYaw = (currentYaw % 360) * (256F / 360F); //TODO this is still off for some angles

        WrapperPlayServerEntityMetadata rotationMetadata = new WrapperPlayServerEntityMetadata();
        rotationMetadata.setEntityID(entityId);
        WrappedDataWatcher armorStandWatcher = new WrappedDataWatcher();
        armorStandWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(16, WrappedDataWatcher.Registry.getVectorSerializer()), new Vector3F(0, byteYaw, 0));
        rotationMetadata.setMetadata(armorStandWatcher.getWatchableObjects());

        Stream.concat(game.getPlayers().stream(), game.getSpectators().stream()).forEach(rotationMetadata::sendPacket);

    }

}
