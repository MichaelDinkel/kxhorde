package com.kaixeleron.horde.buildings.data;

import javax.annotation.Nullable;

public enum HordeBuildingType {

    SENTRY_3(BuildingSentry3.class, false, null),
    SENTRY_2(BuildingSentry2.class, false, SENTRY_3),
    SENTRY(BuildingSentry.class, true, SENTRY_2),
    DISPENSER_3(BuildingDispenser3.class, false, null),
    DISPENSER_2(BuildingDispenser2.class, false, DISPENSER_3),
    DISPENSER(BuildingDispenser.class, true, DISPENSER_2);

    private final Class<? extends HordeBuilding> clazz;
    private final boolean isBuildable;
    private final HordeBuildingType nextUpgrade;

    HordeBuildingType(Class<? extends HordeBuilding> clazz, boolean isBuildable, @Nullable HordeBuildingType nextUpgrade) {

        this.clazz = clazz;
        this.isBuildable = isBuildable;
        this.nextUpgrade = nextUpgrade;

    }

    public Class<? extends HordeBuilding> getClassType() {

        return clazz;

    }

    public boolean isBuildable() {

        return isBuildable;

    }

    public @Nullable HordeBuildingType getNextUpgrade() {

        return nextUpgrade;

    }

}
