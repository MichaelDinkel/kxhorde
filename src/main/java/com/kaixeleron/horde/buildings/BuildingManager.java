package com.kaixeleron.horde.buildings;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.buildings.data.HordeBuilding;
import com.kaixeleron.horde.buildings.data.HordeBuildingType;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class BuildingManager {

    private static final int MAX_ENTITY_ID = Integer.MAX_VALUE - 1000000;

    private final HordeMain main;
    private final List<HordeBuilding> activeBuildings;
    private final BukkitRunnable tickRunnable;

    public BuildingManager(HordeMain main) {

        this.main = main;

        activeBuildings = new ArrayList<>();

        tickRunnable = new BukkitRunnable() {

            @Override
            public void run() {

                for (HordeBuilding building : activeBuildings) {

                    building.tick();

                }

            }

        };

    }

    public @Nullable HordeBuilding addBuilding(Player owner, Game game, HordeBuildingType type) {

        HordeBuilding building = null;

        try {

            Constructor<? extends HordeBuilding> constructor = type.getClassType().getConstructor(Player.class, Game.class, int.class);
            Optional<HordeBuilding> lowest = activeBuildings.stream().min(Comparator.comparing(HordeBuilding::getEntityId));
            building = constructor.newInstance(owner, game, lowest.map(hordeBuilding -> hordeBuilding.getEntityId() - 1).orElse(MAX_ENTITY_ID));
            activeBuildings.add(building);

        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ignored) {}

        return building;

    }

    public void removeBuilding(HordeBuilding building) {

        activeBuildings.remove(building);

    }

    public void clearBuildings() {

        activeBuildings.clear();

    }

    public void startTask() {

        tickRunnable.runTaskTimer(main, 0L, 1L);

    }

    public void stopTask() {

        tickRunnable.cancel();

    }

}
