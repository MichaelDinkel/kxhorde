package com.kaixeleron.horde.game;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.chat.ChatManager;
import com.kaixeleron.horde.entity.EntityManager;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.items.ItemManager;
import com.kaixeleron.horde.map.data.Map;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class GameManager {

    public static final String PREFIX = ChatColor.BLUE + "[Horde] ";
    public static final int MAX_TIMER = 100;

    private final HordeMain main;

    private final ChatManager chatManager;

    private final EntityManager entityManager;

    private final ItemManager itemManager;

    private final int gameSize;

    private final List<Game> games;

    public GameManager(HordeMain main, ChatManager chatManager, EntityManager entityManager, ItemManager itemManager, int gameSize) {

        this.main = main;
        this.chatManager = chatManager;
        this.entityManager = entityManager;
        this.itemManager = itemManager;

        this.gameSize = gameSize;

        this.games = new ArrayList<>();

    }

    public Game findGame(int openSlots, Map map) {

        Game game = null;

        int i = 0;

        while (game == null && i < games.size()) {

            if (gameSize - games.get(i).getPlayerCount() >= openSlots) {

                game = games.get(i);

            }

            i++;

        }

        if (game == null) {

            game = new Game(main, chatManager, entityManager, this, itemManager, gameSize, map);
            games.add(game);

        }

        return game;

    }

    public Game findEmptyGame(Map map) {

        Game game = new Game(main, chatManager, entityManager, this, itemManager, gameSize, map);

        games.add(game);

        return game;

    }

    public Game getGame(Player player) {

        Game game = null;

        int i = 0;

        while (game == null && i < games.size()) {

            if (games.get(i).isPlaying(player) || games.get(i).isSpectating(player)) {

                game = games.get(i);

            }

            i++;

        }

        return game;

    }

    public void removeGame(Game game) {

        games.remove(game);

    }

    public void removePlayer(Player player) {

        Game game = getGame(player);

        if (game != null) {

            game.removePlayer(player, true);

            if (game.getPlayerCount() == 0) {

                game.end(false);
                games.remove(game);

            }

        }

    }

    public void removeSpectator(Player player) {

        Game game = getGame(player);

        if (game != null) {

            game.removeSpectator(player);

        }

    }

    public boolean isPlaying(Player player) {

        return getGame(player) != null;

    }

    public boolean isSpectating(Player player) {

        return getGame(player) != null;

    }

    public void clearGames() {

        for (Game game : new ArrayList<>(games)) {

            game.end(false);

        }

    }

}
