package com.kaixeleron.horde.game.runnable;

import com.kaixeleron.horde.game.data.Game;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class FireworkRunnable extends BukkitRunnable {

    private final Game game;
    private final Random random;

    public FireworkRunnable(Game game) {

        this.game = game;
        random = new Random();

    }

    @Override
    public void run() {

        for (Player player : game.getPlayers()) {

            Firework firework = player.getWorld().spawn(player.getLocation(), Firework.class);

            FireworkMeta fireworkMeta = firework.getFireworkMeta();
            fireworkMeta.setPower(1);
            fireworkMeta.addEffect(FireworkEffect.builder().with(FireworkEffect.Type.values()[random.nextInt(FireworkEffect.Type.values().length) & Integer.MAX_VALUE]).withColor(Color.fromRGB(random.nextInt(255) & Integer.MAX_VALUE, random.nextInt(255) & Integer.MAX_VALUE, random.nextInt(255) & Integer.MAX_VALUE)).build());

            firework.setFireworkMeta(fireworkMeta);

        }

    }

}
