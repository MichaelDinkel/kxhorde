package com.kaixeleron.horde.game.runnable;

import com.kaixeleron.horde.entity.EntityManager;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class RunnableClassSelectHighlighter extends BukkitRunnable {

    private final Game game;

    private final Location[] npcLocs;

    private final EntityManager entityManager;

    public RunnableClassSelectHighlighter(Game game, Location[] npcLocs, EntityManager entityManager) {

        this.game = game;
        this.npcLocs = npcLocs;
        this.entityManager = entityManager;

    }

    @Override
    public void run() {

        for (Player player : game.getSelectingClass()) {

            Location location = player.getLocation();

            Vector origin = location.clone().toVector();
            Vector direction = location.getDirection().clone();

            boolean found = false;

            for (int i = 0; i < npcLocs.length; i++) {

                Location npcFeet = npcLocs[i].clone().add(0.0D, -1.0D, 0.0D);

                Vector point = direction.clone().normalize().multiply(npcLocs[i].distance(location)).add(origin);

                if (point.distance(npcLocs[i].toVector()) < 0.5D || point.distance(npcFeet.toVector()) < 0.5D) {

                    entityManager.highlightClassSelectEntity(player, i);
                    found = true;

                }

            }

            if (!found) {

                entityManager.highlightClassSelectEntity(player, -1);

            }

        }

    }

}
