package com.kaixeleron.horde.game.runnable;

import com.kaixeleron.horde.game.data.Game;
import org.bukkit.scheduler.BukkitRunnable;

public class RunnableCountdown extends BukkitRunnable {

    private final Game game;

    public RunnableCountdown(Game game) {

        this.game = game;

    }

    @Override
    public void run() {

        game.decrementCountdown();

    }

}
