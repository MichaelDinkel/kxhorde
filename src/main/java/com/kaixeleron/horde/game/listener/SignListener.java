package com.kaixeleron.horde.game.listener;

import com.kaixeleron.horde.HordeMain;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class SignListener implements Listener {

    private final HordeMain main;

    private final List<Player> debounce;
    private final Material[] signs;

    public SignListener(HordeMain main) {

        this.main = main;

        List<Material> signTypes = new ArrayList<>();

        for (Material material : Material.values()) {

            if (material.getKey().getKey().endsWith("sign")) {

                signTypes.add(material);

            }

        }

        signs = signTypes.toArray(new Material[0]);

        debounce = new ArrayList<>();

    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {

        if (event.getPlayer().hasPermission("kxhorde.game.signcreate")) {

            String[] lines = event.getLines();

            if (lines[0].equalsIgnoreCase("[Horde]")) {

                if (lines[1].equalsIgnoreCase("Join")) {

                    if (lines[2].equalsIgnoreCase("In progress")) {

                        event.setLine(0, ChatColor.RED + "" + ChatColor.BOLD + "Horde");
                        event.setLine(1, ChatColor.YELLOW + "Join");
                        event.setLine(2, ChatColor.AQUA + "In progress");

                    } else if (lines[2].equalsIgnoreCase("Empty")) {

                        event.setLine(0, ChatColor.RED + "" + ChatColor.BOLD + "Horde");
                        event.setLine(1, ChatColor.YELLOW + "Join");
                        event.setLine(2, ChatColor.AQUA + "Empty");

                    }

                } else if (lines[1].equalsIgnoreCase("leave")) {

                    event.setLine(0, ChatColor.RED + "" + ChatColor.BOLD + "Horde");
                    event.setLine(1, ChatColor.YELLOW + "Leave");

                }

            }

        }

    }

    @EventHandler
    public void onSignClick(final PlayerInteractEvent event) {

        if (!debounce.contains(event.getPlayer())) {

            if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

                if (event.getClickedBlock() != null && isSign(event.getClickedBlock().getType())) {

                    Sign sign = (Sign) event.getClickedBlock().getState();
                    String[] lines = sign.getLines();

                    if (lines[0].equals(ChatColor.RED + "" + ChatColor.BOLD + "Horde")) {

                        if (lines[1].equals(ChatColor.YELLOW + "Join")) {

                            if (lines[2].equals(ChatColor.AQUA + "In progress")) {

                                event.getPlayer().performCommand("horde join");

                            } else if (lines[2].equals(ChatColor.AQUA + "Empty")) {

                                event.getPlayer().performCommand("horde join §rand empty");

                            }

                        } else if (lines[1].equals(ChatColor.YELLOW + "Leave")) {

                            event.getPlayer().performCommand("horde leave");

                        }

                        debounce.add(event.getPlayer());

                        new BukkitRunnable() {

                            @Override
                            public void run() {

                                debounce.remove(event.getPlayer());

                            }

                        }.runTaskLater(main, 1L);

                    }

                }

            }

        }

    }

    private boolean isSign(Material type) {

        boolean found = false;
        int i = 0;

        while (!found && i < signs.length) {

            if (type.equals(signs[i])) {

                found = true;

            }

            i++;

        }

        return found;

    }

}
