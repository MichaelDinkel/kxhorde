package com.kaixeleron.horde.game.listener;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.game.GameManager;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class GameListener extends PacketAdapter implements Listener {

    private final GameManager gameManager;

    public GameListener(HordeMain main, GameManager gameManager) {

        super(main, PacketType.Play.Server.ADVANCEMENTS);
        this.gameManager = gameManager;

    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {

        if (gameManager.isPlaying((Player) event.getEntity())) {

            event.setCancelled(true);

        }

    }

    @EventHandler
    public void onFallDamage(EntityDamageEvent event) {

        if (event.getCause().equals(EntityDamageEvent.DamageCause.FALL) && event.getEntity() instanceof Player player) {

            Game game = gameManager.getGame(player);

            if (game != null) {

                if (game.isSelectingClass(player)) {

                    event.setCancelled(true);

                }

            }

        }

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        quit(event.getPlayer());

    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {

        quit(event.getPlayer());

    }

    private void quit(Player player) {

        Game game = gameManager.getGame(player);

        if (game != null) {

            game.removePlayer(player, true);

        }

    }

    @Override
    public void onPacketSending(PacketEvent event) {

        if (event.getPacketType().equals(PacketType.Play.Server.ADVANCEMENTS) && gameManager.isPlaying(event.getPlayer())) {

            event.setCancelled(true);

        }

    }

}
