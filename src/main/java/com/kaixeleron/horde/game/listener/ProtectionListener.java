package com.kaixeleron.horde.game.listener;

import com.kaixeleron.horde.game.GameManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import static com.kaixeleron.horde.game.GameManager.PREFIX;

public class ProtectionListener implements Listener {

    private final String[] allowedCommands;

    private final GameManager gameManager;

    public ProtectionListener(String[] allowedCommands, GameManager gameManager) {

        this.allowedCommands = allowedCommands;
        this.gameManager = gameManager;

    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {

        if (gameManager.isPlaying(event.getPlayer())) {

            event.setCancelled(true);

        }

    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {

        if (gameManager.isPlaying(event.getPlayer())) {

            event.setCancelled(true);

        }

    }

    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {

        if (event.getRemover() instanceof Player && gameManager.isPlaying((Player) event.getRemover())) {

            event.setCancelled(true);

        }

    }

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent event) {

        if (gameManager.isPlaying(event.getPlayer())) {

            event.setCancelled(true);

        }

    }

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent event) {

        if (gameManager.isPlaying(event.getPlayer())) {

            event.setCancelled(true);

        }

    }

    @EventHandler
    public void onDamageEntity(EntityDamageByEntityEvent event) {

        if (event.getEntity() instanceof ItemFrame && event.getDamager() instanceof Player player) {

            if (gameManager.isPlaying(player)) {

                event.setCancelled(true);

            }

        }

    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {

        if (gameManager.isPlaying(event.getPlayer())) {

            if (event.getAction().equals(Action.PHYSICAL) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {

                event.setCancelled(true);

            }

        }

    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {

        if (!event.getPlayer().hasPermission("kxhorde.game.bypasscommands") && gameManager.isPlaying(event.getPlayer())) {

            String command = event.getMessage().substring(1).toLowerCase();

            if (command.contains(" ")) {

                command = command.split(" ")[0];

            }

            boolean block = true;

            int i = 0;

            while (block && i < allowedCommands.length) {

                if (allowedCommands[i].equals(command)) {

                    block = false;

                }

                i++;

            }

            if (block) {

                event.setCancelled(true);
                event.getPlayer().sendMessage(PREFIX + ChatColor.RED + "You may not use that command while in a match.");

            }

        }

    }

}
