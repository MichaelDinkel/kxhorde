package com.kaixeleron.horde.game.command;

import com.kaixeleron.horde.game.GameManager;
import com.kaixeleron.horde.game.data.Game;
import com.kaixeleron.horde.localization.Localization;
import com.kaixeleron.horde.localization.LocalizationManager;
import com.kaixeleron.horde.map.MapManager;
import com.kaixeleron.horde.map.data.Map;
import com.kaixeleron.horde.party.PartyManager;
import com.kaixeleron.horde.party.data.Party;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;

public class CommandHorde implements CommandExecutor {

    private final GameManager gameManager;
    private final LocalizationManager localizationManager;
    private final MapManager mapManager;
    private final PartyManager partyManager;

    public CommandHorde(GameManager gameManager, LocalizationManager localizationManager, MapManager mapManager, PartyManager partyManager) {

        this.gameManager = gameManager;
        this.localizationManager = localizationManager;
        this.mapManager = mapManager;
        this.partyManager = partyManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player player) {

            if (args.length == 0) {

                help(player, label);

            } else {

                switch (args[0].toLowerCase()) {

                    case "join":
                    case "j":

                        join(player, args.length > 1 ? (args[1].equals("§rand") ? null : args[1]) : null, args.length > 2 && args[2].equalsIgnoreCase("empty"));
                        break;

                    case "leave":
                    case "l":

                        leave(player);
                        break;

                    case "spectate":
                    case "s":

                        spectate(player, label, args.length > 1 ? args[1] : null);
                        break;

                    case "kick":

                        if (player.hasPermission("kxhorde.command.horde.kick")) {

                            kick(player, label, args.length > 1 ? args[1] : null);

                        } else {

                            help(player, label);

                        }

                        break;

                    default:

                        help(player, label);

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

    private void help(Player player, String label) {

        Localization localization = localizationManager.getLocalization(player.getLocale());
        
        boolean playing = gameManager.isPlaying(player);

        player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix") + ChatColor.AQUA + localization.getLocalizedString("game.help.header"));
        player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.YELLOW + "/" + label + (playing ? ChatColor.RED : "") + " join" + ChatColor.YELLOW + " [map] [empty] " + ChatColor.RESET + "- " + localization.getLocalizedString("game.help.join"));
        player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.YELLOW + "/" + label + (!playing ? ChatColor.RED : "") + " leave " + ChatColor.RESET + "- " + localization.getLocalizedString("game.help.leave"));
        player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.YELLOW + "/" + label + (playing ? ChatColor.RED : "") + " spectate" + ChatColor.YELLOW + " <player> " + ChatColor.RESET + "- " + localization.getLocalizedString("game.help.spectate"));

        if (player.hasPermission("kxhorde.command.horde.kick")) {

            player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.YELLOW + "/" + label + (!playing ? ChatColor.RED : "") + " kick" + ChatColor.YELLOW + " <player> " + ChatColor.RESET + "- " + localization.getLocalizedString("game.help.kick"));

        }

    }

    private void join(Player player, @Nullable String mapName, boolean empty) {

        Localization localization = localizationManager.getLocalization(player.getLocale());

        if (gameManager.isPlaying(player)) {

            player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + localization.getLocalizedString("game.error.inmatch"));

        } else {

            Party party = partyManager.getParty(player);

            Map map;

            if (mapName == null) {

                map = mapManager.getRandomMap();

            } else {

                map = mapManager.getMap(mapName);

            }

            if (map == null) {

                player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + String.format(localization.getLocalizedString("game.error.nomap"), ChatColor.RESET + mapName + ChatColor.RED));

            } else {

                Game game;

                if (empty) {

                    game = gameManager.findEmptyGame(map);

                } else {

                    game = gameManager.findGame(party == null ? 1 : party.getMemberCount(), map);

                }

                if (party == null) {

                    game.addPlayer(player);

                } else {

                    for (Player member : party.getMembers()) {

                        game.addPlayer(member);

                    }

                }

            }

        }

    }

    private void leave(Player player) {

        if (gameManager.isPlaying(player)) {

            gameManager.removePlayer(player);

        } else if (gameManager.isSpectating(player)) {

            gameManager.removeSpectator(player);

        } else {

            Localization localization = localizationManager.getLocalization(player.getLocale());

            player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + localization.getLocalizedString("game.error.notinmatch"));

        }

    }

    private void spectate(Player player, String label, @Nullable String targetName) {

        Localization localization = localizationManager.getLocalization(player.getLocale());

        if (targetName == null) {

            player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.AQUA + localization.getLocalizedString("game.spectate.player"));
            player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + localization.getLocalizedString("global.usage") + ChatColor.RESET + "/" + label + " spectate <player>");

        } else {

            Player target = Bukkit.getPlayerExact(targetName);

            if (target == null || !player.canSee(target)) {

                player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + String.format(localization.getLocalizedString("global.error.noplayer"), ChatColor.RESET + targetName + ChatColor.RED));

            } else {

                Game game = gameManager.getGame(target);

                if (game == null) {

                    player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + String.format(localization.getLocalizedString("global.error.playernomatch"), ChatColor.RESET + targetName + ChatColor.RED));

                } else {

                    if (gameManager.isSpectating(player)) {

                        Game spectating = gameManager.getGame(player);

                        if (!spectating.equals(game)) {

                            spectating.removeSpectator(player);
                            game.addSpectator(player);

                        }

                    } else {

                        game.addSpectator(player);

                    }

                    player.setGameMode(GameMode.SPECTATOR);
                    player.teleport(target);

                    player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.AQUA + String.format(localization.getLocalizedString("game.spectate.success"), ChatColor.RESET + target.getName() + ChatColor.AQUA, ChatColor.RESET + "/leave" + ChatColor.AQUA));

                }

            }

        }

    }

    private void kick(Player player, String label, @Nullable String targetName) {

        Localization localization = localizationManager.getLocalization(player.getLocale());

        if (targetName == null) {

            player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.AQUA + localization.getLocalizedString("game.help.kick"));
            player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + localization.getLocalizedString("global.usage")+ ChatColor.RESET + "/" + label + " kick <player>");

        } else {

            Game game = gameManager.getGame(player);

            if (game == null) {

                player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + localization.getLocalizedString("game.error.notinmatch"));

            } else {

                Player target = Bukkit.getPlayerExact(targetName);

                if (target == null) {

                    player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + String.format(localization.getLocalizedString("global.error.noplayer"), ChatColor.RESET + targetName + ChatColor.RED));

                } else {

                    if (game.isPlaying(target)) {

                        gameManager.removePlayer(target);
                        player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.AQUA + String.format(localization.getLocalizedString("game.kick.success"), ChatColor.RESET + target.getName() + ChatColor.AQUA));

                    } else if (game.isSpectating(target)) {

                        gameManager.removeSpectator(target);
                        player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.AQUA + String.format(localization.getLocalizedString("game.kick.spectatorsuccess"), ChatColor.RESET + target.getName() + ChatColor.AQUA));

                    } else {

                        player.sendMessage(ChatColor.BLUE + localization.getLocalizedString("game.prefix")  + ChatColor.RED + String.format(localization.getLocalizedString("game.error.notinmymatch"), ChatColor.RESET + target.getName() + ChatColor.RED));

                    }

                }

            }

        }

    }

}
