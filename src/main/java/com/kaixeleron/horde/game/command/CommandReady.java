package com.kaixeleron.horde.game.command;

import com.kaixeleron.horde.game.GameManager;
import com.kaixeleron.horde.game.data.Game;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.kaixeleron.horde.game.GameManager.PREFIX;

public class CommandReady implements CommandExecutor {

    private final GameManager gameManager;

    public CommandReady(GameManager gameManager) {

        this.gameManager = gameManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player player) {

            Game game = gameManager.getGame(player);

            if (game == null) {

                player.sendMessage(PREFIX + ChatColor.RED + "You are not in a match.");

            } else {

                game.toggleReady(player);

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

        return true;

    }

}
