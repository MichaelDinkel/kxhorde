package com.kaixeleron.horde.game.data;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.chat.ChatManager;
import com.kaixeleron.horde.data.HordePlayerClassType;
import com.kaixeleron.horde.entity.EntityManager;
import com.kaixeleron.horde.entity.data.HordeEntity;
import com.kaixeleron.horde.game.GameManager;
import com.kaixeleron.horde.game.runnable.FireworkRunnable;
import com.kaixeleron.horde.game.runnable.RunnableClassSelectHighlighter;
import com.kaixeleron.horde.game.runnable.RunnableCountdown;
import com.kaixeleron.horde.items.ItemManager;
import com.kaixeleron.horde.items.data.HordeItem;
import com.kaixeleron.horde.items.data.HordeItemType;
import com.kaixeleron.horde.items.data.ItemSlot;
import com.kaixeleron.horde.map.data.Map;
import com.kaixeleron.horde.mission.data.HordeBotData;
import com.kaixeleron.horde.mission.data.Mission;
import com.kaixeleron.horde.mission.data.Subwave;
import com.kaixeleron.horde.mission.data.Wave;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.kaixeleron.horde.game.GameManager.PREFIX;

public class Game {

    private static final int PLAYERS_TO_START = 1;

    private final HordeMain main;

    private final List<Player> selectingClass, players, spectators, ready;
    private final List<HordeEntity> activeEntities;

    private final ChatManager chatManager;

    private final EntityManager entityManager;

    private final GameManager gameManager;

    private final ItemManager itemManager;

    private boolean running = false;

    private final int size;

    private final Map map;

    private final Mission mission;

    int currentSpawn = 0, currentCountdown = GameManager.MAX_TIMER;

    int currentWave = 0, currentSubwave = 0;

    int waveBots = 0, waveBotsRemaining = 0;

    private final BossBar statusBar, instructionBar, countdown, botsRemaining;

    private RunnableCountdown runnableCountdown = null;

    private RunnableClassSelectHighlighter classSelectHighlighter = null;

    private FireworkRunnable fireworkRunnable = null;

    public Game(HordeMain main, ChatManager chatManager, EntityManager entityManager, GameManager gameManager, ItemManager itemManager, int initialSize, Map map) {

        this.main = main;

        selectingClass = new ArrayList<>();
        players = new ArrayList<>(initialSize);
        spectators = new ArrayList<>();
        ready = new ArrayList<>();
        activeEntities = new ArrayList<>();

        this.chatManager = chatManager;
        this.entityManager = entityManager;
        this.gameManager = gameManager;
        this.itemManager = itemManager;

        size = initialSize;

        this.map = map;

        Mission[] missions = map.listMissions();

        this.mission = missions[new Random().nextInt(missions.length)];

        statusBar = Bukkit.createBossBar("", BarColor.BLUE, BarStyle.SOLID);
        instructionBar = Bukkit.createBossBar("", BarColor.RED, BarStyle.SOLID);
        countdown = Bukkit.createBossBar(ChatColor.YELLOW + Integer.toString(GameManager.MAX_TIMER), BarColor.YELLOW, BarStyle.SOLID);
        botsRemaining = Bukkit.createBossBar(ChatColor.GREEN + "Wave " + ChatColor.RESET + (currentWave + 1) + ChatColor.GREEN + "/" + ChatColor.RESET + mission.listWaves().length, BarColor.GREEN, BarStyle.SOLID);

    }

    private void start() {

        ready.clear();
        running = true;

        nextWave();

    }

    public void end(boolean normal) {

        if (normal) {

            long time = 0L;

            if (players.size() > 0) {

                fireworkRunnable = new FireworkRunnable(this);

                time = 100L;

                fireworkRunnable.runTaskTimer(main, 10L, 10L);

                botsRemaining.removeAll();

                for (Player player : players) {

                    player.sendMessage(PREFIX + ChatColor.AQUA + "Congratulations! You won!");

                }

            }

            new BukkitRunnable() {

                @Override
                public void run() {

                    clearPlayers();
                    clearSpectators();

                    if (fireworkRunnable != null) {

                        fireworkRunnable.cancel();

                    }

                }

            }.runTaskLater(main, time);

        } else {

            clearPlayers();
            clearSpectators();

        }

        gameManager.removeGame(this);

    }

    private void nextWave() {

        currentSubwave = 0;

        Wave[] waves = mission.listWaves();

        if (currentWave < waves.length) {

            for (Subwave subwave : waves[currentSubwave].listSubwaves()) {

                waveBotsRemaining += subwave.listBots().length;

            }

            waveBots = waveBotsRemaining;

            botsRemaining.setTitle(ChatColor.GREEN + Integer.toString(waveBotsRemaining));

            nextSubwave(waves[currentWave]);

            currentWave++;

        }

    }

    private void nextSubwave(Wave wave) {

        Subwave[] subwaves = wave.listSubwaves();

        if (currentSubwave < subwaves.length) {

            //TODO bug with multi bot subwaves

            HordeBotData[] bots = subwaves[currentSubwave].listBots();

            for (HordeBotData bot : bots) {

                double armorFactor = 0, weaponFactor = 0;

                HordeEntity entity = entityManager.generateEntity(bot.getEntityType(), bot.getName(), bot.getHealth(), bot.getSpeed(), armorFactor, weaponFactor, map.getMainPath().getPoints()[0].getBukkitLocation());
                activeEntities.add(entity);

                for (Player player : players) {

                    if (!selectingClass.contains(player)) {

                        entityManager.showEntity(entity, player);

                    }

                }

                for (Player spectator : spectators) {

                    entityManager.showEntity(entity, spectator);

                }

                entity.followPath(map.getMainPath());

            }

            currentSubwave++;

        } else {

            endWave();

        }

    }

    private void endWave() {

        botsRemaining.setProgress(1.0D);

        waveBots = 0;

        if (currentWave >= mission.listWaves().length) {

            end(true);

        } else {

            running = false;

            botsRemaining.setTitle(ChatColor.GREEN + "Wave " + ChatColor.RESET + (currentWave + 1) + ChatColor.GREEN + "/" + ChatColor.RESET + mission.listWaves().length );

            for (Player player : players) {

                recalculateStatusBar();
                recalculateInstructionBar();

                statusBar.addPlayer(player);
                instructionBar.addPlayer(player);

            }

        }

    }

    public void damageEntity(Player player, int entityID) {

        HordeEntity entity = null;

        int i = 0;

        while (entity == null && i < activeEntities.size()) {

            if (activeEntities.get(i).getId() == entityID) {

                entity = activeEntities.get(i);

            }

            i++;

        }

        if (entity != null) {

            double distance = entity.getLocation().distance(player.getLocation());

            if (distance < 5.0D) {

                // TODO calculate damage
                entity.damage(2.0D);

            }/* else if (distance > 8.D) {

                player.kickPlayer(PREFIX + ChatColor.RED + "Suspected reach.");

            }*/

        }

    }

    public void addPlayer(Player player) {

        player.setFoodLevel(20);

        players.add(player);
        selectingClass.add(player);

        for (Player inGame : players) {

            inGame.sendMessage(PREFIX + chatManager.getChatColor(player) + player.getName() + ChatColor.AQUA + " joined the match (" + ChatColor.RESET + players.size() + ChatColor.YELLOW + "/" + ChatColor.RESET + size + ChatColor.AQUA + ").");

        }

        player.teleport(map.getClassSelectLocation());
        player.setGameMode(GameMode.SURVIVAL);

        Location[] npcLocs = entityManager.displayClassSelectEntities(player);

        if (classSelectHighlighter == null) {

            classSelectHighlighter = new RunnableClassSelectHighlighter(this, npcLocs, entityManager);
            classSelectHighlighter.runTaskTimer(main, 2L, 2L);

        }

        recalculateStatusBar();
        recalculateInstructionBar();

        botsRemaining.addPlayer(player);
        statusBar.addPlayer(player);

        player.setPlayerTime(mission.getTime(), false);

    }

    @SuppressWarnings("ConstantConditions")
    public void setClass(Player player, HordePlayerClassType classType) {

        selectingClass.remove(player);

        if (selectingClass.isEmpty()) {

            classSelectHighlighter.cancel();
            classSelectHighlighter = null;

        }

        Location[] spawns = map.listSpawns();

        player.teleport(spawns[currentSpawn]);

        currentSpawn++;

        if (currentSpawn >= spawns.length) {

            currentSpawn = 0;

        }

        HordeItem helmet = null, chestplate = null, leggings = null, boots = null, primary = null, secondary = null, tertiary = null, quaternary = null, quinary = null;

        //TODO player loadouts

        for (HordeItemType type : itemManager.getStockItems()) {

            for (HordePlayerClassType playerClassType : type.getPlayerClassTypes()) {

                if (playerClassType.equals(classType)) {

                    switch (type.getSlot()) {
                        case HELMET -> helmet = itemManager.generateItem(player, this, type);
                        case CHESTPLATE -> chestplate = itemManager.generateItem(player, this, type);
                        case LEGGINGS -> leggings = itemManager.generateItem(player, this, type);
                        case BOOTS -> boots = itemManager.generateItem(player, this, type);
                        case PRIMARY -> primary = itemManager.generateItem(player, this, type);
                        case SECONDARY -> secondary = itemManager.generateItem(player, this, type);
                        case TERTIARY -> tertiary = itemManager.generateItem(player, this, type);
                        case QUATERNARY -> quaternary = itemManager.generateItem(player, this, type);
                        case QUINARY -> quinary = itemManager.generateItem(player, this, type);
                    }

                }

            }

        }

        if (helmet != null) {

            player.getInventory().setHelmet(helmet.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.HELMET, helmet);

        }

        if (chestplate != null) {

            player.getInventory().setChestplate(chestplate.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.CHESTPLATE, chestplate);

        }

        if (leggings != null) {

            player.getInventory().setLeggings(leggings.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.LEGGINGS, leggings);

        }

        if (boots != null) {

            player.getInventory().setBoots(boots.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.BOOTS, boots);

        }

        if (primary != null) {

            player.getInventory().setItem(0, primary.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.PRIMARY, primary);

        }

        if (secondary != null) {

            player.getInventory().setItem(1, secondary.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.SECONDARY, secondary);

        }

        if (tertiary != null) {

            player.getInventory().setItem(2, tertiary.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.TERTIARY, tertiary);

        }

        if (quaternary != null) {

            player.getInventory().setItem(3, quaternary.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.QUATERNARY, quaternary);

        }

        if (quinary != null) {

            player.getInventory().setItem(4, quinary.getMinecraftItem());
            itemManager.assignItem(player, ItemSlot.QUINARY, quinary);

        }

        itemManager.refreshPlayerStatus(player, player.getInventory().getHeldItemSlot());

        entityManager.clearClassSelectEntities(player);

        player.sendMessage(PREFIX + ChatColor.AQUA + "Selected class " + ChatColor.RESET + classType.name().toLowerCase() + ChatColor.AQUA + ".");

        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue() * classType.getHealthMultiplier());
        player.setWalkSpeed((float) (player.getWalkSpeed() * classType.getSpeedMultiplier()));

        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());

        recalculateStatusBar();
        instructionBar.addPlayer(player);

        for (HordeEntity entity : activeEntities) {

            entityManager.showEntity(entity, player);

        }

    }

    @SuppressWarnings("ConstantConditions")
    public void removePlayer(Player player, boolean announce) {

        if (selectingClass.contains(player)) {

            entityManager.clearClassSelectEntities(player);

        }

        selectingClass.remove(player);
        players.remove(player);

        player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
        player.setGameMode(Bukkit.getDefaultGameMode());

        for (HordeEntity entity : activeEntities) {

            entityManager.hideEntity(entity, player);

        }

        if (announce) {

            for (Player inGame : players) {

                inGame.sendMessage(PREFIX + chatManager.getChatColor(player) + player.getName() + ChatColor.AQUA + " left the match.");

            }

            player.sendMessage(PREFIX + ChatColor.AQUA + "Left the match.");

        }

        recalculateStatusBar();
        recalculateInstructionBar();

        statusBar.removePlayer(player);
        instructionBar.removePlayer(player);
        countdown.removePlayer(player);
        botsRemaining.removePlayer(player);

        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());
        player.setWalkSpeed(0.2F);

        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());

        player.getInventory().clear();
        player.getInventory().setHelmet(new ItemStack(Material.AIR));
        player.getInventory().setChestplate(new ItemStack(Material.AIR));
        player.getInventory().setLeggings(new ItemStack(Material.AIR));
        player.getInventory().setBoots(new ItemStack(Material.AIR));

        player.resetPlayerTime();

        itemManager.removePlayer(player);

    }

    public List<Player> getPlayers() {

        return players;

    }

    public boolean isPlaying(Player player) {

        return players.contains(player);

    }

    public void clearPlayers() {

        List<Player> copy = new ArrayList<>(players);

        for (Player player : copy) {

            removePlayer(player, false);

        }

    }

    public int getPlayerCount() {

        return players.size();

    }

    public void addSpectator(Player player) {

        spectators.add(player);

        botsRemaining.addPlayer(player);

    }

    public void removeSpectator(Player player) {

        spectators.remove(player);

        botsRemaining.removePlayer(player);

        player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
        player.setGameMode(Bukkit.getDefaultGameMode());

        player.sendMessage(PREFIX + ChatColor.AQUA + "Stopped spectating.");

    }

    public boolean isSpectating(Player player) {

        return spectators.contains(player);

    }

    public void clearSpectators() {

        List<Player> copy = new ArrayList<>(spectators);

        for (Player player : copy) {

            removeSpectator(player);

        }

    }

    public List<Player> getSpectators() {

        return spectators;

    }

    public List<Player> getSelectingClass() {

        return selectingClass;

    }

    public boolean isSelectingClass(Player player) {

        return selectingClass.contains(player);

    }

    public void toggleReady(Player player) {

        if (running) {

            player.sendMessage(PREFIX + ChatColor.RED + "A wave is already in progress.");

        } else if (players.size() < PLAYERS_TO_START) {

            player.sendMessage(PREFIX + ChatColor.RED + "More players are required in order to start.");

        } else if (selectingClass.contains(player)) {

            player.sendMessage(PREFIX + ChatColor.RED + "You must select a class before readying up.");

        } else {

            if (ready.contains(player)) {

                ready.remove(player);
                countdown.removePlayer(player);
                instructionBar.addPlayer(player);

            } else {

                ready.add(player);
                instructionBar.removePlayer(player);
                countdown.addPlayer(player);

            }

            calculateCountdown();

            recalculateStatusBar();

            for (Player inGame : players) {

                inGame.sendMessage(PREFIX + chatManager.getChatColor(player) + player.getName() + ChatColor.AQUA + " is " + (ready.contains(player) ? "now" : "no longer") + " ready.");

            }

        }

    }

    private void recalculateStatusBar() {

        StringBuilder barBuilder = new StringBuilder();

        for (Player player : players) {

            ChatColor color = selectingClass.contains(player) ? ChatColor.GRAY : ChatColor.AQUA;

            barBuilder.append(color);
            barBuilder.append("[");

            if (ready.contains(player)) {

                barBuilder.append(ChatColor.GREEN);
                barBuilder.append("█");
                barBuilder.append(color);

            } else {

                barBuilder.append(" ");

            }

            barBuilder.append("] ");
            barBuilder.append(player.getName());

        }

        statusBar.setTitle(barBuilder.toString());

    }

    private void recalculateInstructionBar() {

        if (players.size() >= PLAYERS_TO_START) {

            instructionBar.setTitle(ChatColor.RED + "Type " + ChatColor.YELLOW + "/ready" + ChatColor.RED + " to ready up.");

        } else {

            instructionBar.setTitle(ChatColor.RED + "Waiting for players.");

        }

    }

    private void calculateCountdown() {

        if (ready.isEmpty()) {

            if (runnableCountdown != null) {

                runnableCountdown.cancel();
                runnableCountdown = null;

            }

            countdown.setTitle(ChatColor.YELLOW + Integer.toString(GameManager.MAX_TIMER));
            currentCountdown = GameManager.MAX_TIMER;

        } else {

            int time = GameManager.MAX_TIMER - (GameManager.MAX_TIMER * (ready.size() / players.size()));

            if (ready.size() == players.size() || time < 10) {

                time = 10;

            }

            if (currentCountdown > time) {

                currentCountdown = time;

            }

            countdown.setTitle(ChatColor.YELLOW + Integer.toString(time));

            if (runnableCountdown == null) {

                runnableCountdown = new RunnableCountdown(this);
                runnableCountdown.runTaskTimer(main, 20L, 20L);

            }

        }

    }

    public void decrementCountdown() {

        currentCountdown--;

        if (currentCountdown <= 0) {

            if (runnableCountdown != null) {

                runnableCountdown.cancel();
                runnableCountdown = null;

            }

            currentCountdown = GameManager.MAX_TIMER;
            countdown.removeAll();
            statusBar.removeAll();
            countdown.setTitle(ChatColor.YELLOW + Integer.toString(GameManager.MAX_TIMER));

            start();

        } else {

            countdown.setTitle(ChatColor.YELLOW + Integer.toString(currentCountdown));

        }

    }

    public void removeEntity(HordeEntity entity) {

        activeEntities.remove(entity);

        waveBotsRemaining--;

        botsRemaining.setTitle(ChatColor.GREEN + Integer.toString(waveBotsRemaining));
        //botsRemaining.setProgress((double) waveBotsRemaining / (double) waveBots / 100.0D);

        if (activeEntities.isEmpty()) {

            nextSubwave(mission.listWaves()[currentWave - 1]);

        }

    }

    public List<HordeEntity> getActiveEntities() {

        return activeEntities;

    }

}
