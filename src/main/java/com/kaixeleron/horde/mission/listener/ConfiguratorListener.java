package com.kaixeleron.horde.mission.listener;

import com.kaixeleron.horde.mission.util.MissionConfigurator;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ConfiguratorListener implements Listener {

    private final MissionConfigurator configurator;

    public ConfiguratorListener(MissionConfigurator configurator) {

        this.configurator = configurator;

    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.getWhoClicked() instanceof Player player) {

            if (configurator.isConfiguring(player)) {

                event.setCancelled(true);

                switch (event.getClick()) {
                    case LEFT -> configurator.fireLeftClick(player, event.getCurrentItem(), event.getSlot());
                    case RIGHT -> configurator.fireRightClick(player, event.getCurrentItem(), event.getSlot());
                }

            }

        }

    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {

        if (event.getPlayer() instanceof Player player) {

            if (configurator.isConfiguring(player) && !configurator.isSwapping(player)) {

                configurator.stopConfiguring(player);

            }

        }

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {

        if (configurator.fireChat(event.getPlayer(), event.getMessage())) {

            event.setCancelled(true);

        }

    }

}
