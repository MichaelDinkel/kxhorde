package com.kaixeleron.horde.mission;

import com.kaixeleron.horde.mission.data.Mission;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MissionManager {

    private final File missionsFolder;

    private final List<Mission> missions;

    private static MissionManager instance;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public MissionManager(File dataFolder) {

        missionsFolder = new File(dataFolder, "missions");
        missionsFolder.mkdir();

        missions = new ArrayList<>();

        MissionManager.instance = this;

    }

    public void addMission(Mission mission) {

        missions.add(mission);

    }

    public void removeMission(Mission mission) {

        missions.remove(mission);

    }

    public Mission[] listMissions() {

        return missions.toArray(new Mission[0]);

    }

    public boolean missionExists(String mission) {

        return getMission(mission) != null;

    }

    public Mission getMission(String name) {

        Mission mission = null;

        int i = 0;

        while (mission == null && i < missions.size()) {

            if (missions.get(i).getName().equals(name)) {

                mission = missions.get(i);

            }

            i++;

        }

        return mission;

    }

    public void clearMissions() {

        for (Mission mission : missions) {

            mission.clearWaves();

        }

        missions.clear();

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public boolean saveMission(Mission mission) {

        boolean success = true;

        File missionFile = new File(missionsFolder, String.format("%s.mission", mission.getName()));

        try {

            missionFile.createNewFile();

        } catch (IOException e) {

            success = false;
            System.err.printf("Could not create mission file %s.mission.%n", mission.getName());

        }

        if (success) {

            try (ObjectOutputStream outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(missionFile)))) {

                outputStream.writeObject(mission);
                outputStream.flush();

            } catch (IOException e) {

                success = false;
                System.err.printf("Could not write to mission file %s.mission.%n", mission.getName());
                e.printStackTrace();

            }

        }

        return success;

    }

    public boolean deleteMission(Mission mission) {

        return new File(missionsFolder, String.format("%s.mission", mission.getName())).delete();

    }

    public void loadMissions() throws IOException {

        File[] missions = missionsFolder.listFiles();

        if (missions != null) {

            for (File mission : missions) {

                if (mission.getName().endsWith(".mission")) {

                    ObjectInputStream inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(mission)));

                    try {

                        this.missions.add((Mission) inputStream.readObject());

                    } catch (ClassNotFoundException ignored) {}

                    inputStream.close();

                }

            }

        }

    }

    public static MissionManager getInstance() {

        return instance;

    }

}
