package com.kaixeleron.horde.mission.util;

import com.kaixeleron.horde.HordeMain;
import com.kaixeleron.horde.entity.data.HordeEntityType;
import com.kaixeleron.horde.mission.MissionManager;
import com.kaixeleron.horde.mission.data.HordeBotData;
import com.kaixeleron.horde.mission.data.Mission;
import com.kaixeleron.horde.mission.data.Subwave;
import com.kaixeleron.horde.mission.data.Wave;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class MissionConfigurator {

    private final HordeMain main;

    private final MissionManager missionManager;

    private final Map<Player, Mission> configuring;
    private final Map<Player, Wave> configuringWave;
    private final Map<Player, Subwave> configuringSubwave;
    private final Map<Player, Integer> selectingBot, configuringBot;

    private final List<Player> swapping, settingName, settingHealth, settingSpeed;

    private final ItemStack wave, newWave, subwave, newSubwave, newBot, pageBack, cancel, pageForward,
            decreaseQuantity, increaseQuantity, setName, setHealth, setSpeed, setArmor, setWeapon;

    private final Inventory[] botTypeSelectors;

    @SuppressWarnings("ConstantConditions")
    public MissionConfigurator(HordeMain main, MissionManager missionManager) {

        this.main = main;
        this.missionManager = missionManager;

        configuring = new HashMap<>();
        configuringWave = new HashMap<>();
        configuringSubwave = new HashMap<>();
        selectingBot = new HashMap<>();
        configuringBot = new HashMap<>();

        swapping = new ArrayList<>();
        settingName = Collections.synchronizedList(new ArrayList<>());
        settingHealth = Collections.synchronizedList(new ArrayList<>());
        settingSpeed = Collections.synchronizedList(new ArrayList<>());

        wave = new ItemStack(Material.BOOK);
        ItemMeta waveMeta = wave.getItemMeta();
        waveMeta.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Left click to configure.", ChatColor.DARK_GRAY + "Right click to delete."));
        wave.setItemMeta(waveMeta);

        newWave = new ItemStack(Material.BOOK);
        ItemMeta newWaveMeta = newWave.getItemMeta();
        newWaveMeta.setDisplayName(ChatColor.YELLOW + "Add new wave");
        newWaveMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        newWave.setItemMeta(newWaveMeta);
        newWave.addUnsafeEnchantment(Enchantment.DURABILITY, 1);

        subwave = new ItemStack(Material.PAPER);
        ItemMeta subwaveMeta = subwave.getItemMeta();
        subwaveMeta.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Left click to configure.", ChatColor.DARK_GRAY + "Right click to delete."));
        subwave.setItemMeta(subwaveMeta);

        newSubwave = new ItemStack(Material.MAP);
        ItemMeta newSubwaveMeta = newSubwave.getItemMeta();
        newSubwaveMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        newSubwaveMeta.setDisplayName(ChatColor.YELLOW + "Create new subwave");
        newSubwave.setItemMeta(newSubwaveMeta);
        newSubwave.addUnsafeEnchantment(Enchantment.DURABILITY, 1);

        newBot = new ItemStack(Material.EMERALD);
        ItemMeta newBotMeta = newBot.getItemMeta();
        newBotMeta.setDisplayName(ChatColor.YELLOW + "Add new bot");
        newBotMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        newBot.setItemMeta(newBotMeta);
        newBot.addUnsafeEnchantment(Enchantment.DURABILITY, 1);

        pageBack = new ItemStack(Material.RED_WOOL);
        ItemMeta pageBackMeta = pageBack.getItemMeta();
        pageBackMeta.setDisplayName(ChatColor.RESET + "Previous page");
        pageBack.setItemMeta(pageBackMeta);

        cancel = new ItemStack(Material.BARRIER);
        ItemMeta cancelMeta = cancel.getItemMeta();
        cancelMeta.setDisplayName(ChatColor.RED + "Cancel");
        cancel.setItemMeta(cancelMeta);

        pageForward = new ItemStack(Material.LIME_WOOL);
        ItemMeta pageForwardMeta = pageForward.getItemMeta();
        pageForwardMeta.setDisplayName(ChatColor.RESET + "Next page");
        pageForward.setItemMeta(pageForwardMeta);

        decreaseQuantity = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        ItemMeta decreaseQuantityMeta = decreaseQuantity.getItemMeta();
        decreaseQuantityMeta.setDisplayName(ChatColor.RED + "Decrease quantity");
        decreaseQuantity.setItemMeta(decreaseQuantityMeta);

        increaseQuantity = new ItemStack(Material.LIME_STAINED_GLASS_PANE);
        ItemMeta increaseQuantityMeta = increaseQuantity.getItemMeta();
        increaseQuantityMeta.setDisplayName(ChatColor.GREEN + "Increase quantity");
        increaseQuantity.setItemMeta(increaseQuantityMeta);

        setName = new ItemStack(Material.NAME_TAG);
        ItemMeta setNameMeta = setName.getItemMeta();
        setNameMeta.setDisplayName(ChatColor.RESET + "Set name");
        setName.setItemMeta(setNameMeta);

        setHealth = new ItemStack(Material.RED_DYE);
        ItemMeta setHealthMeta = setHealth.getItemMeta();
        setHealthMeta.setDisplayName(ChatColor.RESET + "Set health");
        setHealth.setItemMeta(setHealthMeta);

        setSpeed = new ItemStack(Material.FEATHER);
        ItemMeta setSpeedMeta = setSpeed.getItemMeta();
        setSpeedMeta.setDisplayName(ChatColor.RESET + "Set speed");
        setSpeed.setItemMeta(setSpeedMeta);

        setArmor = new ItemStack(Material.DIAMOND_HELMET);
        ItemMeta setArmorMeta = setArmor.getItemMeta();
        setArmorMeta.setDisplayName(ChatColor.RESET + "Set armor");
        setArmorMeta.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Click to set armor to", ChatColor.DARK_GRAY + "your current armor."));
        setArmorMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        setArmor.setItemMeta(setArmorMeta);

        setWeapon = new ItemStack(Material.GOLDEN_SWORD);
        ItemMeta setWeaponMeta = setWeapon.getItemMeta();
        setWeaponMeta.setDisplayName(ChatColor.RESET + "Set weapon");
        setWeaponMeta.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Click to set weapon to", ChatColor.DARK_GRAY + "the item in your main hand."));
        setWeaponMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        setWeapon.setItemMeta(setWeaponMeta);

        HordeEntityType[] botTypes = HordeEntityType.values();

        int typePageCount = botTypes.length / 52 + (botTypes.length % 52 == 0 ? 0 : 1);

        botTypeSelectors = new Inventory[typePageCount];

        for (int i = 0; i < typePageCount; i++) {

            botTypeSelectors[i] = Bukkit.createInventory(null, 54, "Bot type selection");

            if (i != 0) {

                botTypeSelectors[i].setItem(45, pageBack);

            }

            botTypeSelectors[i].setItem(49, cancel);

            if (i != typePageCount - 1) {

                botTypeSelectors[i].setItem(53, pageForward);

            }

            int limit = (i + 1) * 45;

            if (limit > botTypes.length) {

                limit = botTypes.length;

            }

            for (int type = i * 45; type < limit; type++) {

                ItemStack item = new ItemStack(botTypes[type].getMenuMaterial());
                ItemMeta itemMeta = item.getItemMeta();
                itemMeta.setDisplayName(ChatColor.RESET + botTypes[type].getFriendlyName());
                item.setItemMeta(itemMeta);

                botTypeSelectors[i].addItem(item);

            }

        }

    }

    public void startConfiguring(Player player, Mission mission) {

        configuring.put(player, mission);

        displayWaveList(player, mission);

    }

    public void stopConfiguring(Player player) {

        if (missionManager.saveMission(configuring.get(player))) {

            player.sendMessage("Mission saved.");

        } else {

            player.sendMessage(ChatColor.RED + "Could not save the mission.");

        }

        configuring.remove(player);
        configuringWave.remove(player);
        configuringSubwave.remove(player);
        selectingBot.remove(player);
        configuringBot.remove(player);
        swapping.remove(player);

        synchronized (settingName) {

            settingName.remove(player);

        }

        synchronized (settingHealth) {

            settingHealth.remove(player);

        }

        synchronized (settingSpeed) {

            settingSpeed.remove(player);

        }

    }

    public boolean isConfiguring(Player player) {

        return configuring.containsKey(player);

    }

    public void clearConfiguring() {

        for (Player player : configuring.keySet()) {

            player.closeInventory();

        }

        configuring.clear();
        configuringWave.clear();

    }

    public boolean isSwapping(Player player) {

        return swapping.contains(player);

    }

    @SuppressWarnings("ConstantConditions")
    public void fireLeftClick(Player player, ItemStack clickedItem, int position) {

        if (clickedItem != null) {

            if (clickedItem.equals(newWave)) {

                Mission mission = configuring.get(player);
                mission.addWave(new Wave());
                configuring.put(player, mission);

                swapping.add(player);
                player.closeInventory();
                swapping.remove(player);

                displayWaveList(player, mission);

            } else if (clickedItem.equals(newSubwave)) {

                if (configuringWave.containsKey(player)) {

                    Wave wave = configuringWave.get(player);
                    wave.addSubwave(new Subwave());
                    configuringWave.put(player, wave);

                    swapping.add(player);
                    player.closeInventory();
                    swapping.remove(player);

                    displaySubwaveList(player, wave);

                }

            } else {

                if (configuringWave.containsKey(player)) {

                    if (configuringSubwave.containsKey(player)) {

                        if (selectingBot.containsKey(player)) {

                            if (clickedItem.equals(pageForward)) {

                                swapping.add(player);
                                player.closeInventory();
                                swapping.remove(player);

                                int page = selectingBot.get(player) + 1;

                                selectingBot.put(player, page);
                                player.openInventory(botTypeSelectors[page]);

                            } else if (clickedItem.equals(pageBack)) {

                                swapping.add(player);
                                player.closeInventory();
                                swapping.remove(player);

                                int page = selectingBot.get(player) - 1;

                                selectingBot.put(player, page);
                                player.openInventory(botTypeSelectors[page]);

                            } else if (clickedItem.equals(cancel)) {

                                swapping.add(player);
                                player.closeInventory();
                                swapping.remove(player);

                                selectingBot.remove(player);

                                displaySubwave(player, configuringSubwave.get(player));

                            } else {

                                HordeEntityType type = HordeEntityType.getByMenuMaterial(clickedItem.getType());

                                if (type != null) {

                                    configuringSubwave.get(player).addBot(new HordeBotData(type, 1, null, -1.0D, -1.0D));

                                    swapping.add(player);
                                    player.closeInventory();
                                    swapping.remove(player);

                                    selectingBot.remove(player);

                                    displaySubwave(player, configuringSubwave.get(player));

                                }

                            }

                        } else {

                            if (clickedItem.equals(newBot)) {

                                swapping.add(player);
                                player.closeInventory();
                                swapping.remove(player);

                                selectingBot.put(player, 0);
                                player.openInventory(botTypeSelectors[0]);

                            } else if (clickedItem.equals(cancel) && !configuringBot.containsKey(player)) {

                                swapping.add(player);
                                player.closeInventory();
                                swapping.remove(player);

                                configuringSubwave.remove(player);

                                displaySubwaveList(player, configuringWave.get(player));

                            } else {

                                if (configuringBot.containsKey(player)) {

                                    if (clickedItem.equals(decreaseQuantity)) {

                                        Subwave subwave = configuringSubwave.get(player);

                                        HordeBotData data = configuringSubwave.get(player).listBots()[configuringBot.get(player)];

                                        if (data.getQuantity() > 1) {

                                            data.setQuantity(data.getQuantity() - 1);
                                            subwave.setBot(configuringBot.get(player), data);

                                        }

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        displayBotConfigurator(player);

                                    } else if (clickedItem.equals(increaseQuantity)) {

                                        Subwave subwave = configuringSubwave.get(player);

                                        HordeBotData data = configuringSubwave.get(player).listBots()[configuringBot.get(player)];

                                        if (data.getQuantity() < 64) {

                                            data.setQuantity(data.getQuantity() + 1);
                                            subwave.setBot(configuringBot.get(player), data);

                                        }

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        displayBotConfigurator(player);

                                    } else if (clickedItem.equals(setName)) {

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        synchronized (settingName) {

                                            settingName.add(player);

                                        }

                                        player.sendMessage("Type the new bot name in chat.");

                                    } else if (clickedItem.equals(setHealth)) {

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        synchronized (settingHealth) {

                                            settingHealth.add(player);

                                        }

                                        player.sendMessage("Type the new bot health in chat.");

                                    } else if (clickedItem.equals(setSpeed)) {

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        synchronized (settingSpeed) {

                                            settingSpeed.add(player);

                                        }

                                        player.sendMessage("Type the new bot speed in chat.");

                                    } else if (clickedItem.equals(setArmor)) {

                                        Subwave subwave = configuringSubwave.get(player);
                                        int botPosition = configuringBot.get(player);

                                        HordeBotData data = subwave.listBots()[botPosition];

                                        PlayerInventory inventory = player.getInventory();

                                        data.setHelmet(inventory.getHelmet());
                                        data.setChestplate(inventory.getChestplate());
                                        data.setLeggings(inventory.getLeggings());
                                        data.setBoots(inventory.getBoots());

                                        subwave.setBot(botPosition, data);

                                        player.sendMessage("Armor set to your current armor.");

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        displayBotConfigurator(player);

                                    } else if (clickedItem.equals(setWeapon)) {

                                        Subwave subwave = configuringSubwave.get(player);
                                        int botPosition = configuringBot.get(player);

                                        HordeBotData data = subwave.listBots()[botPosition];

                                        data.setWeapon(player.getInventory().getItemInMainHand());

                                        subwave.setBot(botPosition, data);

                                        player.sendMessage("Weapon set to the item in your main hand.");

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        displayBotConfigurator(player);

                                    } else if (clickedItem.equals(cancel)) {

                                        swapping.add(player);
                                        player.closeInventory();
                                        swapping.remove(player);

                                        configuringBot.remove(player);

                                        displaySubwave(player, configuringSubwave.get(player));

                                    }

                                } else {

                                    swapping.add(player);
                                    player.closeInventory();
                                    swapping.remove(player);

                                    configuringBot.put(player, position);

                                    displayBotConfigurator(player);

                                }

                            }

                        }

                    } else {

                        if (clickedItem.equals(cancel)) {

                            configuringWave.remove(player);

                            swapping.add(player);
                            player.closeInventory();
                            swapping.remove(player);

                            displayWaveList(player, configuring.get(player));

                        } else {

                            String name = clickedItem.getItemMeta().getDisplayName();

                            int subwaveID = Integer.parseInt(name.split(" ")[1]) - 1;
                            Subwave subwave = configuringWave.get(player).getSubwave(subwaveID);

                            configuringSubwave.put(player, subwave);

                            swapping.add(player);
                            player.closeInventory();
                            swapping.remove(player);

                            displaySubwave(player, subwave);

                        }

                    }

                } else {

                    String name = clickedItem.getItemMeta().getDisplayName();

                    int waveID = Integer.parseInt(name.substring(name.length() - 1)) - 1;
                    Wave wave = configuring.get(player).getWave(waveID);

                    configuringWave.put(player, wave);

                    swapping.add(player);
                    player.closeInventory();
                    swapping.remove(player);

                    displaySubwaveList(player, wave);

                }

            }

        }

    }

    @SuppressWarnings("ConstantConditions")
    public void fireRightClick(Player player, ItemStack clickedItem, int position) {

        if (clickedItem != null) {

            if (!clickedItem.equals(newWave) && !clickedItem.equals(newSubwave) && !clickedItem.equals(newBot) && !clickedItem.equals(cancel) && !configuringBot.containsKey(player) && !selectingBot.containsKey(player)) {

                if (configuringSubwave.containsKey(player)) {

                    Subwave subwave = configuringSubwave.get(player);

                    subwave.removeBot(position);

                    swapping.add(player);
                    player.closeInventory();
                    swapping.remove(player);

                    displaySubwave(player, subwave);

                } else if (configuringWave.containsKey(player)) {

                    String name = clickedItem.getItemMeta().getDisplayName();

                    int subwaveID = Integer.parseInt(name.substring(name.length() - 1)) - 1;
                    Subwave subwave = configuringWave.get(player).getSubwave(subwaveID);

                    configuringWave.get(player).removeSubwave(subwave);

                    swapping.add(player);
                    player.closeInventory();
                    swapping.remove(player);

                    displaySubwaveList(player, configuringWave.get(player));

                } else if (configuring.containsKey(player)) {

                    String name = clickedItem.getItemMeta().getDisplayName();

                    int waveID = Integer.parseInt(name.substring(name.length() - 1)) - 1;
                    Wave wave = configuring.get(player).getWave(waveID);

                    configuring.get(player).removeWave(wave);

                    swapping.add(player);
                    player.closeInventory();
                    swapping.remove(player);

                    displayWaveList(player, configuring.get(player));

                }

            }

        }

    }

    public boolean fireChat(final Player player, final String message) {

        boolean cancel = false;

        synchronized (settingName) {

            if (settingName.contains(player)) {

                Bukkit.getScheduler().callSyncMethod(main, () -> {

                    Subwave subwave = configuringSubwave.get(player);

                    int position = configuringBot.get(player);

                    HordeBotData data = subwave.listBots()[position];
                    data.setName(message);

                    subwave.setBot(position, data);

                    displayBotConfigurator(player);

                    player.sendMessage("Name set.");

                    return null;

                });

                settingName.remove(player);

                cancel = true;

            }

        }

        synchronized (settingHealth) {

            if (settingHealth.contains(player)) {

                double health = -1.0D;

                try {

                    health = Double.parseDouble(message);

                } catch (NumberFormatException ignored) {}

                final double finalHealth = health;

                Bukkit.getScheduler().callSyncMethod(main, () -> {

                    if (finalHealth <= 0.0D) {

                        player.sendMessage(ChatColor.RED + "Invalid health. Must be a positive decimal.");

                    } else {

                        Subwave subwave = configuringSubwave.get(player);

                        int position = configuringBot.get(player);

                        HordeBotData data = subwave.listBots()[position];
                        data.setHealth(finalHealth);

                        subwave.setBot(position, data);

                        displayBotConfigurator(player);

                        player.sendMessage("Health set.");

                    }

                    return null;

                });

                if (health > 0.0D) {

                    settingHealth.remove(player);

                }

                cancel = true;

            }

        }

        synchronized (settingSpeed) {

            if (settingSpeed.contains(player)) {

                double speed = -1.0D;

                try {

                    speed = Double.parseDouble(message);

                } catch (NumberFormatException ignored) {}

                final double finalSpeed = speed;

                Bukkit.getScheduler().callSyncMethod(main, () -> {

                    if (finalSpeed <= 0.0D) {

                        player.sendMessage(ChatColor.RED + "Invalid speed. Must be a positive decimal.");

                    } else {

                        Subwave subwave = configuringSubwave.get(player);

                        int position = configuringBot.get(player);

                        HordeBotData data = subwave.listBots()[position];
                        data.setSpeed(finalSpeed);

                        subwave.setBot(position, data);

                        displayBotConfigurator(player);

                        player.sendMessage("Speed set.");

                    }

                    return null;

                });

                if (speed > 0.0D) {

                    settingHealth.remove(player);

                }

                cancel = true;

            }

        }

        return cancel;

    }

    @SuppressWarnings("ConstantConditions")
    private void displayWaveList(Player player, Mission mission) {

        Wave[] waves = mission.listWaves();

        Inventory inventory = Bukkit.createInventory(null, (waves.length / 9) * 9 + 9, String.format("Mission %s", mission.getName()));

        for (int i = 0; i < waves.length; i++) {

            ItemStack waveStack = this.wave.clone();
            ItemMeta waveMeta = waveStack.getItemMeta();
            waveMeta.setDisplayName(ChatColor.RESET + String.format("Wave %d", i + 1));
            waveStack.setItemMeta(waveMeta);

            inventory.addItem(waveStack);

        }

        inventory.addItem(newWave.clone());

        player.openInventory(inventory);

    }

    @SuppressWarnings("ConstantConditions")
    private void displaySubwaveList(Player player, Wave wave) {

        Subwave[] subwaves = wave.listSubwaves();

        Mission mission = configuring.get(player);

        int size = (subwaves.length / 9) * 9 + 18;

        if (size > 54) size = 54;

        Inventory inventory = Bukkit.createInventory(null, size, String.format("%s wave %d", mission.getName(), mission.getWaveId(wave) + 1));

        for (int i = 0; i < subwaves.length; i++) {

            ItemStack waveStack = this.subwave.clone();
            ItemMeta waveMeta = waveStack.getItemMeta();
            waveMeta.setDisplayName(ChatColor.RESET + String.format("Subwave %d", i + 1));
            waveStack.setItemMeta(waveMeta);

            inventory.addItem(waveStack);

        }

        inventory.addItem(newSubwave.clone());
        inventory.setItem(size - 1, cancel);

        player.openInventory(inventory);

    }

    @SuppressWarnings("ConstantConditions")
    private void displaySubwave(Player player, Subwave subwave) {

        HordeBotData[] bots = subwave.listBots();

        Mission mission = configuring.get(player);
        Wave wave = configuringWave.get(player);

        int size = (bots.length / 9) * 9 + 18;

        if (size > 54) size = 54;

        Inventory inventory = Bukkit.createInventory(null, size, String.format("%s wave %d subwave %d", mission.getName(), mission.getWaveId(wave) + 1, wave.getSubwaveId(subwave) + 1));

        for (HordeBotData bot : bots) {

            ItemStack botStack = new ItemStack(bot.getEntityType().getMenuMaterial());
            ItemMeta botMeta = botStack.getItemMeta();
            botMeta.setDisplayName(ChatColor.RESET + bot.getEntityType().getFriendlyName());
            botMeta.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Left click to configure.", ChatColor.DARK_GRAY + "Right click to delete."));
            botStack.setItemMeta(botMeta);
            botStack.setAmount(bot.getQuantity());

            inventory.addItem(botStack);

        }

        inventory.addItem(newBot.clone());
        inventory.setItem(size - 1, cancel);

        player.openInventory(inventory);

    }

    @SuppressWarnings("ConstantConditions")
    private void displayBotConfigurator(Player player) {

        Subwave subwave = configuringSubwave.get(player);
        int position = configuringBot.get(player);

        HordeBotData data = subwave.listBots()[position];

        Inventory inventory = Bukkit.createInventory(null, 9, String.format("Configuring bot %s", data.getName() == null ? data.getEntityType().getFriendlyName() : data.getName()));

        ItemStack currentQuantity = new ItemStack(data.getEntityType().getMenuMaterial());
        ItemMeta currentQuantityMeta = currentQuantity.getItemMeta();
        currentQuantityMeta.setDisplayName(ChatColor.RESET + "Current quantity: " + data.getQuantity());
        currentQuantity.setItemMeta(currentQuantityMeta);
        currentQuantity.setAmount(data.getQuantity());

        if (data.getQuantity() > 1) {

            inventory.setItem(0, decreaseQuantity);

        }

        inventory.setItem(1, currentQuantity);

        if (data.getQuantity() < 64) {

            inventory.setItem(2, increaseQuantity);

        }

        inventory.setItem(3, setName);
        inventory.setItem(4, setHealth);
        inventory.setItem(5, setSpeed);
        inventory.setItem(6, setArmor);
        inventory.setItem(7, setWeapon);
        inventory.setItem(8, cancel);

        player.openInventory(inventory);

    }

}
