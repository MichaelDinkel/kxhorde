package com.kaixeleron.horde.mission.command.tabcomplete;

import com.kaixeleron.horde.mission.MissionManager;
import com.kaixeleron.horde.mission.data.Mission;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TabCompleteMission implements TabCompleter {

    private final String[] arg1Suggestions, difficulties;

    private final MissionManager missionManager;

    public TabCompleteMission(MissionManager missionManager) {

        arg1Suggestions = new String[]{"create", "delete", "setdifficulty", "settime", "list", "showmission", "configmission"};
        difficulties = new String[]{"easy", "medium", "hard", "expert", "nightmare", "impossible"};

        this.missionManager = missionManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

        List<String> suggestions = new ArrayList<>();

        switch (args.length) {

            case 1:

                if (args[0].length() == 0) {

                    suggestions.addAll(Arrays.asList(arg1Suggestions));

                } else {

                    for (String suggestion : arg1Suggestions) {

                        if (suggestion.startsWith(args[0].toLowerCase())) {

                            suggestions.add(suggestion);

                        }

                    }

                }

                break;

            case 2:

                switch (args[0].toLowerCase()) {

                    case "create":

                        if (args[1].length() == 0) {

                            suggestions.add("name");

                        } else {

                            suggestions.add(args[1]);

                        }

                        break;

                    case "delete":
                    case "setdifficulty":
                    case "settime":
                    case "showmission":
                    case "configmission":

                        for (Mission mission : missionManager.listMissions()) {

                            if (args[1].length() == 0 || mission.getName().startsWith(args[1])) {

                                suggestions.add(mission.getName());

                            }

                        }

                        break;

                }

                break;

            case 3:

                switch (args[0].toLowerCase()) {

                    case "setdifficulty":

                        if (args[2].length() == 0) {

                            suggestions.addAll(Arrays.asList(difficulties));

                        } else {

                            for (String difficulty : difficulties) {

                                if (difficulty.startsWith(args[2].toLowerCase())) {

                                    suggestions.add(difficulty);

                                }

                            }

                        }

                        break;

                    case "settime":

                        if (args[2].length() == 0) {

                            suggestions.add("time");

                        } else {

                            suggestions.add(args[2]);

                        }

                        break;

                }

                break;

        }

        return suggestions;

    }

}
