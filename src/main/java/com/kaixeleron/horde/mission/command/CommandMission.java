package com.kaixeleron.horde.mission.command;

import com.kaixeleron.horde.chat.ChatManager;
import com.kaixeleron.horde.mission.MissionManager;
import com.kaixeleron.horde.mission.data.Mission;
import com.kaixeleron.horde.mission.data.MissionDifficulty;
import com.kaixeleron.horde.mission.util.MissionConfigurator;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;

public class CommandMission implements CommandExecutor {

    private final ChatManager chatManager;
    private final MissionManager missionManager;

    private final MissionConfigurator missionConfigurator;

    public CommandMission(ChatManager chatManager, MissionManager missionManager, MissionConfigurator missionConfigurator) {

        this.chatManager = chatManager;
        this.missionManager = missionManager;
        this.missionConfigurator = missionConfigurator;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            help(sender, label);

        } else {

            switch (args[0].toLowerCase()) {
                case "create" -> create(sender, label, args.length > 1 ? args[1] : null);
                case "delete" -> delete(sender, label, args.length > 1 ? args[1] : null);
                case "setdifficulty" -> setDifficulty(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "settime" -> setTime(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "list" -> list(sender);
                case "showmission" -> showMission(sender, label, args.length > 1 ? args[1] : null);
                case "configmission" -> configMission(sender, label, args.length > 1 ? args[1] : null);
                default -> help(sender, label);
            }
            
        }

        return true;

    }

    private void help(CommandSender sender, String label) {

        sender.sendMessage("kxHorde mission management:");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " create <name> " + ChatColor.RESET + "- Create a new mission.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " delete <name> " + ChatColor.RESET + "- Delete a mission.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " setdifficulty <mission> <level> " + ChatColor.RESET + "- Set mission difficulty.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " settime <mission> <time> " + ChatColor.RESET + "- Set mission time.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " list " + ChatColor.RESET + "- List missions.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " showmission <mission> " + ChatColor.RESET + "- Display mission information.");

        if (sender instanceof Player) {

            sender.sendMessage(ChatColor.GRAY + "/" + label + " configmission <mission> " + ChatColor.RESET + "- Configure a mission.");

        }

    }

    private void create(CommandSender sender, String label, @Nullable String name) {

        if (name == null) {

            sender.sendMessage("Create a new mission.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " create <name>");

        } else {

            if (missionManager.missionExists(name)) {

                sender.sendMessage(ChatColor.RED + "The mission " + ChatColor.RESET + name + ChatColor.RED + " already exists.");

            } else {

                Mission mission = new Mission(name);

                if (missionManager.saveMission(mission)) {

                    missionManager.addMission(mission);
                    sender.sendMessage("Mission " + ChatColor.GRAY + name + ChatColor.RESET + " created.");

                } else {

                    sender.sendMessage(ChatColor.RED + "The mission could not be saved.");

                }

            }

        }

    }

    private void delete(CommandSender sender, String label, @Nullable String name) {

        if (name == null) {

            sender.sendMessage("Delete a mission.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " delete <name>");

        } else {

            Mission mission = missionManager.getMission(name);

            if (mission == null) {

                sender.sendMessage(ChatColor.RED + "The mission " + ChatColor.RESET + name + ChatColor.RED + " does not exist.");

            } else {

                if (missionManager.deleteMission(mission)) {

                    missionManager.removeMission(mission);
                    sender.sendMessage("Mission " + ChatColor.GRAY + name + ChatColor.RESET + " deleted.");

                } else {

                    sender.sendMessage(ChatColor.RED + "The mission could not be deleted.");

                }

            }

        }

    }

    private void setDifficulty(CommandSender sender, String label, @Nullable String missionName, @Nullable String difficultyLevel) {

        if (missionName == null || difficultyLevel == null) {

            sender.sendMessage("Set mission difficulty.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " setdifficulty <mission> <level>");

        } else {

            Mission mission = missionManager.getMission(missionName);

            if (mission == null) {

                sender.sendMessage(ChatColor.RED + "The mission " + ChatColor.RESET + missionName + ChatColor.RED + " does not exist.");

            } else {

                MissionDifficulty difficulty = switch (difficultyLevel.toLowerCase()) {
                    case "easy", "0" -> MissionDifficulty.EASY;
                    case "medium", "1" -> MissionDifficulty.MEDIUM;
                    case "hard", "2" -> MissionDifficulty.HARD;
                    case "expert", "3" -> MissionDifficulty.EXPERT;
                    case "nightmare", "4" -> MissionDifficulty.NIGHTMARE;
                    case "impossible", "5" -> MissionDifficulty.IMPOSSIBLE;
                    default -> null;
                };

                if (difficulty == null) {

                    sender.sendMessage(ChatColor.RED + "Invalid difficulty. Must be " + ChatColor.RESET + "easy"
                            + ChatColor.RED + ", " + ChatColor.RESET + "medium" + ChatColor.RED + ", " + ChatColor.RESET
                            + "hard" + ChatColor.RED + ", " + ChatColor.RESET + "expert" + ChatColor.RED + ", "
                            + ChatColor.RESET + "nightmare" + ChatColor.RED + ", or " + ChatColor.RESET + "impossible"
                            + ChatColor.RED + ".");

                } else {

                    mission.setDifficulty(difficulty);
                    missionManager.saveMission(mission);

                    sender.sendMessage("Difficulty on mission " + ChatColor.GRAY + missionName + ChatColor.RESET + " set to "
                            + chatManager.getDifficultyChatColor(difficulty) + difficulty.getFriendlyName().toLowerCase() + ChatColor.RESET + ".");

                }

            }

        }

    }

    private void setTime(CommandSender sender, String label, @Nullable String missionName, @Nullable String timeString) {

        if (missionName == null || timeString == null) {

            sender.sendMessage("Set mission time.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " settime <mission> <time>");

        } else {

            Mission mission = missionManager.getMission(missionName);

            if (mission == null) {

                sender.sendMessage(ChatColor.RED + "The mission " + ChatColor.RESET + missionName + ChatColor.RED + " does not exist.");

            } else {

                long time = -1L;

                try {

                    time = Long.parseLong(timeString);

                } catch (NumberFormatException ignored) {}

                if (time < 0L) {

                    sender.sendMessage(ChatColor.RED + "Invalid time. Must be a positive integer.");

                } else {

                    mission.setTime(time);
                    missionManager.saveMission(mission);

                    sender.sendMessage("Time on mission " + ChatColor.GRAY + missionName + ChatColor.RESET + " set to " + ChatColor.RESET + time + ChatColor.RESET + ".");

                }

            }

        }

    }

    private void list(CommandSender sender) {

        Mission[] missions = missionManager.listMissions();

        sender.sendMessage("There " + (missions.length == 1 ? "is " : "are ") + missions.length + " mission" + (missions.length == 1 ? "" : "s") + " loaded" + (missions.length == 0 ? "." : ":"));

        for (Mission mission : missions) {

            sender.sendMessage(ChatColor.GRAY + " - " + ChatColor.RESET + mission.getName());

        }

    }

    private void showMission(CommandSender sender, String label, @Nullable String missionName) {

        if (missionName == null) {

            sender.sendMessage("Display mission information.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " showmission <mission>");

        } else {

            Mission mission = missionManager.getMission(missionName);

            if (mission == null) {

                sender.sendMessage(ChatColor.RED + "The mission " + ChatColor.RESET + missionName + ChatColor.RED + " does not exist.");

            } else {

                sender.sendMessage("Mission " + ChatColor.GRAY + missionName + ChatColor.RESET + ":");
                sender.sendMessage("Mission difficulty: " + chatManager.getDifficultyChatColor(mission.getDifficulty()) + mission.getDifficulty().getFriendlyName().toLowerCase());
                sender.sendMessage("Mission time: " + ChatColor.GRAY + mission.getTime());
                sender.sendMessage("Waves: " + ChatColor.GRAY + mission.listWaves().length);

            }

        }

    }

    private void configMission(CommandSender sender, String label, @Nullable String missionName) {

        if (sender instanceof Player player) {

            if (missionName == null) {

                sender.sendMessage("Configure a mission.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " configmission <mission>");

            } else {

                Mission mission = missionManager.getMission(missionName);

                if (mission == null) {

                    sender.sendMessage(ChatColor.RED + "The mission " + ChatColor.RESET + missionName + ChatColor.RED + " does not exist.");

                } else {

                        missionConfigurator.startConfiguring(player, mission);
                        sender.sendMessage("Configuring mission " + ChatColor.GRAY + missionName + ChatColor.RESET + ".");

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

}
