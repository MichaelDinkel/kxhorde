package com.kaixeleron.horde.mission.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Subwave implements Serializable {

    private final List<HordeBotData> bots;

    public Subwave() {

        bots = new ArrayList<>();

    }

    public Subwave(HordeBotData[] bots) {

        this.bots = new ArrayList<>(Arrays.asList(bots));

    }

    public void addBot(HordeBotData botData) {

        boolean found = false;
        int i = 0;

        while (!found && 1 < bots.size()) {

            if (bots.get(i).getEntityType().equals(botData.getEntityType())) {

                bots.get(i).setQuantity(bots.get(i).getQuantity() + botData.getQuantity());
                found = true;

            }

            i++;

        }

        if (!found) {

            bots.add(botData);

        }

    }

    public void removeBot(int position) {

        bots.remove(position);

    }

    public void setBot(int position, HordeBotData data) {

        bots.set(position, data);

    }

    public HordeBotData[] listBots() {

        return bots.toArray(new HordeBotData[0]);

    }

    public void clearBots() {

        bots.clear();

    }

}
