package com.kaixeleron.horde.mission.data;

import com.kaixeleron.horde.entity.data.HordeEntityType;
import org.bukkit.inventory.ItemStack;

import java.io.Serializable;

public class HordeBotData implements Serializable {

    private final HordeEntityType entityType;

    private int quantity;

    private double health, speed;

    private String name;

    private ItemStack helmet = null, chestplate = null, leggings = null, boots = null, weapon = null;

    public HordeBotData(HordeEntityType entityType, int quantity, String name, double health, double speed) {

        this.entityType = entityType;

        this.quantity = quantity;

        this.name = name;
        this.health = health;
        this.speed = speed;

    }

    public HordeEntityType getEntityType() {

        return entityType;

    }

    public int getQuantity() {

        return quantity;

    }

    public void setQuantity(int quantity) {

        this.quantity = quantity;

    }

    public double getHealth() {

        return health;

    }

    public void setHealth(double health) {

        this.health = health;

    }

    public double getSpeed() {

        return speed;

    }

    public void setSpeed(double speed) {

        this.speed = speed;

    }

    public String getName() {

        return name;

    }

    public void setName(String name) {

        this.name = name;

    }

    public void setHelmet(ItemStack helmet) {

        if (helmet != null) {

            helmet.setAmount(1);

        }

        this.helmet = helmet;

    }

    public ItemStack getHelmet() {

        return helmet;

    }

    public void setChestplate(ItemStack chestplate) {

        if (chestplate != null) {

            chestplate.setAmount(1);

        }

        this.chestplate = chestplate;

    }

    public ItemStack getChestplate() {

        return chestplate;

    }

    public void setLeggings(ItemStack leggings) {

        if (leggings != null) {

            leggings.setAmount(1);

        }

        this.leggings = leggings;

    }

    public ItemStack getLeggings() {

        return leggings;

    }

    public void setBoots(ItemStack boots) {

        if (boots != null) {

            boots.setAmount(1);

        }

        this.boots = boots;

    }

    public ItemStack getBoots() {

        return boots;

    }

    public void setWeapon(ItemStack weapon) {

        weapon.setAmount(1);

        this.weapon = weapon;

    }

    public ItemStack getWeapon() {

        return weapon;

    }

}
