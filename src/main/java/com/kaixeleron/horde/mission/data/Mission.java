package com.kaixeleron.horde.mission.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mission implements Serializable {

    private final String name;

    private long time;
    private MissionDifficulty difficulty;

    private final List<Wave> waves;

    public Mission(String name) {

        this.name = name;

        time = 6000L;
        difficulty = MissionDifficulty.UNSET;

        waves = new ArrayList<>();

    }

    public Mission(String name, long time, MissionDifficulty difficulty, Wave[] waves) {

        this.name = name;
        this.time = time;
        this.difficulty = difficulty;
        this.waves = new ArrayList<>(Arrays.asList(waves));

    }

    public String getName() {

        return name;

    }

    public long getTime() {

        return time;

    }

    public void setTime(long time) {

        this.time = time;

    }

    public MissionDifficulty getDifficulty() {

        return difficulty;

    }

    public void setDifficulty(MissionDifficulty difficulty) {

        this.difficulty = difficulty;

    }

    public void addWave(Wave wave) {

        waves.add(wave);

    }

    public void removeWave(Wave wave) {

        waves.remove(wave);

    }

    public void setWave(int position, Wave wave) {

        waves.set(position, wave);

    }

    public Wave getWave(int position) {

        return waves.get(position);

    }

    public Wave[] listWaves() {

        return waves.toArray(new Wave[0]);

    }

    public int getWaveId(Wave wave) {

        int id = -1, i = 0;

        while (id == -1 && i < waves.size()) {

            if (waves.get(i).equals(wave)) {

                id = i;

            }

            i++;

        }

        return id;

    }

    public void clearWaves() {

        waves.clear();

    }

}
