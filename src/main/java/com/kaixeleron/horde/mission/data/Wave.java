package com.kaixeleron.horde.mission.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wave implements Serializable {

    private final List<Subwave> subwaves;

    public Wave() {

        subwaves = new ArrayList<>();

    }

    public Wave(Subwave[] subwaves) {

        this.subwaves = new ArrayList<>(Arrays.asList(subwaves));

    }

    public void addSubwave(Subwave subwave) {

        subwaves.add(subwave);

    }

    public void removeSubwave(Subwave subwave) {

        subwaves.remove(subwave);

    }

    public void setSubwave(int position, Subwave wave) {

        subwaves.set(position, wave);

    }

    public Subwave getSubwave(int position) {

        return subwaves.get(position);

    }

    public Subwave[] listSubwaves() {

        return subwaves.toArray(new Subwave[0]);

    }

    public int getSubwaveId(Subwave subwave) {

        int id = -1, i = 0;

        while (id == -1 && i < subwaves.size()) {

            if (subwaves.get(i).equals(subwave)) {

                id = i;

            }

            i++;

        }

        return id;

    }

    public void clearSubwaves() {

        for (Subwave subwave : subwaves) {

            subwave.clearBots();

        }

        subwaves.clear();

    }

}
