package com.kaixeleron.horde.mission.data;

import java.io.Serializable;

public enum MissionDifficulty implements Serializable {

    UNSET(-1, "Unset"),
    EASY(0, "Easy"),
    MEDIUM(1, "Medium"),
    HARD(2, "Hard"),
    EXPERT(3, "Expert"),
    NIGHTMARE(4, "Nightmare"),
    IMPOSSIBLE(5, "Impossible");

    private final int id;

    private final String friendlyName;

    MissionDifficulty(int id, String friendlyName) {

        this.id = id;
        this.friendlyName = friendlyName;

    }

    public int getId() {

        return id;

    }

    public String getFriendlyName() {

        return friendlyName;

    }

    public static MissionDifficulty getById(int id) {

        MissionDifficulty result = null;

        int i = 0;

        MissionDifficulty[] values = values();

        while (result == null && i < values.length) {

            if (values[i].id == id) {

                result = values[i];

            }

            i++;

        }

        return result;

    }

}
