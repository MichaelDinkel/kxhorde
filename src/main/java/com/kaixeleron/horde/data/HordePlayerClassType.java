package com.kaixeleron.horde.data;

public enum HordePlayerClassType {

    RUNNER(0.6D, 1.5D),
    FIRE(1.0D, 1.0D),
    EXPLOSIVES(1.0D, 1.0D),
    TANK(2.0D, 0.75D),
    BUILDER(0.7D, 1.0D),
    HEALER(0.8D, 1.0D);

    private final double healthMultiplier, speedMultiplier;

    HordePlayerClassType(double healthMultiplier, double speedMultiplier) {

        this.healthMultiplier = healthMultiplier;
        this.speedMultiplier = speedMultiplier;

    }

    public double getHealthMultiplier() {

        return healthMultiplier;

    }

    public double getSpeedMultiplier() {

        return speedMultiplier;

    }
}
