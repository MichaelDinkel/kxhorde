package com.kaixeleron.horde.map.command;

import com.kaixeleron.horde.map.MapManager;
import com.kaixeleron.horde.map.data.Map;
import com.kaixeleron.horde.mission.MissionManager;
import com.kaixeleron.horde.mission.data.Mission;
import com.kaixeleron.horde.navigation.GraphManager;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Path;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class CommandMap implements CommandExecutor {

    private final GraphManager graphManager;
    private final MapManager mapManager;
    private final MissionManager missionManager;

    public CommandMap(GraphManager graphManager, MapManager mapManager, MissionManager missionManager) {

        this.graphManager = graphManager;
        this.mapManager = mapManager;
        this.missionManager = missionManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            help(sender, label);

        } else {

            switch (args[0].toLowerCase()) {
                case "create" -> create(sender, label, args.length > 1 ? args[1] : null);
                case "delete" -> delete(sender, label, args.length > 1 ? args[1] : null);
                case "setenabled" -> setEnabled(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "list" -> list(sender);
                case "setmesh" -> setMesh(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "setpath" -> setPath(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "addmission" -> addMission(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "removemission" -> removeMission(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "setclassselect" -> setClassSelect(sender, label, args.length > 1 ? args[1] : null);
                case "addspawn" -> addSpawn(sender, label, args.length > 1 ? args[1] : null);
                case "deletespawn" -> deleteSpawn(sender, label, args.length > 1 ? args[1] : null, args.length > 2 ? args[2] : null);
                case "showmap" -> showMap(sender, label, args.length > 1 ? args[1] : null);
                case "teleport" -> teleport(sender, label, args.length > 1 ? args[1] : null);
                default -> help(sender, label);
            }

        }

        return true;

    }

    private void help(CommandSender sender, String label) {

        sender.sendMessage("kxHorde map management:");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " create <name> " + ChatColor.RESET + "- Create a new map.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " delete <map> " + ChatColor.RESET + "- Delete a map.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " setenabled <map> <true|false> " + ChatColor.RESET + "- Set if a map is enabled.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " list " + ChatColor.RESET + "- List maps.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " setmesh <map> <mesh> " + ChatColor.RESET + "- Set a map's navmesh.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " setpath <map> <path> " + ChatColor.RESET + "- Set a map's path.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " addmission <map> <mission> " + ChatColor.RESET + "- Add a mission to a map.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " removemission <map> <mission> " + ChatColor.RESET + "- Remove a map mission.");

        if (sender instanceof Player) {

            sender.sendMessage(ChatColor.GRAY + "/" + label + " setclassselect <map> " + ChatColor.RESET + "- Set a map's class select location.");
            sender.sendMessage(ChatColor.GRAY + "/" + label + " addspawn <map> " + ChatColor.RESET + "- Add a spawn to a map.");

        }

        sender.sendMessage(ChatColor.GRAY + "/" + label + " deletespawn <map> <spawn> " + ChatColor.RESET + "- Delete a spawn from a map.");
        sender.sendMessage(ChatColor.GRAY + "/" + label + " showmap <map> " + ChatColor.RESET + "- Show data about a map.");

        if (sender instanceof Player) {

            sender.sendMessage(ChatColor.GRAY + "/" + label + " teleport <map> " + ChatColor.RESET + "- Teleport to a map.");

        }

    }

    private void create(CommandSender sender, String label, @Nullable String name) {

        if (name == null) {

            sender.sendMessage("Create a new map.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " create <name>");

        } else {

            if (mapManager.mapExists(name)) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + name + ChatColor.RED + " already exists.");

            } else {

                Map map = new Map(name);

                mapManager.addMap(map);
                mapManager.saveMap(map);

                sender.sendMessage("Map " + ChatColor.GRAY + name + ChatColor.RESET + " created.");

            }

        }

    }

    private void delete(CommandSender sender, String label, @Nullable String name) {

        if (name == null) {

            sender.sendMessage("Delete a map.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " delete <map>");

        } else {

            Map map = mapManager.getMap(name);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + name + ChatColor.RED + " does not exist.");

            } else {

                if (mapManager.deleteMap(map)) {

                    mapManager.removeMap(map);
                    mapManager.deleteMap(map);

                    sender.sendMessage("Map " + ChatColor.GRAY + name + ChatColor.RESET + " deleted.");

                } else {

                    sender.sendMessage(ChatColor.RED + "The map " + ChatColor.RESET + name + ChatColor.RED + " could not be deleted.");

                }

            }

        }

    }

    private void setEnabled(CommandSender sender, String label, @Nullable String mapName, @Nullable String enabled) {

        if (mapName == null || enabled == null) {

            sender.sendMessage("Set if a map is enabled.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " setenabled <map> <true|false> ");

        } else {

            Map map = mapManager.getMap(mapName);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

            } else {

                switch (enabled.toLowerCase()) {

                    case "true":

                        if (map.getGraph() == null) {

                            sender.sendMessage(ChatColor.RED + "Could not enable " + ChatColor.RESET + mapName + ChatColor.RED + ": missing navmesh.");

                        } else if (map.getMainPath() == null) {

                            sender.sendMessage(ChatColor.RED + "Could not enable " + ChatColor.RESET + mapName + ChatColor.RED + ": missing path.");

                        } else if (map.listSpawns().length == 0) {

                            sender.sendMessage(ChatColor.RED + "Could not enable " + ChatColor.RESET + mapName + ChatColor.RED + ": no spawn points.");

                        } else if (map.listMissions().length == 0) {

                            sender.sendMessage(ChatColor.RED + "Could not enable " + ChatColor.RESET + mapName + ChatColor.RED + ": no missions.");

                        } else if (map.getClassSelectLocation() == null) {

                            sender.sendMessage(ChatColor.RED + "Could not enable " + ChatColor.RESET + mapName + ChatColor.RED + ": no class select location.");

                        } else {

                            map.setEnabled(true);
                            mapManager.saveMap(map);
                            sender.sendMessage("Map " + ChatColor.GRAY + mapName + ChatColor.RESET + " enabled.");

                        }

                        break;

                    case "false":

                        map.setEnabled(false);
                        mapManager.saveMap(map);
                        sender.sendMessage("Map " + ChatColor.GRAY + mapName + ChatColor.RESET + " disabled.");

                        break;

                    default:

                        sender.sendMessage(ChatColor.RED + "Invalid enabled status. Must be " + ChatColor.RESET + "true" + ChatColor.RED + " or " + ChatColor.RESET + "false" + ChatColor.RED + ".");

                        break;

                }

            }

        }

    }

    private void list(CommandSender sender) {

        Map[] maps = mapManager.listMaps();

        sender.sendMessage("There " + (maps.length == 1 ? "is " : "are ") + maps.length + " map" + (maps.length == 1 ? "" : "s") + " loaded" + (maps.length == 0 ? "." : ":"));

        for (Map map : maps) {

            sender.sendMessage(ChatColor.GRAY + " - " + ChatColor.RESET + map.getName());

        }

    }

    private void setMesh(CommandSender sender, String label, @Nullable String mapName, @Nullable String mesh) {

        if (mapName == null || mesh == null) {

            sender.sendMessage("Set a map's navmesh.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " setmesh <map> <mesh>");

        } else {

            Map map = mapManager.getMap(mapName);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

            } else {

                Graph graph = graphManager.getGraph(mesh);

                if (graph == null) {

                    sender.sendMessage(ChatColor.RED + "A navmesh with the name " + ChatColor.RESET + mesh + ChatColor.RED + " does not exist.");

                } else {

                    map.setGraph(graph);
                    mapManager.saveMap(map);

                    sender.sendMessage("Navmesh for " + ChatColor.GRAY + mapName + ChatColor.RESET + " set to " + ChatColor.GRAY + mesh + ChatColor.RESET + ".");

                }

            }

        }

    }

    private void setPath(CommandSender sender, String label, @Nullable String mapName, @Nullable String pathName) {

        if (mapName == null || pathName == null) {

            sender.sendMessage("Set a map's path.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " setpath <map> <path>");

        } else {

            Map map = mapManager.getMap(mapName);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

            } else {

                Graph graph = map.getGraph();

                if (graph == null) {

                    sender.sendMessage(ChatColor.RED + "The specified map has no navmesh.");

                } else {

                    Path path = graph.getPath(pathName);

                    if (path == null) {

                        sender.sendMessage(ChatColor.RED + "A path with the name " + ChatColor.RESET + mapName + ChatColor.RED + " on the navmesh " + ChatColor.RESET + graph.getName() + ChatColor.RED + " does not exist.");

                    } else {

                        map.setMainPath(path);
                        mapManager.saveMap(map);

                        sender.sendMessage("Path for " + ChatColor.GRAY + mapName + ChatColor.RESET + " set to " + ChatColor.GRAY + pathName + ChatColor.RESET + ".");

                    }

                }

            }

        }

    }

    private void addMission(CommandSender sender, String label, @Nullable String mapName, @Nullable String missionName) {

        if (mapName == null || missionName == null) {

            sender.sendMessage("Add a mission to a map.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " addmission <map> <mission>");

        } else {

            Map map = mapManager.getMap(mapName);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

            } else {

                Mission mission = missionManager.getMission(missionName);

                if (mission == null) {

                    sender.sendMessage(ChatColor.RED + "A mission with the name " + ChatColor.RESET + missionName + ChatColor.RED + " does not exist.");

                } else {

                    if (map.hasMission(mission)) {

                        sender.sendMessage(ChatColor.RED + "Map " + ChatColor.RESET + mapName + ChatColor.RED + " already has " + ChatColor.RESET + missionName + ChatColor.RED + " as a mission.");

                    } else {

                        map.addMission(mission);
                        mapManager.saveMap(map);

                        sender.sendMessage("Mission " + ChatColor.GRAY + missionName + ChatColor.RESET + " added to " + ChatColor.GRAY + mapName + ChatColor.RESET + ".");

                    }

                }

            }

        }

    }

    private void removeMission(CommandSender sender, String label, @Nullable String mapName, @Nullable String missionName) {

        if (mapName == null || missionName == null) {

            sender.sendMessage("Remove a map mission.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " removemission <map> <mission>");

        } else {

            Map map = mapManager.getMap(mapName);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

            } else {

                Mission mission = missionManager.getMission(missionName);

                if (mission == null) {

                    sender.sendMessage(ChatColor.RED + "A mission with the name " + ChatColor.RESET + missionName + ChatColor.RED + " does not exist.");

                } else {

                    if (map.hasMission(mission)) {

                        map.removeMission(mission);
                        mapManager.saveMap(map);

                        sender.sendMessage("Mission " + ChatColor.GRAY + missionName + ChatColor.RESET + " removed from " + ChatColor.GRAY + mapName + ChatColor.RESET + ".");

                    } else {

                        sender.sendMessage(ChatColor.RED + "Map " + ChatColor.RESET + mapName + ChatColor.RED + " does not have " + ChatColor.RESET + missionName + ChatColor.RED + " as a mission.");

                    }

                }

            }

        }

    }

    private void setClassSelect(CommandSender sender, String label, @Nullable String mapName) {

        if (sender instanceof Player player) {

            if (mapName == null) {

                sender.sendMessage("Set a map's class select location.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " setclassselect <map>");

            } else {

                Map map = mapManager.getMap(mapName);

                if (map == null) {

                    sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

                } else {

                    map.setClassSelectLocation(player.getLocation());
                    mapManager.saveMap(map);

                    sender.sendMessage("Class select location set on map " + ChatColor.GRAY + mapName + ChatColor.RESET + ".");

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

    private void addSpawn(CommandSender sender, String label, @Nullable String mapName) {

        if (sender instanceof Player player) {

            if (mapName == null) {

                sender.sendMessage("Add a spawn to a map.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " addspawn <map>");

            } else {

                Map map = mapManager.getMap(mapName);

                if (map == null) {

                    sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

                } else {

                    map.addSpawn(player.getLocation());
                    mapManager.saveMap(map);

                    sender.sendMessage("Spawn point added to map " + ChatColor.GRAY + mapName + ChatColor.RESET + ".");

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

    private void deleteSpawn(CommandSender sender, String label, @Nullable String mapName, @Nullable String spawn) {

        if (mapName == null || spawn == null) {

            sender.sendMessage("Delete a spawn from a map.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " deletespawn <map> <spawn>");

        } else {

            Map map = mapManager.getMap(mapName);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

            } else {

                int spawnId = -1;

                try {

                    spawnId = Integer.parseInt(spawn);

                } catch (NumberFormatException ignored) {
                }

                if (spawnId >= 0) {

                    if (spawnId > map.listSpawns().length - 1) {

                        sender.sendMessage(ChatColor.RED + "The map " + ChatColor.RESET + mapName + ChatColor.RED + " has no spawn point with ID " + ChatColor.RESET + spawnId + ChatColor.RED + ".");

                    } else {

                        map.removeSpawn(spawnId);
                        mapManager.saveMap(map);

                        sender.sendMessage("Spawn point " + ChatColor.GRAY + spawnId + ChatColor.RESET + " deleted from map " + ChatColor.GRAY + mapName + ChatColor.RESET + ".");

                    }

                } else {

                    sender.sendMessage(ChatColor.RED + "Invalid spawn point ID. Must be a positive integer.");

                }

            }

        }

    }

    @SuppressWarnings("ConstantConditions")
    private void showMap(CommandSender sender, String label, @Nullable String mapName) {

        if (mapName == null) {

            sender.sendMessage("Show data about a map.");
            sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " showmap <map>");

        } else {

            Map map = mapManager.getMap(mapName);

            if (map == null) {

                sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

            } else {

                sender.sendMessage("Map " + ChatColor.GRAY + mapName + ChatColor.RESET + ":");

                sender.sendMessage("Enabled: " + ChatColor.GRAY + (map.isEnabled() ? "yes" : "no"));

                if (map.getGraph() == null) {

                    sender.sendMessage("No navmesh set.");

                } else {

                    sender.sendMessage("Navmesh: " + ChatColor.GRAY + map.getGraph().getName());

                    if (map.getMainPath() == null) {

                        sender.sendMessage("No path set.");

                    } else {

                        sender.sendMessage("Path: " + ChatColor.GRAY + map.getMainPath().getName());

                    }

                }

                Location classSelectLocation = map.getClassSelectLocation();

                if (classSelectLocation == null) {

                    sender.sendMessage("No class select location set.");

                } else {

                    sender.sendMessage("Class select location: " + ChatColor.GRAY
                            + BigDecimal.valueOf(classSelectLocation.getX()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(classSelectLocation.getY()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(classSelectLocation.getZ()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(classSelectLocation.getYaw()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(classSelectLocation.getPitch()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString());

                }

                Mission[] missions = map.listMissions();

                sender.sendMessage("This map has " + missions.length + " mission" + (missions.length == 1 ? "" : "s") + (missions.length == 0 ? "." : ":"));

                for (Mission mission : missions) {

                    sender.sendMessage(ChatColor.GRAY + " - " + ChatColor.RESET + mission.getName());

                }

                Location[] spawns = map.listSpawns();

                sender.sendMessage("This map has " + spawns.length + " spawn point" + (spawns.length == 1 ? "" : "s") + (spawns.length == 0 ? "." : " in world " + ChatColor.GRAY + spawns[0].getWorld().getName() + ChatColor.RESET + ":"));

                for (int i = 0; i < spawns.length; i++) {

                    sender.sendMessage(ChatColor.GRAY + " " + i + ". " + ChatColor.RESET
                            + BigDecimal.valueOf(spawns[i].getX()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(spawns[i].getY()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(spawns[i].getZ()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(spawns[i].getYaw()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString()
                            + ", " + BigDecimal.valueOf(spawns[i].getPitch()).setScale(3, RoundingMode.DOWN).stripTrailingZeros().toPlainString());

                }

            }

        }

    }

    private void teleport(CommandSender sender, String label, @Nullable String mapName) {

        if (sender instanceof Player player) {

            if (mapName == null) {

                sender.sendMessage("Teleport to a map.");
                sender.sendMessage(ChatColor.RED + "Usage: " + ChatColor.RESET + "/" + label + " teleport <map>");

            } else {

                Map map = mapManager.getMap(mapName);

                if (map == null) {

                    sender.sendMessage(ChatColor.RED + "A map with the name " + ChatColor.RESET + mapName + ChatColor.RED + " does not exist.");

                } else {

                    Location[] spawns = map.listSpawns();

                    if (spawns.length == 0) {

                        sender.sendMessage(ChatColor.RED + "The map " + ChatColor.RESET + mapName + ChatColor.RED + " has no spawn points.");

                    } else {

                        player.teleport(spawns[0]);
                        sender.sendMessage("Teleported to map " + ChatColor.GRAY + mapName + ChatColor.RESET + ".");

                    }

                }

            }

        } else {

            sender.sendMessage("This command can only be used by a player.");

        }

    }

}
