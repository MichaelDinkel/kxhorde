package com.kaixeleron.horde.map.command.tabcomplete;

import com.kaixeleron.horde.map.MapManager;
import com.kaixeleron.horde.map.data.Map;
import com.kaixeleron.horde.mission.MissionManager;
import com.kaixeleron.horde.mission.data.Mission;
import com.kaixeleron.horde.navigation.GraphManager;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Path;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TabCompleteMap implements TabCompleter {

    private final String[] arg1Suggestions;

    private final GraphManager graphManager;
    private final MapManager mapManager;
    private final MissionManager missionManager;

    public TabCompleteMap(GraphManager graphManager, MapManager mapManager, MissionManager missionManager) {

        arg1Suggestions = new String[]{"create", "delete", "setenabled", "list", "setmesh", "setpath", "addmission", "removemission", "setclassselect", "addspawn", "deletespawn", "showmap", "teleport"};

        this.graphManager = graphManager;
        this.mapManager = mapManager;
        this.missionManager = missionManager;

    }

    @SuppressWarnings("NullableProblems")
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {

        List<String> suggestions = new ArrayList<>();

        switch (args.length) {

            case 1:

                if (args[0].length() == 0) {

                    suggestions.addAll(Arrays.asList(arg1Suggestions));

                } else {

                    for (String suggestion : arg1Suggestions) {

                        if (suggestion.startsWith(args[0].toLowerCase())) {

                            suggestions.add(suggestion);

                        }

                    }

                }

                break;

            case 2:

                switch (args[0].toLowerCase()) {

                    case "create":

                        if (args[1].length() == 0) {

                            suggestions.add("name");

                        } else {

                            suggestions.add(args[1]);

                        }

                        break;

                    case "delete":
                    case "setenabled":
                    case "setmesh":
                    case "setpath":
                    case "addmission":
                    case "removemission":
                    case "setclassselect":
                    case "addspawn":
                    case "deletespawn":
                    case "showmap":
                    case "teleport":

                        for (Map map : mapManager.listMaps()) {

                            if (args[1].length() == 0 || map.getName().startsWith(args[1])) {

                                suggestions.add(map.getName());

                            }

                        }

                        break;

                }

                break;

            case 3:

                switch (args[0].toLowerCase()) {

                    case "setenabled":

                        if (args[2].length() == 0 || "true".startsWith(args[2].toLowerCase())) {

                            suggestions.add("true");

                        }

                        if (args[2].length() == 0 || "false".startsWith(args[2].toLowerCase())) {

                            suggestions.add("false");

                        }

                        break;

                    case "setmesh":

                        for (Graph graph : graphManager.listGraphs()) {

                            if (args[2].length() == 0 || graph.getName().startsWith(args[2])) {

                                suggestions.add(graph.getName());

                            }

                        }

                        break;

                    case "setpath": {

                        Map map = mapManager.getMap(args[1]);

                        if (map != null) {

                            Graph graph = map.getGraph();

                            if (graph != null) {

                                for (Path path : graph.getPaths()) {

                                    if (args[2].length() == 0 || path.getName().startsWith(args[2])) {

                                        suggestions.add(path.getName());

                                    }

                                }

                            }

                        }

                        break;

                    }

                    case "addmission": {

                        Map map = mapManager.getMap(args[1]);

                        if (map != null) {

                            for (Mission mission : missionManager.listMissions()) {

                                if ((args[2].length() == 0 || mission.getName().startsWith(args[2])) && !map.hasMission(mission)) {

                                    suggestions.add(mission.getName());

                                }

                            }

                        }

                        break;

                    }

                    case "removemission": {

                        Map map = mapManager.getMap(args[1]);

                        if (map != null) {

                            for (Mission mission : map.listMissions()) {

                                if (args[2].length() == 0 || mission.getName().startsWith(args[2])) {

                                    suggestions.add(mission.getName());

                                }

                            }

                        }

                        break;

                    }

                    case "deletespawn": {

                        Map map = mapManager.getMap(args[1]);

                        if (map != null) {

                            Location[] spawns = map.listSpawns();

                            for (int i = 0; i < spawns.length; i++) {

                                String id = Integer.toString(i);

                                if (args[2].length() == 0 || id.startsWith(args[2])) {

                                    suggestions.add(id);

                                }

                            }

                        }

                        break;

                    }

                }

                break;

        }

        return suggestions;

    }

}
