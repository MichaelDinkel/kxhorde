package com.kaixeleron.horde.map;

import com.kaixeleron.horde.map.data.Map;
import com.kaixeleron.horde.navigation.GraphManager;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MapManager {

    private final List<Map> maps;

    private final File mapFolder;
    private final GraphManager graphManager;

    private final Random random;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public MapManager(File dataFolder, GraphManager graphManager) {

        this.graphManager = graphManager;

        this.maps = new ArrayList<>();

        mapFolder = new File(dataFolder, "maps");
        mapFolder.mkdir();

        random = new Random();

    }

    public void addMap(Map map) {

        maps.add(map);

    }

    public void removeMap(Map map) {

        maps.remove(map);

    }

    public Map[] listMaps() {

        return maps.toArray(new Map[0]);

    }

    public Map getMap(String name) {

        Map map = null;

        int i = 0;

        while (map == null && i < maps.size()) {

            if (maps.get(i).getName().equals(name)) {

                map = maps.get(i);

            }

            i++;

        }

        return map;

    }

    public Map getRandomMap() {

        Map map = null;

        if (maps.size() > 0) {

            map = maps.get(random.nextInt(maps.size()));

        }

        return map;

    }

    public boolean mapExists(String name) {

        return getMap(name) != null;

    }

    public void clearMaps() {

        for (Map map : maps) {

            map.clearSpawns();
            map.clearMissions();

        }

        maps.clear();

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void saveMap(Map map) {

        File file = new File(mapFolder, String.format("%s.map", map.getName()));

        if (!file.exists()) {

            try {

                file.createNewFile();

            } catch (IOException e) {

                System.err.printf("Could not create file %s.map.%n", map.getName());
                e.printStackTrace();

            }

        }

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {

            outputStream.writeObject(map);
            outputStream.flush();

        } catch (IOException e) {

            System.err.printf("Could not write to file %s.map.%n", map.getName());
            e.printStackTrace();

        }

    }

    public boolean deleteMap(Map map) {

        return new File(mapFolder, String.format("%s.map", map.getName())).delete();

    }

    public void loadMaps() throws IOException {

        File[] maps = mapFolder.listFiles();

        if (maps != null) {

            for (File map : maps) {

                if (map.getName().endsWith(".map")) {

                    ObjectInputStream inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(map)));

                    try {

                        Map loadedMap = (Map) inputStream.readObject();
                        loadedMap.loadGraphAndPath(graphManager);

                        this.maps.add(loadedMap);

                    } catch (ClassNotFoundException ignored) {}

                    inputStream.close();

                }

            }

        }

    }

}
