package com.kaixeleron.horde.map.data;

import com.kaixeleron.horde.mission.MissionManager;
import com.kaixeleron.horde.mission.data.Mission;
import com.kaixeleron.horde.navigation.GraphManager;
import com.kaixeleron.horde.navigation.data.Graph;
import com.kaixeleron.horde.navigation.data.Path;
import org.bukkit.Location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Map implements Serializable {

    private final String name;

    private transient Graph graph = null;
    private transient Path mainPath = null;

    private String graphName, mainPathName;

    private final List<SerializableLocation> spawns;

    private SerializableLocation classSelectLocation;

    private transient List<Mission> missions;
    private final List<String> missionNames;

    private boolean enabled;

    public Map(String name) {

        this.name = name;
        graphName = null;
        mainPathName = null;
        spawns = new ArrayList<>();
        classSelectLocation = null;
        missions = new ArrayList<>();
        missionNames = new ArrayList<>();

        enabled = false;

    }

    public String getName() {

        return name;

    }

    public Graph getGraph() {

        return graph;

    }

    public void setGraph(Graph graph) {

        this.graph = graph;
        graphName = graph.getName();

    }

    public Path getMainPath() {

        return mainPath;

    }

    public void setMainPath(Path mainPath) {

        this.mainPath = mainPath;
        mainPathName = mainPath.getName();

    }

    public void loadGraphAndPath(GraphManager graphManager) {

        this.graph = graphManager.getGraph(graphName);

        if (graph != null) {

            this.mainPath = graph.getPath(mainPathName);

        }

    }

    public void addSpawn(Location spawn) {

        spawns.add(new SerializableLocation(spawn));

    }

    public void removeSpawn(int position) {

        spawns.remove(position);

    }

    public Location[] listSpawns() {

        SerializableLocation[] spawns = this.spawns.toArray(new SerializableLocation[0]);

        Location[] unwrapped = new Location[spawns.length];

        for (int i = 0; i < spawns.length; i++) {

            unwrapped[i] = spawns[i].getWrappedLocation();

        }

        return unwrapped;

    }

    public void clearSpawns() {

        spawns.clear();

    }

    public Location getClassSelectLocation() {

        Location classSelect = null;

        if (classSelectLocation != null) {

            classSelect = classSelectLocation.getWrappedLocation();

        }

        return classSelect;

    }

    public void setClassSelectLocation(Location classSelectLocation) {

        this.classSelectLocation = new SerializableLocation(classSelectLocation);

    }

    public void addMission(Mission mission) {

        missions.add(mission);
        missionNames.add(mission.getName());

    }

    public void removeMission(Mission mission) {

        missions.remove(mission);
        missionNames.remove(mission.getName());

    }

    public boolean hasMission(Mission mission) {

        loadMissions();

        return missions.contains(mission);

    }

    public Mission[] listMissions() {

        loadMissions();

        return missions.toArray(new Mission[0]);

    }

    public void clearMissions() {

        if (missions != null) {

            missions.clear();

        }

        missionNames.clear();

    }

    public boolean isEnabled() {

        return enabled;

    }

    public void setEnabled(boolean enabled) {

        this.enabled = enabled;

    }

    @Override
    public boolean equals(Object obj) {

        boolean equals = false;

        if (obj instanceof Map map) {

            equals = name.equals(map.name);

        }

        return equals;

    }

    private void loadMissions() {

        if (missions == null) {

            missions = new ArrayList<>();

        }

        if (missions.isEmpty() && !missionNames.isEmpty()) {

            for (String name : missionNames) {

                Mission missionObj = MissionManager.getInstance().getMission(name);

                if (missionObj != null) {

                    missions.add(missionObj);

                }

            }

        }

    }

}
