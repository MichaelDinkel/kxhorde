package com.kaixeleron.horde.map.data;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.Serializable;

public class SerializableLocation implements Serializable {

    private transient Location wrappedLocation;

    private final String worldName;

    private final double x, y, z;

    private final float yaw, pitch;

    @SuppressWarnings("ConstantConditions")
    SerializableLocation(Location wrappedLocation) {

        this.wrappedLocation = wrappedLocation;

        this.worldName = wrappedLocation.getWorld().getName();
        this.x = wrappedLocation.getX();
        this.y = wrappedLocation.getY();
        this.z = wrappedLocation.getZ();
        this.yaw = wrappedLocation.getYaw();
        this.pitch = wrappedLocation.getPitch();

    }

    Location getWrappedLocation() {

        if (wrappedLocation == null) {

            wrappedLocation = new Location(Bukkit.getWorld(worldName), x, y, z, yaw, pitch);

        }

        return wrappedLocation;

    }

}
