package com.kaixeleron.horde.localization;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class LocalizationManager {

    //TODO implement in CommandReady, Game, and CommandListener

    private final Map<String, Localization> localizations;

    private final File localizationFolder;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public LocalizationManager(File dataFolder) {

        localizations = new HashMap<>();

        localizationFolder = new File(dataFolder, "localization");
        localizationFolder.mkdir();

        saveDefaultFiles();

        File[] localizationFiles = localizationFolder.listFiles();

        if (localizationFiles != null) {

            for (File file : localizationFiles) {

                if (file.getName().endsWith(".txt")) {

                    localizations.put(file.getName().substring(0, file.getName().length() - 4), new Localization(file));

                }

            }

        }

    }

    public Localization getLocalization(String id) {

        Localization localization = localizations.get(id);

        if (localization == null) {

            localization = localizations.get("en_us");

        }

        return localization;

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void saveDefaultFiles() {

        File en_us = new File(localizationFolder, "en_us.txt");
        File ja_jp = new File(localizationFolder, "ja_jp.txt");

        if (!en_us.exists()) {

            try {

                en_us.createNewFile();

            } catch (IOException e) {

                System.err.println("Could not create en_us.txt.");
                e.printStackTrace();

            }

            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(en_us), StandardCharsets.UTF_8))) {

                writer.write("global.usage=Usage: ");
                writer.newLine();
                writer.write("global.error.noplayer=%s is not online.");
                writer.newLine();

                writer.write("game.prefix=[Horde] ");
                writer.newLine();

                writer.write("game.help.header=Horde commands:");
                writer.newLine();
                writer.write("game.help.join=Join a match.");
                writer.newLine();
                writer.write("game.help.leave=Leave a match.");
                writer.newLine();
                writer.write("game.help.spectate=Spectate a player.");
                writer.newLine();
                writer.write("game.help.kick=Kick a player from your match.");
                writer.newLine();

                writer.write("game.join.success=%s joined the match (%s).");
                writer.newLine();

                writer.write("game.leave.othersuccess=%s left the match.");
                writer.newLine();
                writer.write("game.leave.success=Left the match.");
                writer.newLine();

                writer.write("game.spectate.success=Spectating %s. Type %s to leave.");
                writer.newLine();
                writer.write("game.spectate.stopsuccess=Stopped spectaing.");
                writer.newLine();

                writer.write("game.kick.success=Kicked %s.");
                writer.newLine();
                writer.write("game.kick.spectatorsuccess=Kicked spectator %s.");
                writer.newLine();

                writer.write("game.ready.toggle=%s is %s ready.");
                writer.newLine();
                writer.write("game.ready.enabled=now");
                writer.newLine();
                writer.write("game.ready.disabled=no longer");
                writer.newLine();
                writer.write("game.ready.instruction=Type %s to ready up.");
                writer.newLine();
                writer.write("game.ready.waiting=Waiting for players.");
                writer.newLine();

                writer.write("game.error.inmatch=You are already in a match.");
                writer.newLine();
                writer.write("game.error.notinmatch=You are not in a match.");
                writer.newLine();
                writer.write("game.error.nomap=The map %s does not exist.");
                writer.newLine();
                writer.write("game.error.playernomatch=%s is not in a match.");
                writer.newLine();
                writer.write("game.error.notinmymatch=%s is not in your match.");
                writer.newLine();
                writer.write("game.error.notenough=More players are required to start.");
                writer.newLine();
                writer.write("game.error.badcommand=You may not use that command while in a match.");
                writer.newLine();

                writer.write("party.prefix=[Party] ");
                writer.newLine();
                writer.write("party.chatprefix=[P] ");
                writer.newLine();
                writer.write("party.disband=Your party has been disbanded.");
                writer.newLine();

                writer.write("party.help.create=Create a match.");
                writer.newLine();
                writer.write("party.help.leave=Leave a party.");
                writer.newLine();
                writer.write("party.help.join=Join a leader's party.");
                writer.newLine();
                writer.write("party.help.invite=Invite a player to your party.");
                writer.newLine();
                writer.write("party.help.kick=Kick a player from your party.");
                writer.newLine();
                writer.write("party.help.setleader=Set the party leader.");
                writer.newLine();
                writer.write("party.help.mute=Mute party invitation notifications.");
                writer.newLine();
                writer.write("party.help.chat=Chat with members of your party.");
                writer.newLine();

                writer.write("party.create.success=Party created.");
                writer.newLine();

                writer.write("party.leave.success=You have left your party.");
                writer.newLine();
                writer.write("party.leave.broadcast=%s has left the party.");
                writer.newLine();

                writer.write("party.join.success=Joined %s's party.");
                writer.newLine();
                writer.write("party.join.broadcast=%s has joined the party.");
                writer.newLine();

                writer.write("party.invite.cancelled=Cancelled %s's invitation.");
                writer.newLine();
                writer.write("party.invite.success=Invited %s.");
                writer.newLine();
                writer.write("party.invite.notification=%s has invited you to their party. Type %s to join.");
                writer.newLine();

                writer.write("party.kick.success=Kicked %s.");
                writer.newLine();
                writer.write("party.kick.victim=You have been kicked from your party.");
                writer.newLine();

                writer.write("party.setleader.promotion=%s has been promoted to leader of the party.");
                writer.newLine();

                writer.write("party.mute.toggle=Party invites %s.");
                writer.newLine();
                writer.write("party.mute.enabled=muted");
                writer.newLine();
                writer.write("party.mute.disabled=unmuted");
                writer.newLine();

                writer.write("party.error.create=You are already in a party and cannot create a new one.");
                writer.newLine();
                writer.write("party.error.noparty=You are not in a party.");
                writer.newLine();
                writer.write("party.error.inparty=You are already in a party.");
                writer.newLine();
                writer.write("party.error.playerinparty=%s is already in your party.");
                writer.newLine();
                writer.write("party.error.playernotinparty=%s is not in your party.");
                writer.newLine();
                writer.write("party.error.playernoparty=%s is not in a party.");
                writer.newLine();
                writer.write("party.error.full=%s's party is full.");
                writer.newLine();
                writer.write("party.error.noinvite=You have not been invited to %s's party.");
                writer.newLine();
                writer.write("party.error.notleader=You are not the leader of your party.");
                writer.newLine();
                writer.write("party.error.playernotleader=%s is not the leader of their party.");
                writer.newLine();
                writer.write("party.error.alreadyleader=%s is already the leader of the party.");
                writer.newLine();

            } catch (IOException e) {

                System.err.println("Could not write to en_US.txt.");
                e.printStackTrace();

            }

        }

        if (!ja_jp.exists()) {

            try {

                ja_jp.createNewFile();

            } catch (IOException e) {

                System.err.println("Could not create ja_jp.txt.");
                e.printStackTrace();

            }

            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ja_jp), StandardCharsets.UTF_8))) {

                writer.write("global.usage=使用法: ");
                writer.newLine();
                writer.write("global.error.noplayer=%sはオンラインではありません。");
                writer.newLine();

                writer.write("game.prefix=[大群] ");
                writer.newLine();

                writer.write("game.help.header=大群コマンド:");
                writer.newLine();
                writer.write("game.help.join=試合に参加する。");
                writer.newLine();
                writer.write("game.help.leave=試合を残します。");
                writer.newLine();
                writer.write("game.help.spectate=プレーヤーを見て。");
                writer.newLine();
                writer.write("game.help.kick=マッチからプレイヤーをキックします。");
                writer.newLine();

                writer.write("game.join.success=%s joined the match (%s).");
                writer.newLine();

                writer.write("game.leave.othersuccess=%s left the match.");
                writer.newLine();
                writer.write("game.leave.success=Left the match.");
                writer.newLine();

                writer.write("game.spectate.success=Spectating %s. Type %s to leave.");
                writer.newLine();
                writer.write("game.spectate.stopsuccess=Stopped spectaing.");
                writer.newLine();

                writer.write("game.kick.success=Kicked %s.");
                writer.newLine();
                writer.write("game.kick.spectatorsuccess=Kicked spectator %s.");
                writer.newLine();

                writer.write("game.ready.toggle=%s is %s ready.");
                writer.newLine();
                writer.write("game.ready.enabled=now");
                writer.newLine();
                writer.write("game.ready.disabled=no longer");
                writer.newLine();
                writer.write("game.ready.instruction=Type %s to ready up.");
                writer.newLine();
                writer.write("game.ready.waiting=Waiting for players.");
                writer.newLine();

                writer.write("game.error.inmatch=You are already in a match.");
                writer.newLine();
                writer.write("game.error.notinmatch=You are not in a match.");
                writer.newLine();
                writer.write("game.error.nomap=The map %s does not exist.");
                writer.newLine();
                writer.write("game.error.playernomatch=%s is not in a match.");
                writer.newLine();
                writer.write("game.error.notinmymatch=%s is not in your match.");
                writer.newLine();
                writer.write("game.error.notenough=More players are required to start.");
                writer.newLine();
                writer.write("game.error.badcommand=You may not use that command while in a match.");
                writer.newLine();

                writer.write("party.prefix=[Party] ");
                writer.newLine();
                writer.write("party.chatprefix=[P] ");
                writer.newLine();
                writer.write("party.disband=Your party has been disbanded.");
                writer.newLine();

                writer.write("party.help.create=Create a match.");
                writer.newLine();
                writer.write("party.help.leave=Leave a party.");
                writer.newLine();
                writer.write("party.help.join=Join a leader's party.");
                writer.newLine();
                writer.write("party.help.invite=Invite a player to your party.");
                writer.newLine();
                writer.write("party.help.kick=Kick a player from your party.");
                writer.newLine();
                writer.write("party.help.setleader=Set the party leader.");
                writer.newLine();
                writer.write("party.help.mute=Mute party invitation notifications.");
                writer.newLine();
                writer.write("party.help.chat=Chat with members of your party.");
                writer.newLine();

                writer.write("party.create.success=Party created.");
                writer.newLine();

                writer.write("party.leave.success=You have left your party.");
                writer.newLine();
                writer.write("party.leave.broadcast=%s has left the party.");
                writer.newLine();

                writer.write("party.join.success=Joined %s's party.");
                writer.newLine();
                writer.write("party.join.broadcast=%s has joined the party.");
                writer.newLine();

                writer.write("party.invite.cancelled=Cancelled %s's invitation.");
                writer.newLine();
                writer.write("party.invite.success=Invited %s.");
                writer.newLine();
                writer.write("party.invite.notification=%s has invited you to their party. Type %s to join.");
                writer.newLine();

                writer.write("party.kick.success=Kicked %s.");
                writer.newLine();
                writer.write("party.kick.victim=You have been kicked from your party.");
                writer.newLine();

                writer.write("party.setleader.promotion=%s has been promoted to leader of the party.");
                writer.newLine();

                writer.write("party.mute.toggle=Party invites %s.");
                writer.newLine();
                writer.write("party.mute.enabled=muted");
                writer.newLine();
                writer.write("party.mute.disabled=unmuted");
                writer.newLine();

                writer.write("party.error.create=You are already in a party and cannot create a new one.");
                writer.newLine();
                writer.write("party.error.noparty=You are not in a party.");
                writer.newLine();
                writer.write("party.error.inparty=You are already in a party.");
                writer.newLine();
                writer.write("party.error.playerinparty=%s is already in your party.");
                writer.newLine();
                writer.write("party.error.playernotinparty=%s is not in your party.");
                writer.newLine();
                writer.write("party.error.playernoparty=%s is not in a party.");
                writer.newLine();
                writer.write("party.error.full=%s's party is full.");
                writer.newLine();
                writer.write("party.error.noinvite=You have not been invited to %s's party.");
                writer.newLine();
                writer.write("party.error.notleader=You are not the leader of your party.");
                writer.newLine();
                writer.write("party.error.playernotleader=%s is not the leader of their party.");
                writer.newLine();
                writer.write("party.error.alreadyleader=%s is already the leader of the party.");
                writer.newLine();

            } catch (IOException e) {

                System.err.println("Could not write to en_US.txt.");
                e.printStackTrace();

            }

        }

    }

}
