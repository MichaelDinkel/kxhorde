package com.kaixeleron.horde.localization;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Localization {

    private final Map<String, String> strings;

    public Localization(File source) {

        strings = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(source), StandardCharsets.UTF_8))) {

            String line = reader.readLine();

            while (line != null) {

                String[] split = line.split("=");

                strings.put(split[0], split[1]);

                line = reader.readLine();

            }

        } catch (IOException e) {

            System.err.printf("Could not open localization file %s.%n", source.getName());
            e.printStackTrace();

        }

    }

    public String getLocalizedString(String id) {

        String string = strings.get(id);

        if (string == null) {

            string = "";

        }

        return string;

    }

}
